//
//  WaitListViewCell.swift
//  Butter
//
//  Created by Grigor Avagyan on 9/23/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class WaitListViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var guestCountLabel: UILabel!
}
