//
//  AddServerPopoverController.swift
//  Butter
//
//  Created by Harut on 8/17/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol AddServerPopoverEventListener {
    func addServer(name: String, serverColor: String)
}

class AddServerPopoverController: UIViewController, UITextFieldDelegate{
    var delegate: AddServerPopoverEventListener!
    var latestPressedButton: UIButton!
    var buttonPosX: CGFloat = 0.0
    var buttonPosY: CGFloat = 165
    var viewWidth: CGFloat = 0.0
    var currentButtonTag: Int = 0
    var name: String = ""
    var serverColor: String = ""
    var addButtonDefaultColor: UIColor? = nil

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    
    @IBAction func onAddServerButtonClick(sender: UIButton) {
        delegate?.addServer(name, serverColor: serverColor)
        resetAddServerPopover()
    }
    
    @IBAction func dismissAddServerPopover(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        self.preferredContentSize = CGSizeMake(370, 450)
        buttonPosX = self.preferredContentSize.width / 12.3
        setupAddServerButtons()
        addButton.enabled = false
        addButtonDefaultColor = addButton.backgroundColor!
    }

    func setupAddServerButtons() {
        let buttonInterval: CGFloat = 70
        
        for color in UIUtils.serverServerColors {
            let button = UIButton(frame: CGRect(x: buttonPosX, y: buttonPosY, width: 30, height: 30))
            button.tag = currentButtonTag
            currentButtonTag += 1
            buttonPosX += buttonInterval
            if buttonPosX > self.preferredContentSize.width {
                buttonPosX = self.preferredContentSize.width / 12.3
                buttonPosY += buttonInterval
            }
            button.backgroundColor = UIUtils.hexToUIColor(color)
            button.layer.cornerRadius = button.bounds.width / 2
            let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddServerPopoverController.onButtonClick(_:)))
            button.addGestureRecognizer(tapGesture)
            self.view.addSubview(button)
        }
    }
    
    func resetAddServerPopover() {
        name = ""
        serverColor = ""
        addButton.backgroundColor = addButtonDefaultColor
        addButton.enabled = false
        nameTextField.text = ""
        removeCircleSublayer()
    }
    
    func onButtonClick(gest: UITapGestureRecognizer){
        removeCircleSublayer()
        let button = gest.view as! UIButton
        addSelectionCircle(button)
        latestPressedButton = button
        serverColor = UIUtils.serverServerColors[button.tag]
        if name != ""  {
            addButton.enabled = true
            addButton.backgroundColor = UIUtils.COLOR_GREEN
        }
    }
    
    func addSelectionCircle(button: UIButton) {
        let sub: CALayer = CALayer()
        sub.frame = CGRectInset(button.bounds , -6, -6)
        sub.cornerRadius = sub.bounds.width / 2
        sub.borderWidth = 2
        sub.borderColor = UIColor.grayColor().CGColor
        button.layer.addSublayer(sub)
    }
    
    func removeCircleSublayer() {
        if latestPressedButton != nil {
            latestPressedButton.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        let updatedString = (textField.text as NSString?)?.stringByReplacingCharactersInRange(range, withString: string)
        name = updatedString!
        if serverColor != "" {
            addButton.enabled = true
            addButton.backgroundColor = UIUtils.COLOR_GREEN
        }
        
        return true
    }
}