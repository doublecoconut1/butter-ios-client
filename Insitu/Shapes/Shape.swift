//
//  ShapesFactory.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/23/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

class Shape : UIView{
    
    override init(frame: CGRect) {
        super.init(frame:frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
    }
    
}