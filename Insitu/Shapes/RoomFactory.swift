//
//  ShapeFactory.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/24/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

class RoomFactory{
    
    class func createRoom(parentView: UIView, floor: Floor, isVisibleMaxMinSizes: Bool) -> UIView {
        
        var roomWidth = parentView.bounds.width
        var roomHeight = parentView.bounds.height
        
        if floor.config.widthToHeightRatio <= 1 {
            roomWidth = parentView.bounds.height * floor.config.widthToHeightRatio
        }
        else{
            roomHeight = parentView.bounds.width / floor.config.widthToHeightRatio
        }
        
        let posY = (parentView.bounds.height - roomHeight) / 2
        let posX = (parentView.bounds.width - roomWidth) / 2
        let frame = CGRect(x: posX, y: posY, width: roomWidth, height: roomHeight)

        let roomView: UIView = UIView(frame: frame)

        for table in floor.tables {
            
            let rootViewProps = getRectFromProperties(table, parentWidth: Double(roomWidth), parentHeight: Double(roomHeight))
            let childProps = CGRect(x: 0, y: 0, width: rootViewProps.width, height: rootViewProps.height)
            
            let tableView = createTable(table.config.shape, properties: childProps )
            tableView.tag = table.id
            
            var rotation: CGFloat = 0
            if table.config.rotation != nil{
                rotation = CGFloat(Double(StringUtils.removeLastCharacters(table.config.rotation, charactersCount: StringUtils.DEGREE))!)
                tableView.rotateByDegree(Int(rotation), animDuration: 0)
            }
            
            let doubleView = UIView()
            doubleView.frame = rootViewProps
            
            let tableNumberL = createTableLabel(tableView.frame, table: table)
            let upcomingViewContainer = createUpcomingViewContainer(doubleView)
            let guestSizeView = createGuestSizeView(doubleView.frame, type: table.type, isVisibleMaxMinSizes: isVisibleMaxMinSizes)
            
            doubleView.addSubview(tableView)
            doubleView.addSubview(tableNumberL)
            doubleView.addSubview(guestSizeView)
            doubleView.addSubview(upcomingViewContainer)
            
            roomView.addSubview(doubleView)
        }
        
        return roomView
    }
    
    class func createGuestSizeView(frame: CGRect, type: Type, isVisibleMaxMinSizes: Bool) -> UILabel{
        
        let guestSizeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        guestSizeLabel.textAlignment = NSTextAlignment.Center
        guestSizeLabel.text = "\(type.minSeats)-\(type.maxSeats)"
        guestSizeLabel.font = UIFont.systemFontOfSize(8)
        if !isVisibleMaxMinSizes {
            guestSizeLabel.alpha = 0
        }
        return guestSizeLabel
        
    }
    
    class func createChairImageView(view:UIView, imageName: String) -> UIImageView{
        let imageSize:CGFloat = 25
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.tag = Tag.CHAIR_PIC_TAG
        imageView.frame = CGRect(
            x: view.frame.width/2-imageSize/2,
            y: view.frame.height/2-imageSize/2,
            width: imageSize,
            height: imageSize)
        return imageView
    }
    
    class func getRectFromProperties(table: Tables, parentWidth: Double, parentHeight: Double) -> CGRect{
        
        let width = parentWidth * Double(StringUtils.removeLastCharacters(
            table.config.width, charactersCount: StringUtils.PERCENT))! / 100
        let height =  width / Double(table.config.widthToHeightRatio)
        let top =  (parentHeight * Double(StringUtils.removeLastCharacters(
            table.config.top, charactersCount:StringUtils.PERCENT))! / 100 - height/2) * 0.96
        let left =  parentWidth * Double(StringUtils.removeLastCharacters(
            table.config.left, charactersCount:StringUtils.PERCENT))! / 100 - width/2
        
        
        return CGRect(x: left, y: top, width: width, height: height)
    }

    class func createTable(shapeType: ShapeType,properties: CGRect) -> Shape {

        var tableV: Shape!
        
        switch shapeType {
            case .ROUND:
                tableV = Round(frame: properties)
                tableV.layer.masksToBounds = true;
                tableV.layer.cornerRadius = tableV.frame.size.width/2
                break
            case .RECTANGLE:
                tableV = Rectangle(frame: properties)
                tableV.layer.masksToBounds = true;
                tableV.layer.cornerRadius = 5
                break
            default:
                tableV = Square(frame: properties)
                tableV.layer.masksToBounds = true;
                tableV.layer.cornerRadius = 5
                break
        }
        
        let borderWidth: CGFloat = 3;
        
        tableV.backgroundColor = UIColor.whiteColor()
        tableV.layer.borderColor = UIUtils.COLOR_TABLE_BORDER.CGColor
        tableV.layer.borderWidth = borderWidth
        
        return tableV
    }
    
    class func createSeatedTimeView(parentView: UIView, time: Int) -> UILabel{
        let viewHeight = 16
        let viewWidth = 40
        let properties = CGRect(x: Int(parentView.bounds.width) / 2 - viewWidth / 2,
                                y: Int(parentView.bounds.height) / 2 - viewHeight / 2,
                                width: viewWidth, height: viewHeight)
        
        let timeLabel = UILabel(frame: properties)
        timeLabel.tag = Tag.SEATED_TABLE_TIME_VIEW
        timeLabel.backgroundColor = UIUtils.COLOR_TABLE_SEATED_TIME
        timeLabel.textColor = UIColor.whiteColor()
        timeLabel.font = UIFont.systemFontOfSize(10)
        timeLabel.textAlignment = NSTextAlignment.Center
        timeLabel.text = TimeUtils.getSeatedTime(time)
        timeLabel.adjustsFontSizeToFitWidth = true
        timeLabel.layer.masksToBounds = true
        timeLabel.layer.cornerRadius = 2
        
        return timeLabel
    }
    
    class func createUpcomingViewContainer(parentView: UIView) -> UIView{
        let viewHeight = 30
        let viewWidth = 40
        let view = UIView(frame: CGRect(x: Int(parentView.bounds.width) / 2 - viewWidth / 2,
            y: Int(parentView.bounds.height) / 2 - viewHeight / 2,
            width: viewWidth, height: viewHeight))
        view.tag = Tag.UPCOMING_TABLE_CONTAINER
        return view
    }
    
    class func createUpcomingTimeView(parentView: UIView, upcomingOrders: [Order]){
        
        let parentWidth = parentView.bounds.width
        let parentHeight = parentView.bounds.height
        let upcomingTimesCount = (upcomingOrders.count > 1) ? 2 : 1
        var upcomingTimesArr = [NSDate]()
        
        if upcomingTimesCount > 1 {
            upcomingTimesArr.append(TimeUtils.stringToDate(upcomingOrders[0].orderDateTime, dateFormat: YYYYMMDDHHMMSS))
            upcomingTimesArr.append(TimeUtils.stringToDate(upcomingOrders[1].orderDateTime, dateFormat: YYYYMMDDHHMMSS))
        }else{
            upcomingTimesArr.append(TimeUtils.stringToDate(upcomingOrders[0].orderDateTime, dateFormat: YYYYMMDDHHMMSS))
        }
        
        for i in 0 ..< upcomingTimesArr.count {
            
            let timeView: UIView!
            let date = upcomingTimesArr[i]
            
            if upcomingTimesCount == 1 {
                timeView = UIView(frame:CGRect(x: 0, y: Int(parentHeight / 2 - parentHeight / 4),
                    width: Int(parentWidth),
                    height: Int(parentHeight / 2)))
            }else{
                if i == 0 {
                    timeView = UIView(frame:CGRect(x: 0, y: 0, width: Int(parentWidth), height: Int(parentHeight / 2) - 1))
                }else{
                    timeView = UIView(frame:CGRect(x: 0, y: Int(parentHeight / 2) + 1, width: Int(parentWidth), height: Int(parentHeight / 2) - 1))
                }
            }
            
            let timeLabel = UILabel(frame:
                CGRect(x: 0, y: 0, width: Int(timeView.bounds.width), height: Int(timeView.bounds.height)))
            timeLabel.text = TimeUtils.dateToString(date, dateFormat: HMM)
            timeLabel.textAlignment = NSTextAlignment.Center
            timeLabel.font = UIFont.systemFontOfSize(10)
            timeLabel.textColor = UIColor.whiteColor()
            
            timeView.backgroundColor = UIColor.blackColor()
            timeView.addSubview(timeLabel)
            timeView.tag = Tag.UPCOMING_TABLE_TIME_VIEW
            timeView.layer.cornerRadius = 2
            parentView.addSubview(timeView)
            
        }
        
        parentView.superview!.bringSubviewToFront(parentView)
        
    }
    
    class func createTableObjects(currentDateData: CurrentDateDataResponse) -> Dictionary<Int,TableInfo>{
        
        var roomTableListDic = Dictionary<Int,TableInfo>()
        
        for table in currentDateData.floor.tables {
            
            let roomTable: TableInfo = TableInfo()
            
            roomTable.id = table.id
            roomTable.table = table
            roomTableListDic[table.id] = roomTable
        }
        
        roomTableListDic = RoomUtils.updateTableOrders(currentDateData.orders, roomTableDic: roomTableListDic)
        
        return roomTableListDic
    }
    
    class func createTableObjectsWithPosition(roomView: UIView, currentDateData: CurrentDateDataResponse, availableTables: AvailableTables?) -> Dictionary<Int,TableInfo>{
        
        var roomTableListDic = Dictionary<Int,TableInfo>()
        let upcomingOrders = RoomUtils.filterUpcomingOrders(currentDateData.orders)
        
        for table in currentDateData.floor.tables {
            let roomTable: TableInfo = TableInfo()
            let tableView:UIView = (roomView.viewWithTag(table.id))!
            let pointInWindow: CGPoint = tableView.superview!.convertPoint(tableView.frame.origin, toView: nil)
            let pointInScreen: CGPoint = tableView.window!.convertPoint(pointInWindow, toWindow: nil)
            
            let posXMin = pointInScreen.x
            let posYMin = pointInScreen.y
            let posXMax = pointInScreen.x + tableView.frame.width
            let posYMax = pointInScreen.y + tableView.frame.height
            
            for assignmnet in currentDateData.serverAssignments {
                if assignmnet.table.id == table.id {
                    roomTable.assignment = assignmnet
                }
            }
            
            if availableTables != nil{
                if !RoomUtils.isTableAvailable(table, availableTables: availableTables!) ||
                    RoomUtils.isTableSeated(table, availableTables: availableTables!){
                    tableView.backgroundColor = UIUtils.COLOR_GRAY_DARK
                    roomTable.isAvailable = false
                }
                if RoomUtils.isTableWalkIn(table, availableTables: availableTables!){
                    tableView.backgroundColor = UIUtils.COLOR_PINK
                    roomTable.isAvailable = true
                }
            }
            
            roomTable.id = table.id
            roomTable.table = table
            roomTable.position = [TableInfo.X_MIN:posXMin,TableInfo.Y_MIN:posYMin,TableInfo.X_MAX:posXMax,TableInfo.Y_MAX:posYMax]
            roomTable.upcomingList = RoomUtils.getCurrentTableUpcomingOrders(table.id, upcomingOrders: upcomingOrders)
            roomTableListDic[table.id] = roomTable
        }
        
        roomTableListDic = RoomUtils.updateTableOrders(currentDateData.orders, roomTableDic: roomTableListDic)
        
        return roomTableListDic
    }
    
    class func createTableLabel(rect: CGRect, table: Tables) -> UILabel{
        
        let labelH: CGFloat = 14
        let labelW: CGFloat = 40
        let middleX = rect.minX + (rect.maxX - rect.minX) / 2 - labelW / 2

        let nameLabel = UILabel(frame: CGRectMake(middleX, rect.maxY, labelW, labelH))
        nameLabel.textAlignment = NSTextAlignment.Center
        nameLabel.textColor = UIColor.blackColor()
        nameLabel.numberOfLines = 1
        nameLabel.font = UIFont.systemFontOfSize(10)
        nameLabel.text = table.name
        
        if(table.name.containsString("Wtlst")){
            nameLabel.text = table.name.stringByReplacingOccurrencesOfString("Wtlst", withString:"W.")
        }

        return nameLabel;
    }

}