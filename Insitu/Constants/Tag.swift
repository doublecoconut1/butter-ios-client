//
//  Tag.swift
//  Butter
//
//  Created by Grigor Avagyan on 7/27/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

public class Tag {

    public static let CHAIR_PIC_TAG = 10000
    public static let DRAGGABLE_VIEW_TAG = 20000
    
    public static let RESERVATION_TAB: Int = 100;
    public static let UPCOMING_TAB: Int = 101;
    public static let SERVERS_TAB: Int = 102;
    public static let WAITLIST_TAB: Int = 103;
    
    public static let PERSON_INFO_TAG = 123456
    public static let TABLE_INFO_TAG = 12345
    public static let ROOT_PERSON_INFO_TAG = 333333
    
    public static let GUEST_STATUS_TABLE_CELL_LABEL = 778899
    
    public static let UPCOMING_TABLE_CONTAINER = 999999999
    public static let UPCOMING_TABLE_TIME_VIEW = 111111111
    public static let SEATED_TABLE_TIME_VIEW = 77777777
    
    public static let GUEST_PROFILE_NOTE_TAB = 201
    public static let GUEST_PROFILE_PROFILE_TAB = 202
    public static let GUEST_PROFILE_SHARE_TAB = 203
    public static let GUEST_PROFILE_CLOCK_TAB = 204
    
    public static let SPLIT_GUEST_LABEL = 11
    public static let SPLIT_MENU_ERROR_VIEW = 444
    
    public static let SPLIT_MENU_GUEST_FULLNAME_VIEW = 701
    public static let SPLIT_MENU_DATE_VIEW = 702
    public static let SPLIT_MENU_RESERVATION_VIEW = 703
    public static let SPLIT_MENU_GUESTS_VIEW = 704
    public static let SPLIT_MENU_TABLE_VIEW = 705
    public static let SPLIT_MENU_TAGS_VIEW = 707
    public static let SPLIT_MENU_NOTES_VIEW = 708
    
    
}