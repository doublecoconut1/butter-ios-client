//
//  GuestCode.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/19/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

class Constants{
    
    static var GUEST_TAGS = ["Wheelchair Access","VIP","Partner","Employee",
                           "Allergy","Industry","Media","MB VIP","Benu VIP",
                           "Regular","SF Socialite","Purveyor","Friend of CL",
                           "CL Reservation","Captain of Industry","Hotel Number",
                           "Concierge Contact","Complained","$Wine Spender$",
                           "No Corkage $","Stage","Vegetarian","Friend of Employee",
                           "Special Event","Big Spender","Gourmand","SFMOMA employee",
                           "SFMOMA trustee"]
    
    static let RESERVATION_TAGS = ["Birthday", "Anniversary", "Wheelchair Access", "Special Request",
                                 "Sparkling Greeting", "PDR", "CC Authorization", "Menu Header",
                                 "Email", "Graduation", "Highchair", "Server Request", "Special Event",
                                 "VIP L", "VIP M", "VIP H", "VIP FULL COMP", "Employee Discount", "Concierge Res"]
    
    static let VIP_TAGS = ["VIP L", "VIP M", "VIP H", "VIP FULL COMP", "VIP", "MB VIP", "Benu VIP"]
}