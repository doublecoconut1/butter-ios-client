//
//  Images.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/28/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

class Images{
    
    // Statuses
    static let CHAIR = "chair"
    static let PARTIALLY_CHAIR = "partialChair"
    static let PAID = "paid"
    static let BUS = "bus"
    static let CHECK_DROPDOWN = "checkDropdown"
    static let UPCOMING = "upcoming"
    static let DONE = "done"
    static let DESSERT = "dessert"
    static let ENTREE = "entree"
    static let CANCEL = "cancel"
    static let GREEN_PLUS = "plusGreen"
    static let VIP = "VIP"
    
    // Other
    static let UPCOMING_SELECTED = "visibleFilled"
    static let UPCOMING_DESELECTED = "visible"
    static let SEATED_TIME_SELECTED = "watch"
    static let SEATED_TIME_DESELECTED = "watchFilled"
}