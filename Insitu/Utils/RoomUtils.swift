//
//  RoomUtility.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/13/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class RoomUtils{//: UIViewController {

    class func removeViewsWithTag(view: UIView, tag: Int) {
        while view.viewWithTag(tag) != nil {
            view.viewWithTag(tag)!.removeFromSuperview()
        }
    }
    
    class func getStatusPopup(sourceView: UIView) -> GuestStatusTableViewController{
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let statusController = storyboard.instantiateViewControllerWithIdentifier(String(GuestStatusTableViewController)) as! GuestStatusTableViewController
        statusController.modalPresentationStyle = .Popover
        statusController.preferredContentSize = CGSizeMake(400, 800)
        statusController.status = .UPCOMING
        
        let popoverMenuViewController = statusController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .Right
        popoverMenuViewController?.sourceView = sourceView
        popoverMenuViewController?.sourceRect = CGRect(x: 0, y: 0, width: sourceView.frame.width, height: sourceView.frame.height)
        return statusController
    }
    
    class func isTableAvailable(table: Tables, availableTables: AvailableTables) -> Bool{
        for availableTable in availableTables.available {
            if availableTable.id == table.id {
                return true
            }
        }
        return false
    }
    
    class func isTableSeated(table: Tables, availableTables: AvailableTables) -> Bool{
        for availableTable in availableTables.seated {
            if availableTable.id == table.id {
                return true
            }
        }
        return false
    }
    
    class func isTableWalkIn(table: Tables, availableTables: AvailableTables) -> Bool{
        for walkIn in availableTables.walkIn {
            if walkIn.id == table.id {
                return true
            }
        }
        return false
    }
    
    class func removeTablesFromOrders(orders: [Order]) -> [Order]{
        let sameOrders = orders
        for order in sameOrders{
            order.orderedTable = nil
        }
        return sameOrders
    }
    
    class func getSelectedTable(roomTableDic: Dictionary<Int,TableInfo>) -> Tables?{
        for (key, value) in roomTableDic{
            if value.selected {
                return roomTableDic[key]?.table
            }
        }
        return nil
    }
    
    class func unselectAllTables(roomTableDic: Dictionary<Int,TableInfo>) -> Dictionary<Int,TableInfo>{
        for (_,value) in roomTableDic{
            value.selected = false
        }
        return roomTableDic
    }
    
    class func removeOrdersFromAllOrders(removableOrders: [Order], allOrders: [Order]) -> [Order]{
        
        var orders = allOrders
        
        for i in 0..<removableOrders.count{
            for j in 0..<orders.count{
                let currentOrder = orders[j]
                if currentOrder.orderedTable.id == removableOrders[i].orderedTable.id {
                    orders.removeAtIndex(j)
                    break
                }
            }
        }
        return orders
    }
    
    class func startPulsingTables(roomView: UIView, roomTableDic: Dictionary<Int,TableInfo>){
        
        for (_,value) in roomTableDic{
            let tableView = roomView.viewWithTag(value.id)
            UIUtils.addPulseAnimationToView(tableView!)
        }
    }
    
    class func stopPulsingTables(roomView: UIView, roomTableDic: Dictionary<Int,TableInfo>){
        for (_,value) in roomTableDic{
            let tableView = roomView.viewWithTag(value.id)!
            tableView.layer.removeAnimationForKey(AnimationName.ANIMATION_OPACITY.rawValue)
        }
    }
    
    class func getSumOfOrdersGuestCount(orders: [Order]) -> Int{
        var sum = 0
        for order in orders{
            sum += order.guestCount
        }
        return sum
    }
    
    class func onSeatedTimeButtonClick(seatedTimeBtn: UIButton, upcomingBtn: UIButton, roomView: UIView, roomTableListDic: Dictionary<Int,TableInfo>){
        
        if !seatedTimeBtn.selected {
            showSeatedTimes(roomView, roomTableDic: roomTableListDic)
            removeUpcomingTableTime(roomView)
            
            seatedTimeBtn.selected = true
            upcomingBtn.selected = false
        }else{
            removeSeatedTimes(roomView)
            seatedTimeBtn.selected = false
        }
    }
    
    class func onUpcomingButtonClick(seatedTimeBtn: UIButton, upcomingBtn: UIButton, roomView: UIView, roomTableListDic: Dictionary<Int,TableInfo>){
        if !upcomingBtn.selected {
            removeSeatedTimes(roomView)
            showUpcomingTableTime(roomView, roomTableDic: roomTableListDic)
            
            upcomingBtn.selected = true
            seatedTimeBtn.selected = false
        }else{
            removeUpcomingTableTime(roomView)
            upcomingBtn.selected = false
        }
    }
    
    class func getTablesFromOrders(orders: [Order]) -> [Tables]{
        var tables = [Tables]()
        for order in orders{
            tables.append(order.orderedTable)
        }
        return tables
    }
    
    class func getOrderedTablesByParentId(parentId: Int, orders: [Order]) -> [Tables]{
        let groupedOrder = getGroupedOrderByParentId(parentId, orders: orders)
        return getTablesFromOrders(groupedOrder!.childOrderList)
    }
    
    class func getOccupiedTableOrdersByParentId(parentId: Int, tables: [Tables], roomTableDic: Dictionary<Int,TableInfo>) -> [Order]{
        var diffOrderes = [Order]()
        for table in tables {
            let tableInfo = roomTableDic[table.id]
            let tableOrder = tableInfo?.currentOrder
            if tableOrder != nil && (tableOrder!.parentId == nil  || (tableOrder!.parentId != nil && tableOrder!.parentId != parentId)){
                diffOrderes.append(tableOrder!)
            }
        }
        return diffOrderes
    }
    
    class func getGroupedOrderByParentId(parentId: Int, orders: [Order]) -> Order?{
        for order in orders{
            if order.parentId != nil && order.parentId == parentId {
                return order
            }
        }
        return nil
    }
    
    class func getOccupiedTablesFromCurrentTables(tables: [Tables], roomTableDic: Dictionary<Int,TableInfo>) -> [Tables]{
        var seatedTables = [Tables]()
        for table in tables{
            let tableInfo = roomTableDic[table.id]
            if tableInfo?.currentOrder != nil {
                seatedTables.append(table)
            }
        }
        return seatedTables
    }
    
    class func isTableInOrderList(table: Tables, orders: [Order]) -> Bool{
        for order in orders{
            if order.orderedTable.id == table.id {
                return true
            }
        }
        return false
    }
    
    class func showSeatedTimes(roomView: UIView, roomTableDic: Dictionary<Int,TableInfo>){
        
        for (_,value) in roomTableDic{
            if value.currentOrder != nil {
                let view = roomView.viewWithTag(value.id)?.superview
                let dateTimeCalculated = value.currentOrder.dateTimeCalculated
                let seatedDateTime = TimeUtils.stringToDate(value.currentOrder.seatedDateTime, dateFormat: YYYYMMDDHHMMSS)
                let interval = TimeUtils.now().timeIntervalSinceDate(seatedDateTime) / 60
                let intervalTime = (interval < 0) ? 0 : interval
                let time = (dateTimeCalculated != nil && dateTimeCalculated != 0) ? dateTimeCalculated : Int(intervalTime)
                
                view?.addSubview(RoomFactory.createSeatedTimeView(view!, time: time))
            }
        }
    }
    
    class func getSeatedOrdersGuestCount(startIndex: Int, orders: [Order]) -> Int{
        var index = startIndex
        for order in orders{
            if order.fake != nil {
                index += getSeatedOrdersGuestCount(index, orders: order.childOrderList)
            }else if StatusUtils.isSeatedCategory(order.status){
                index += order.guestCount
            }
        }
        return index
    }
    
    class func updateTableOrders(allOrders: [Order], roomTableDic: Dictionary<Int,TableInfo>) -> Dictionary<Int,TableInfo>{
        for order in allOrders{
            if order.fake != nil{
                updateTableOrders(order.childOrderList, roomTableDic: roomTableDic)
            }
            else{
                if StatusUtils.isSeatedCategory(order.status) {
                    roomTableDic[order.orderedTable.id]?.currentOrder = order
                }
            }
        }
        return roomTableDic
    }
    
    class func getTableFromPosition(point: CGPoint, roomTableListDic: Dictionary<Int,TableInfo>) -> TableInfo? {
        
        for (_, tableInfo) in roomTableListDic {
            if point.x > tableInfo.position[TableInfo.X_MIN] && point.x < tableInfo.position[TableInfo.X_MAX] &&
                point.y > tableInfo.position[TableInfo.Y_MIN] && point.y < tableInfo.position[TableInfo.Y_MAX]{
                return tableInfo
            }
        }
        return nil
    }
    
    class func filterUpcomingOrders(allOrders: [Order]) -> [Order] {
        var upcomingOrders = Array<Order>()
        for order in allOrders {
            if order.fake != nil {
                for currOrder in order.childOrderList{
                    if StatusUtils.isUpcomingCategory(currOrder.status) {
                        upcomingOrders.append(order)
                        break
                    }
                }
            }else if StatusUtils.isUpcomingCategory(order.status) {
                upcomingOrders.append(order)
            }
        }
        return upcomingOrders
    }
    
    class func showUpcomingTableTime(roomView: UIView, roomTableDic: Dictionary<Int,TableInfo>){
        for(_,value) in roomTableDic{
            if value.upcomingList.count != 0 {
                let upcomingTableView = roomView.viewWithTag(value.id)?.superview?.viewWithTag(Tag.UPCOMING_TABLE_CONTAINER)
                RoomFactory.createUpcomingTimeView(upcomingTableView!, upcomingOrders: SortingUtils.sortUpcomingOrtdersByDate(value.upcomingList))
            }
        }
    }
    
    class func reloadSeatedTimeInfo(view: UIView, roomTableDic: Dictionary<Int,TableInfo>){
        removeSeatedTimes(view)
        showSeatedTimes(view, roomTableDic: roomTableDic)
    }
    
    class func reloadUpcomingTablesTimeInfo(view: UIView, roomTableDic: Dictionary<Int,TableInfo>){
        removeUpcomingTableTime(view)
        showUpcomingTableTime(view, roomTableDic: roomTableDic)
    }
    
    class func removeUpcomingTableTime(view: UIView) {
        removeViewsWithTag(view, tag: Tag.UPCOMING_TABLE_TIME_VIEW)
    }
    
    class func removeSeatedTimes(view: UIView) {
        removeViewsWithTag(view, tag: Tag.SEATED_TABLE_TIME_VIEW)
    }
    
    class func setupGroupedCell(tableView: UITableView, index: Int, order: Order) -> GroupedOrderViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("groupedCellId") as! GroupedOrderViewCell
        let color = getOrderTypeColor(order)
        
        let upcomingOrder = getFirstUpcomingOrder(order.childOrderList)
        let fullName = StringUtils.getFullNameFromUser(upcomingOrder.user)
        let guestCount = RoomUtils.getSumOfOrdersGuestCount(order.childOrderList)
        let globalStatus = upcomingOrder.status
        let statusImage = UIImage(named: StatusUtils.getStatusImage(globalStatus))
        
        cell.nameLabel.text = fullName
        cell.guestCountLabel.text = String(guestCount)
        cell.orders = order.childOrderList
        cell.parentCellIndex = index
        cell.tableNumberLabel.text = upcomingOrder.orderedTable.name + "+"
        cell.timeLabel.text = TimeUtils.getHourAndMinutesFromDateInString(upcomingOrder.orderDateTime)
        cell.statusIcon.setImage(statusImage, forState: UIControlState.Normal)
        cell.statusLabel.text = globalStatus.rawValue
        
        cell.gropedCellContentView.backgroundColor = color
        cell.vipImageView.hidden = !isOrderVip(order)
        cell.vipImageView.superview!.backgroundColor = color
        cell.statusIcon.superview!.backgroundColor = color
        cell.selectionStyle = .None
        
        return cell
    }
    
    class func setupWaitlistCell(tableView: UITableView, waitList: WaitList) -> WaitListViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("waitListCellId") as! WaitListViewCell
        
        cell.nameLabel.text = StringUtils.getFullNameFromUser(waitList.user)
        cell.timeLabel.text = TimeUtils.getHourAndMinutesFromDateInString(waitList.orderDateTime)
        cell.guestCountLabel.text = String(waitList.guestCount)
        return cell
    }
    
    class func setupReservationCell(tableView: UITableView, order: Order) -> ReservationViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("reservationCellId") as! ReservationViewCell
        let color = getOrderTypeColor(order)
        
        cell.tabelNumberLabel.text = String(order.orderedTable.name)
        cell.guestCountLabel.text = String(order.guestCount)
        cell.nameLabel.text = StringUtils.getFullNameFromUser(order.user)
        cell.statusLabel.text = String(order.status)
        
        cell.timeLabel.text = TimeUtils.getHourAndMinutesFromDateInString(order.orderDateTime)
        cell.statusImageView.image = UIImage(named: StatusUtils.getStatusImage(order.status))
        
        cell.VIPImageView.hidden = !isOrderVip(order)
        cell.VIPImageView.superview?.backgroundColor = color
        cell.statusImageView.superview?.backgroundColor = color
        cell.backgroundColor = color
        
        return cell
    }
    
    class func setupServerCell(tableView: UITableView, server: Server) -> ServersViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("serversCellId") as! ServersViewCell
        let fullName: String = ((server.lastName != nil) ? " \(server.lastName)" : "") + ((server.firstName != nil) ? " \(server.firstName)" : "")
        UIUtils.changeImageViewColor(cell.serverIcon, color: UIColor().HexToColor(server.color, alpha: 1.0))
        cell.serverName.text = fullName;
        cell.coversCurr.text = String(server.coverCurrent)
        cell.coversTotal.text = String(server.coverTotal)
        return cell
    }
    
    class func getOrderTypeColor(order: Order) -> UIColor {
        let orders = order.fake != nil ? order.childOrderList : [order]
        for currOrder in orders {
            if currOrder.notes.count > 0 {
                return UIColor.redColor()
            } else if currOrder.user.notes.count > 0 {
                return UIColor.yellowColor()
            }
        }
        return UIColor.whiteColor()
    }
    
    class func isOrderVip(order: Order) -> Bool{
        let orders = order.fake != nil ? order.childOrderList : [order]
        for currOrder in orders {
            if isVip(currOrder.codes) || isVip(currOrder.user.codes){
                return true
            }
        }
        return false
    }
    
    class func isVip(codes: [Code]) -> Bool {
        for code in codes {
            let index = Constants.VIP_TAGS.indexOf(code.name)
            if index != nil {
                return true
            }
        }
        return false
    }
    
    class func getFirstUpcomingOrder(orders: [Order]) -> Order{
        var firstOrder: Order = orders[0]
        
        for order in orders {
            let dateTime = TimeUtils.orderDateTimeToNSDate(order.orderDateTime)
            let firstOrderDateTime = TimeUtils.orderDateTimeToNSDate(firstOrder.orderDateTime)
            if dateTime < firstOrderDateTime {
                firstOrder = order
            }
        }
        
        return firstOrder
    }
    
    class func getCurrentTableUpcomingOrders(tableId: Int, upcomingOrders: [Order]) -> [Order] {
        var orderList: [Order] = [Order]()
        for order in upcomingOrders {
            if order.fake != nil {
                for currOrder in order.childOrderList {
                    if currOrder.orderedTable.id == tableId{
                        orderList.append(currOrder)
                    }
                }
            }
            else if order.orderedTable.id == tableId{
                orderList.append(order)
            }
        }
        return orderList
    }
}
