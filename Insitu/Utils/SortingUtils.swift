//
//  SortingUtils.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/5/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import GameplayKit


public class SortingUtils{

    class func sortUpcomingOrtdersByDate(orders: [Order]) -> [Order]{
        
        var upcomingOrders = orders
        var sortedOrders = [Order]()
        var removableIndex = 0
        var firstDate : NSDate = TimeUtils.stringToDate(upcomingOrders[0].orderDateTime, dateFormat: YYYYMMDDHHMMSS)
        
        while upcomingOrders.count != 0 {
            for i in 0 ..< upcomingOrders.count {
                if i + 1 != upcomingOrders.count {
                    let nextDate = TimeUtils.stringToDate(upcomingOrders[i + 1].orderDateTime, dateFormat: YYYYMMDDHHMMSS)
                    switch firstDate.compare(nextDate) {
                    case .OrderedAscending:
                        break
                    case .OrderedDescending:
                        firstDate = nextDate
                        removableIndex = i + 1
                        break
                    default:
                        break
                    }
                }else if(upcomingOrders.count - 1 == 0){
                    removableIndex = i
                }
            }
            sortedOrders.append(upcomingOrders[removableIndex])
            upcomingOrders.removeAtIndex(removableIndex)
            if upcomingOrders.count != 0 {
                firstDate = TimeUtils.stringToDate(upcomingOrders[0].orderDateTime, dateFormat: YYYYMMDDHHMMSS)
            }
            
            removableIndex = 0
        }
        return sortedOrders
    }
    
}