//
//  UiUtils.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/12/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import QuartzCore

class UIUtils{
    
    //global text color #73838B
    
    static let COLOR_GREEN = UIColor(red:57/255.0, green:149/255.0, blue:132/255.0, alpha: 1) // hex #399584
    static let COLOR_GRAY = UIColor(white:0.94, alpha:1.0);
    static let COLOR_GRAY_DARK = UIColor.grayColor()
    static let COLOR_GRAY_LIGHT = UIColor(red:200/255.0, green:209/255.0, blue:216/255.0, alpha: 1)//#C8D1D8
    static let COLOR_DEF_BLUE = UIColor(red:0.0,green:122.0/255.0,blue:1.0,alpha:1.0)
    static let COLOR_TABLE_SEATED_TIME = UIColor(red:169/255.0, green:63/255.0, blue:191/255.0, alpha: 1) // #A93FBF
    static let COLOR_PINK = UIColor(red:240/255.0, green:87/255.0, blue:147/255.0, alpha: 1)//#F05793
    
    static let COLOR_ROOM = UIColor(red:200/255.0, green:209/255.0, blue:216/255.0, alpha: 1) // #C8D1D8
    static let COLOR_TABLE_BORDER = UIColor(red:227/255.0, green:229/255.0, blue:235/255.0, alpha: 1)
    static let COLOR_TABLE_BORDER_DARK = UIColor(red:181/255.0, green:183/255.0, blue:188/255.0, alpha: 1) //#B5B7BC
    static let COLOR_CHAIR = UIColor(red:240/255.0, green:87/255.0, blue:147/255.0, alpha: 1)
    static let COLOR_BUTTON_BORDER_BG = UIColor(red:233/255.0, green:239/255.0, blue:242/255.0, alpha: 1) // #E9EFF2
    static let COLOR_TAB_PRESSED_BACKGROUND = UIColor(red:65/255.0, green:91/255.0, blue:103/255.0, alpha: 1)
    static let COLOR_TAB_DEFAULT_BACKGROUND = UIColor(red:245/255.0, green:249/255.0, blue:252/255.0, alpha: 1) // #F5F9FC
    static let COLOR_TAB_DEFAULT_ICON = UIColor(red:72/255.0, green:88/255.0, blue:97/255.0, alpha: 1)
    static let COLOR_SPLITVIEW_TABLEVIEW_BORDER = UIColor(red:216/255.0, green:221/255.0, blue:225/255.0, alpha: 1)
    static let COLOR_TITLE = UIColor(red:137/255.0, green:148/255.0, blue:153/255.0, alpha: 1)
    static let COLOR_DEFAULT_NAVBAR = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1)
    static let COLOR_RED_ERROR = UIColor(red:227/255.0, green:69/255.0, blue:76/255.0, alpha: 1) //#E3454C
    static let COLOR_SEARCH_BAR = UIColor(red:200/255.0, green:200/255.0, blue:205/255.0, alpha: 1)
    
    static let serverServerColors: [String] = ["#E13F85", "#75C060", "#58B6FD", "#EA8200", "#4CAC9E",
                                               "#8251CF", "#EB6857", "#D5E900", "#A93EC9", "#E5D800",
                                               "#5D77DE", "#E08DA3", "#69C7C6", "#E4BC00", "#C85057",
                                               "#8EDCB5", "#D595EC", "#96A711"]
    
    class func setColorToBorder(view: UIView, color: UIColor) {
        view.layer.borderColor = color.CGColor
    }
    
    class func hexToUIColor(color: String) -> UIColor {
        return UIColor().HexToColor(color)
    }
    
    class func transformViewToPosition(view: UIView, point: CGPoint) {
        view.transform = CGAffineTransformMakeTranslation(point.x,point.y)
    }
    
    class func changeImageViewColor(imageView: UIImageView, color: UIColor){
        imageView.image = imageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imageView.tintColor = color
    }
    
    class func dismissKeyboard(view: UIView) {
        view.endEditing(true)
    }
    
    class func getControllerFromStoryboard(viewController: UIViewController, controllerName: String) -> UIViewController{
        let storyBoard: UIStoryboard = viewController.storyboard!
        let object = storyBoard.instantiateViewControllerWithIdentifier(String(controllerName))
        
        if (object as? UINavigationController) != nil {
            let controller = (object as! UINavigationController).topViewController! as UIViewController
            return controller
        }
        return object
        
    }
    
    class func addPulseAnimationToView(view: UIView){
        let pulseAnimation = CABasicAnimation(keyPath: "borderColor")
        pulseAnimation.duration = 0.8
        pulseAnimation.fromValue = UIUtils.COLOR_TABLE_BORDER.CGColor
        pulseAnimation.toValue = UIUtils.COLOR_TABLE_BORDER_DARK.CGColor
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = FLT_MAX
        view.layer.addAnimation(pulseAnimation, forKey: AnimationName.ANIMATION_OPACITY.rawValue)
    }

}

public enum AnimationName : String{
    case ANIMATION_OPACITY = "animateOpacity"
}

extension CALayer {
    
    func setBorderColorFromUIColor(color: UIColor) {
        self.borderColor = color.CGColor
    }
}

extension UIView{
    func rotateByDegree(degree: Int, animDuration: Double){
        UIView.animateWithDuration(animDuration, animations:{
            self.transform = CGAffineTransformMakeRotation(CGFloat(degree.degreesToRadians))
        })
    }
}

extension UIColor{
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        
        let hexint = Int(self.intFromHexString(hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        let scanner: NSScanner = NSScanner(string: hexStr)
        scanner.charactersToBeSkipped = NSCharacterSet(charactersInString: "#")
        scanner.scanHexInt(&hexInt)
        return hexInt
    }
}