//
//  TimeUtils.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/14/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

public let MMDDYYYY: String = "MM/dd/yyyy"
public let YYYYMMDD: String = "yyyy-MM-dd"
public let YYYYMMDDHHMMSS: String = "yyyy-MM-dd HH:mm:ss"
public let MMMDD : String = "MMM dd"
public let HMM: String = "h:mm"
public let HHMMSS: String = "HH:mm:ss"
public let HHMM: String = "HH:mm"
public let MMMMYYYY: String = "MMMM, YYYY"

class TimeUtils {
    
    static let dateFormatter = NSDateFormatter()
    
    class func dateToString(date: NSDate, dateFormat: String) -> String{
        let formatter = getDateFormatterWithTimeZone()
        formatter.dateFormat = dateFormat
        return formatter.stringFromDate(date)
    }
    
    class func stringToDate(date: String, dateFormat: String) -> NSDate{
        let formatter = getDateFormatterWithTimeZone()
        formatter.dateFormat = dateFormat;
        return formatter.dateFromString(date)!;
    }
    
    class func getHourAndMinutesFromDateInString(date: String) -> String{
        let date = stringToDate(date, dateFormat: YYYYMMDDHHMMSS)
        return dateToString(date, dateFormat: HMM)
    }
    
    class func getHourAndMinutesFromDateInString(date: String, dateFormat: String, timeFormat: String) -> String{
        let date = stringToDate(date, dateFormat: dateFormat)
        return dateToString(date, dateFormat: timeFormat)
    }
    
    class func getMonthAndDayFromDateInString(date: String) -> String{
        return dateToString(stringToDate(date, dateFormat: YYYYMMDDHHMMSS), dateFormat: MMMDD)
    }
    
    class func getYearMonthDayInString(date: String) -> String{
        return dateToString(stringToDate(date, dateFormat: YYYYMMDDHHMMSS), dateFormat: YYYYMMDD)
    }

    class func getHourMinuteSecondInString(date: String) -> String{
        return dateToString(stringToDate(date, dateFormat: YYYYMMDDHHMMSS), dateFormat: HHMMSS)
    }
    
    class func getDateFormatterWithTimeZone() -> NSDateFormatter{
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        return dateFormatter
    }
    
    class func now()-> NSDate{
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Year, .Month, .Day, .Hour, .Minute], fromDate: NSDate())
        components.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        return calendar.dateFromComponents(components)!
    }
    
    class func getCurrTimeInSeconds() -> Double{
        return NSDate().timeIntervalSince1970
    }
    
    class func orderDateTimeToNSDate(orderDateTime: String) -> NSDate{
        return stringToDate(orderDateTime, dateFormat: YYYYMMDDHHMMSS)
    }
    
    class func getNearestTimeFromInterval(timeRange: [NSDate]) -> NSDate{
        
        let timeNow = now()
        
        for time in timeRange {
            if time > timeNow{
                return time
            }
        }
        return timeRange[0]
    }
    
    class func addMonthCountToCurrentDate(monthCount: Int) -> NSDate{
        let cal = NSCalendar.currentCalendar()
        return cal.dateByAddingUnit(.Month, value: 1, toDate: now(), options: [])!
    }
    
    class func isSameDate(firstDate: NSDate, secondDate: NSDate) -> Bool{
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        let firstComponents = calendar.components([.Year, .Month, .Day], fromDate: firstDate)
        let secondComponents = calendar.components([.Year, .Month, .Day], fromDate: secondDate)
        
        if firstComponents.year != secondComponents.year || firstComponents.month != secondComponents.month || firstComponents.day != secondComponents.day{
            return false
        }
        return true
    }
    
    class func differenceOfDatesInMinutes(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: NSDate(), options: []).minute
    }
    
    class func getSeatedTime(minutes: Int) -> String {
        
        let hours = minutes / 60
        let mins = minutes % 60
        
        return (hours != 0) ? "\(hours)h \(mins)m" : "\(mins)m"
    }
    
    class func changeDayOfDate(currentDate: NSDate, toDate: NSDate) -> NSDate{
        
        let calendar = NSCalendar.currentCalendar()
        
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        let firstComp = calendar.components([.Day], fromDate: toDate)
        let secondComp = calendar.components([.Year, .Month, .Hour, .Minute], fromDate: currentDate)
        
        secondComp.day = firstComp.day
        
        return calendar.dateFromComponents(secondComp)!
    }
    
    class func isFirstDateMonthBeforeSecondDateMonth(first: NSDate, second: NSDate)-> Bool{
        let calendar = NSCalendar.currentCalendar()
        let firstComp = calendar.components([.Month], fromDate: first)
        let secondComp = calendar.components([.Month], fromDate: second)
        if firstComp.month < secondComp.month {
            return true
        }else{
            return false
        }
    }
    
    class func isFirstDateTimeBeforeSecondDateTime(first: NSDate, second: NSDate)-> Bool{
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        let firstComp = calendar.components([.Hour, .Minute], fromDate: first)
        let secondComp = calendar.components([.Hour, .Minute], fromDate: second)
        
        if firstComp.hour < secondComp.hour {
            return true
        }else if(firstComp.hour == secondComp.hour){
            if firstComp.minute < secondComp.minute {
                return true
            }else{
                return false
            }
        }
        
        return false
    }
    
    class func getArrayFromTimeInterval(startTime: String, endTime: String) -> [NSDate]{
        var timeArr = [NSDate]()
        let startTimeArr = startTime.componentsSeparatedByString(":")
        let endTimeArr = endTime.componentsSeparatedByString(":")
        
        let nowTime = now()
        
        var startDate = changeHourAndMinuteOfDay(nowTime, hour: Int(startTimeArr[0])!, minute: Int(startTimeArr[1])!)
        let endDate = changeHourAndMinuteOfDay(nowTime, hour: Int(endTimeArr[0])!, minute: Int(endTimeArr[1])!)
        
        while startDate != endDate {
            timeArr.append(startDate)
            startDate = startDate.dateByAddingTimeInterval(15 * 60)
        }
        
        timeArr.append(startDate)
        return timeArr
    }
    
    class func changeHourAndMinuteOfDay(date: NSDate, hour: Int, minute: Int) -> NSDate{
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        let components = calendar.components([.Year, .Month, .Day], fromDate: date)
        components.hour = hour
        components.minute = minute
        return calendar.dateFromComponents(components)!
    }

}

func <=(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.timeIntervalSince1970 <= rhs.timeIntervalSince1970
}
func >=(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.timeIntervalSince1970 >= rhs.timeIntervalSince1970
}
func >(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.timeIntervalSince1970 > rhs.timeIntervalSince1970
}
func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.timeIntervalSince1970 < rhs.timeIntervalSince1970
}
func ==(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.timeIntervalSince1970 == rhs.timeIntervalSince1970
}

