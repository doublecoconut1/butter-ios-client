//
//  StringUtils.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/23/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

public class StringUtils{
    public static let DEGREE:Int = 3;
    public static let PERCENT:Int = 1;

    class func removeLastCharacters(text: String, charactersCount: Int) -> String{
        
        let index = text.endIndex.advancedBy(-(charactersCount))
        return text.substringToIndex(index)
    }
    
    class func parseCodesToStringWithCommas(notes: [Code]) -> String{
        
        return notes.map{($0).name}.joinWithSeparator(", ")
    }
    
    class func parseNotesToStringWithCommas(notes: [Note]) -> String{
        
        return notes.map{($0).name}.joinWithSeparator(", ")
    }
    
    class func isEmpty(text: String) -> Bool {
        return text.isEmpty || (text.characters.split(" ").map(String.init)).count == 0 
    }
    
    class func removeUnnecessaryCharsFromStartAndEnd(text: String) -> String{
        return text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
    class func stringContainsChar(text: String, searchText: String) -> Bool{
        return text.lowercaseString.rangeOfString(searchText.lowercaseString) != nil
    }
    
    class func stringContainsCharAndIsFromFirstIndex(text: String, searchText: String) -> Bool {
        let startIndex = text.lowercaseString.rangeOfString(searchText.lowercaseString)?.startIndex
        return startIndex != nil && text.lowercaseString.startIndex.distanceTo(startIndex!) == 0
    }
    
    class func getFullNameFromUser(user: User)-> String{
        let defaultName = "ANONYMOUS"
        
        let firstName = !user.firstName.isEmpty ? user.firstName : ""
        let lastName = !user.lastName.isEmpty ? user.lastName : ""
        return (firstName.isEmpty && lastName.isEmpty) ? defaultName : firstName + " \(lastName)"
    }
    
    class func objectNameContainsChar(object: AnyObject, searchText: String) -> Bool{
        
        let firstName = (object is User) ? (object as! User).firstName : (object as! Server).firstName
        let lastName = (object is User) ? (object as! User).lastName : (object as! Server).lastName
        let fullName: String? = (object is User) ? (object as! User).fullName : nil
        
        let firstNameHasChar = StringUtils.stringContainsCharAndIsFromFirstIndex(firstName, searchText: searchText)
        var lastNameHasChar : Bool = false
        var fullNameHasChar : Bool = false
        
        if lastName != nil && !lastName!.isEmpty {
            lastNameHasChar = StringUtils.stringContainsCharAndIsFromFirstIndex(lastName!, searchText: searchText)
        }
        
        if fullName != nil && !fullName!.isEmpty {
            fullNameHasChar = StringUtils.stringContainsCharAndIsFromFirstIndex(fullName!, searchText: searchText)
        }
        
        return firstNameHasChar || lastNameHasChar || fullNameHasChar
    }
}

extension String {
    var first: String {
        return String(characters.prefix(1))
    }
    var last: String {
        return String(characters.suffix(1))
    }
    
    var uppercaseFirst: String {
        return first.uppercaseString + String(characters.dropFirst())
    }
}

