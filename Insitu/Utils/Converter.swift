//
//  Converter.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/24/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

class Converter{
    //Todo
}

extension Int {
    var degreesToRadians: Double { return Double(self) * M_PI / 180 }
    var radiansToDegrees: Double { return Double(self) * 180 / M_PI }
}