//
//  GuestStatusUtils.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/3/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

public class StatusUtils{

    public static var  section1: Array<GuestStatus> {
        return [
            GuestStatus.CANCELLED,
            GuestStatus.NO_SHOW
        ]
    }
    public static var  section2: Array<GuestStatus> {
        return [GuestStatus.NOT_CONFIRMED,
                GuestStatus.CONFIRMED,
                GuestStatus.LEFT_MESSAGE,
                GuestStatus.UNKNOWN,
                GuestStatus.WRONG_NUMBER,
                GuestStatus.LATE]
    }
    public static var section3: Array<GuestStatus> {
        return [GuestStatus.PARTIALLY_ARRIVED,
                GuestStatus.ALL_ARRIVED,
                GuestStatus.SEND_TEXT]
    }
    public static var section4: Array<GuestStatus> {
        return [
                GuestStatus.PARTIALLY_SEATED,
                GuestStatus.SEATED,
                GuestStatus.ENTREE,
//                GuestStatus.CLEARED,
                GuestStatus.DESERT,
//                GuestStatus.VALET,
                GuestStatus.CHECK_DROPPED,
                GuestStatus.PAID,
                GuestStatus.BUS_TABLE,
                GuestStatus.DONE]
    }
    
    class var allSection: Array<Array<GuestStatus>> {
        return [section1,section2,section3,section4]
    }
    
    class func getIndexPath(status: GuestStatus) -> NSIndexPath? {
        
        for i in 0 ..< allSection.count{
            var currentSection: Array<GuestStatus> = allSection[i]
            for j in 0 ..< currentSection.count{
                if currentSection[j] == status {
                    return NSIndexPath(forRow: j, inSection: i)
                }
            }
        }
        return nil
    }
    
    class func isCancelledCategory(status: GuestStatus) -> Bool {
        if status == GuestStatus.NO_SHOW || status == GuestStatus.CANCELLED {
            return true
        }
        return false
    }
    
    class func isSeatedCategory(status: GuestStatus) -> Bool{
        if status == GuestStatus.SEATED ||
            status == GuestStatus.DESERT ||
            status == GuestStatus.ENTREE ||
            status == GuestStatus.PARTIALLY_SEATED ||
            status == GuestStatus.CHECK_DROPPED ||
            status == GuestStatus.PAID ||
            status == GuestStatus.BUS_TABLE {
            return true
        }
        return false
    }
    
    class func isUpcomingCategory(status: GuestStatus) -> Bool{
        if status == GuestStatus.UPCOMING ||
            status == GuestStatus.PARTIALLY_ARRIVED{
            return true
        }
        return false
    }
    
    class func isWaitListCategory(category: OrderCategory) -> Bool{
        if category == .WAITLIST || category == .EXISTING_WAITLIST{
            return true
        }
        return false
    }
    
    class func getStatusImage(status: GuestStatus) -> String{
        switch status {
        case .UPCOMING,.CONFIRMED,.NOT_CONFIRMED,.LEFT_MESSAGE,.UNKNOWN,.WRONG_NUMBER,.LATE,.PARTIALLY_ARRIVED,.ALL_ARRIVED,.SEND_TEXT:
            return Images.UPCOMING
        case .ENTREE:
            return Images.ENTREE
        case .DONE:
            return Images.DONE
        case .DESERT:
            return Images.DESSERT
        case .CANCELLED, .NO_SHOW:
            return Images.CANCEL
        case .PARTIALLY_SEATED:
            return Images.PARTIALLY_CHAIR
        case .CHECK_DROPPED:
            return Images.CHECK_DROPDOWN
        case .PAID:
            return Images.PAID
        case .BUS_TABLE:
            return Images.BUS
        default:
            return Images.CHAIR
        }
    }
}