//
//  GuestHistoryViewCell.swift
//  Butter
//
//  Created by Harut on 8/31/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class GuestHistoryViewCell: UITableViewCell {
    
    @IBOutlet weak var pastDateLabel: UILabel!
    @IBOutlet weak var pastTimeLabel: UILabel!
    @IBOutlet weak var pastGuestsCountLabel: UILabel!
    @IBOutlet weak var pastTableLabel: UILabel!
    @IBOutlet weak var pastDurationLabel: UILabel!
}
