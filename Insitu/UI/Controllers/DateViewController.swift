//
//  DateViewController.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/14/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class DateViewController: UIViewController {
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var currentDate: NSDate!
    var delegate: SendDataDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.timeZone = NSTimeZone(name: "UTC")
        datePicker.setDate(currentDate, animated: false)
    }
    
    @IBAction func onDateChanged(sender: UIDatePicker) {
        delegate.onComplete(String(DateViewController), object: sender.date)
    }
    
}
