//
//  Butter
//
//  Created by Grigor Avagyan on 8/7/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit
protocol SplitMenuEventListener {
    func onTableItemClicked(controllerName: String)
    func onAddReservationButtonClick()
    func onRemoveUserInfoBtnClick()
    func onNewReservationPopupClosed()
    func onGuestNameButtonClick()
}

class SplitViewMenuController: UIViewController {
    
    
    @IBOutlet weak var guestNameButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reservationTimeLabel: UILabel!
    @IBOutlet weak var guestCountLabel: UILabel!
    @IBOutlet weak var tableNumberLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var addReservationButton: UIButton!
    @IBOutlet weak var removeUserInfoButton: UIButton!
    @IBOutlet weak var errorTableCell: UIView!

    
    @IBOutlet weak var reservationTagsLabel: UILabel!
    @IBOutlet weak var reservationNotesLabel: UILabel!
    
    var controllerTagArr = [Tag.SPLIT_MENU_GUEST_FULLNAME_VIEW, Tag.SPLIT_MENU_DATE_VIEW, Tag.SPLIT_MENU_RESERVATION_VIEW,
                            Tag.SPLIT_MENU_GUESTS_VIEW, Tag.SPLIT_MENU_TABLE_VIEW, Tag.SPLIT_MENU_TAGS_VIEW, Tag.SPLIT_MENU_NOTES_VIEW]
    
    var fullName: String = ""
    var date: String = ""
    var reservationTime: String = ""
    var guestCount: String = ""
    var tableNumber: String = ""
    var isAddButtonEnabled = true
    var orderCategory: OrderCategory!

    var reservationNotes = [Note]()
    var reservationCodes = [Code]()
    
    var delegate: SplitMenuEventListener!
    
    override func viewDidLoad() {
        errorView.hidden = true
        setupViewClickListeners()
        insertTableInfo()
    }
    
    @IBAction func onGuestNameButtonClick(sender: UIButton) {
       delegate.onGuestNameButtonClick()
    }
    @IBAction func onRemoveUserInfoBtnClick(sender: UIButton) {
        delegate.onRemoveUserInfoBtnClick()
    }
    
    @IBAction func onAddReservationButton(sender: UIButton) {
        delegate.onAddReservationButtonClick()
    }
    
    @IBAction func onCancelButtonClick(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
        delegate.onNewReservationPopupClosed()
    }
    
    func setupViewClickListeners() {
        for tag in controllerTagArr{
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onTableItemClick(_:)))
            self.view.viewWithTag(tag)?.addGestureRecognizer(tapGesture)
        }
    }
    
    func onTableItemClick(gesture: UITapGestureRecognizer){
        
        let tag = gesture.view!.tag
        
        switch tag {
        case Tag.SPLIT_MENU_GUEST_FULLNAME_VIEW:
            delegate.onTableItemClicked(String(SplitViewGuestProfileController))
            break
        case Tag.SPLIT_MENU_DATE_VIEW:
            delegate.onTableItemClicked(String(SplitViewCalendarController))
            break
        case Tag.SPLIT_MENU_RESERVATION_VIEW:
            delegate.onTableItemClicked(String(SplitViewReservationTimeController))
            break
        case Tag.SPLIT_MENU_GUESTS_VIEW:
            delegate.onTableItemClicked(String(SplitViewGuestCountController))
            break
        case Tag.SPLIT_MENU_TABLE_VIEW:
            delegate.onTableItemClicked(String(SplitViewTableController))
            break
        case Tag.SPLIT_MENU_TAGS_VIEW:
            delegate.onTableItemClicked(String(SplitViewTagController))
            break
        case Tag.SPLIT_MENU_NOTES_VIEW:
            delegate.onTableItemClicked(String(SplitViewNoteController))
            break
        default:
            break
        }
    }
    
    func insertTableInfo(){
        if !fullName.isEmpty {
            guestNameButton.setTitle(fullName, forState: .Normal)
            guestNameButton.setTitleColor(UIUtils.COLOR_GREEN, forState: .Normal)
        }
        if !date.isEmpty{
            dateLabel.text = date
        }
        if !reservationTime.isEmpty{
            reservationTimeLabel.text = reservationTime
            reservationTimeLabel.textColor = UIUtils.COLOR_GREEN
        }
        if !guestCount.isEmpty {
            guestCountLabel.text = guestCount
            guestCountLabel.textColor = UIUtils.COLOR_GREEN
        }
        if !tableNumber.isEmpty {
            tableNumberLabel.text = tableNumber
            tableNumberLabel.textColor = UIUtils.COLOR_GREEN
        }
        if StatusUtils.isWaitListCategory(orderCategory){
            tableNumberLabel.text = "You can't choose table"
        }
        
        updateReservationLabelsInfo()
        changeAddButtonAccessibility(isAddButtonEnabled)
    }
    
    func updateReservationLabelsInfo(){
        reservationTagsLabel.text = StringUtils.parseCodesToStringWithCommas(reservationCodes)
        reservationNotesLabel.text = StringUtils.parseNotesToStringWithCommas(reservationNotes)
    }
    
    func errorViewHidden(isHidden: Bool){
        errorView.hidden = isHidden
    }
    
    func addTableName(tableInfo: TableInfo){
        
        var tableNames = tableNumberLabel.text!
        
        if (tableNames.containsString("Select a Table")){
            tableNames = ""
            tableNumberLabel.text = ""
        }
        
        if tableNames.isEmpty {
            tableNames = "\(tableInfo.table.name)"
        }else{
            tableNames = tableNames + ", \(tableInfo.table.name)"
        }
        tableNumberLabel.text = tableNames
        tableNumberLabel.textColor = UIUtils.COLOR_GREEN
    }
    
    func changeAddButtonAccessibility(isEnabled: Bool){
        
        if isEnabled {
            addReservationButton.enabled = true
            addReservationButton.backgroundColor = UIUtils.COLOR_GREEN
        }else{
            addReservationButton.enabled = false
            addReservationButton.backgroundColor = UIUtils.COLOR_SEARCH_BAR
        }
    }
    
    func removeTableName(tableInfo: TableInfo){
        
        var tableNames = tableNumberLabel.text
        let namesArr = tableNames!.componentsSeparatedByString(", ")
        let index = namesArr.indexOf(tableInfo.table.name)
        
        if index == 0 {
            if namesArr.count == 1 {
                tableNames = tableNames!.stringByReplacingOccurrencesOfString(tableInfo.table.name, withString: "")
            }else{
                tableNames = tableNames!.stringByReplacingOccurrencesOfString("\(tableInfo.table.name), ", withString: "")
            }
        }else{
            tableNames = tableNames!.stringByReplacingOccurrencesOfString(", \(tableInfo.table.name)", withString: "")
        }
        
        tableNumberLabel.text = tableNames
    }
}
