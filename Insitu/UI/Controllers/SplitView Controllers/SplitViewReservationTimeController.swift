//
//  SplitViewReservationController.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/8/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol SplitViewReservationTimeEventListener {
    func onReservationTimeChange(date: NSDate)
}

class SplitViewReservationTimeController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var reservationTimeTable: UITableView!
    
    var isToday: Bool = true
    var orderDateTime: NSDate!
    var todayDate = TimeUtils.now()
    var delegate: SplitViewReservationTimeEventListener!
    var orderCategory: OrderCategory!
    var timeRange: [NSDate]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isToday = TimeUtils.isSameDate(todayDate, secondDate: orderDateTime)
        reservationTimeTable.delegate = self
        reservationTimeTable.dataSource = self
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeRange.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        let date = timeRange[indexPath.row]
        
        cell.textLabel!.textAlignment = NSTextAlignment.Center
        cell.textLabel?.text = TimeUtils.dateToString(date, dateFormat: HMM)

        if isToday && date <= todayDate{
            cell = disableCellClick(cell)
        }
        
        return cell
    }
    
    func disableCellClick(cell: UITableViewCell) -> UITableViewCell{
        cell.textLabel?.textColor = UIUtils.COLOR_SEARCH_BAR
        cell.userInteractionEnabled = false
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell!.tintColor = UIUtils.COLOR_GREEN
        cell!.textLabel?.textColor = UIUtils.COLOR_GREEN
        cell!.accessoryType = .Checkmark
        cell!.layoutMargins = UIEdgeInsetsMake(0, 40, 0, 10)
        
        delegate.onReservationTimeChange(timeRange[indexPath.row])
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if cell != nil {
            cell?.tintColor = nil
            cell?.textLabel?.textColor = nil
            cell!.accessoryType = .None
            cell!.layoutMargins = UIEdgeInsetsMake(0, 10, 0, 10);
        }
    }

}























