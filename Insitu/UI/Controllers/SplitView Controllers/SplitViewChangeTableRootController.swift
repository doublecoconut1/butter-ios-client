//
//  SplitViewChangeTableController.swift
//  Butter
//
//  Created by Grigor Avagyan on 10/22/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol SplitViewChangeTableEventListener {
    func getUpdate()
}

class SplitViewChangeTableRootController: UIViewController, SplitViewTableEventListener, ChangeTableEventListener{
    
    var delegate: SplitViewChangeTableEventListener!
    var currentDateData: CurrentDateDataResponse!
    var availableTables: AvailableTables!
    var changeTableController: SplitViewChangeTableViewController!
    var chooseTableController: SplitViewTableController!
    var status: GuestStatus!
    var orders: [Order]!
    var selectedTables = [Int:Tables?]()
    var currentIndex = 0
    var roomTableDic: [Int: TableInfo]!
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let nav = segue.destinationViewController as? UINavigationController{
            if let controller = nav.topViewController as? SplitViewTableController {
                chooseTableController = controller
                chooseTableController.pickMode = .WITHOUT_GUEST_COUNT
                chooseTableController.currentDateData = currentDateData
                chooseTableController.availableTables = availableTables
                chooseTableController.tables = []
                chooseTableController.delegate = self
            }else if let controller = nav.topViewController as? SplitViewChangeTableViewController {
                changeTableController = controller
                changeTableController.delegate = self
                changeTableController.orders = orders
            }
        }
    }
    
    func addTableToOrder(tableInfo: TableInfo) {
        selectedTables[currentIndex] = tableInfo.table
        changeTableController.setTableLabel(currentIndex, text: tableInfo.table.name)
    }
    
    func removeTableFromOrder(tableInfo: TableInfo) {
        selectedTables[currentIndex] = nil
        changeTableController.setTableLabel(currentIndex, text: "?")
    }
    
    func onRowClick(index: Int) {
        if currentIndex != index {
            chooseTableController.setTableUnavailableIfSelectedTableExists()
            if selectedTables[index] != nil {
                chooseTableController.setTableAvailableAndSelected(selectedTables[index]!!)
            }
        }
        currentIndex = index
    }
    
    func onCancelButtonClick() {
        delegate.getUpdate()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func getUnchengedOrders() -> [Order]{
        var unchangedOrders = [Order]()
        
        for i in 0..<orders.count{
            if selectedTables[i] == nil {
                unchangedOrders.append(orders[i])
            }
        }
        return unchangedOrders
    }
    
    func onSaveButtonClick() {
        let unchangedOrders = getUnchengedOrders()
        if unchangedOrders.count == 0{
            self.checkIfThereAreOccupiedTables()
        }else{
            Alerts.getInstance.someOfTablesAreNotChanged(self, orders: unchangedOrders){ action in
                if action == .CONTINUE{
                    self.checkIfThereAreOccupiedTables()
                }
            }
        }
    }
    
    func onRoomTableDicCreated(roomTableDic: [Int : TableInfo]) {
        self.roomTableDic = roomTableDic
    }
    
    func checkIfThereAreOccupiedTables(){
        let toDoneOrders = getOccupiedTableOrders()
        
        if toDoneOrders.count != 0 {
            let tableNames = toDoneOrders.map{$0.orderedTable.name}.joinWithSeparator(", ")
            let tables = RoomUtils.getTablesFromOrders(toDoneOrders)
            
            Alerts.getInstance.showExistingSeatedTables(self, tableNames: tableNames){ action in
                
                if action == AlertActions.MAKE_ALL_PREVIOUS_ORDERS_DONE{
                    
                    ServerApi.getInstance.updateAllTableStatusesToDone(tables){ response in
                        self.sendOrdersUpdate()
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                }else{
                    print("cancelled")
                }
            }
        }else{
            sendOrdersUpdate()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func sendOrdersUpdate(){
        var counter = 0
        let changedTablesCount = getChangedTablesCount()
        
        for (key, value) in selectedTables{
            if value != nil{
                let order = orders[key]
                order.orderedTable = value
                order.status = status
                
                ServerApi.getInstance.forceUpdateOrder(order){ result in
                    counter+=1
                    if changedTablesCount == counter{
                        self.delegate.getUpdate()
                    }
                }
            }
        }
    }
    
    func getOccupiedTableOrders() -> [Order]{
        var toDoneOrders = [Order]()
        
        for (_, value) in selectedTables{
            if value != nil {
                for (_, tableInfo) in roomTableDic{
                    if tableInfo.table.id == value?.id && tableInfo.currentOrder != nil{
                        toDoneOrders.append(tableInfo.currentOrder)
                        break
                    }
                }
            }
        }
        return toDoneOrders
    }
    
    func getChangedTablesCount() -> Int{
        var counter = 0
        for (_, value) in selectedTables {
            if value != nil {
                counter+=1
            }
        }
        return counter
    }
    
    func getHighlightedTables() -> [Tables] {
        return [Tables]()
    }
}










