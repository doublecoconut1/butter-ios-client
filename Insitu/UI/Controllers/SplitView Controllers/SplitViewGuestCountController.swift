//
//  Butter
//
//  Created by Grigor Avagyan on 8/7/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol SplitGuestCountEventListener{
    func onGuestCountChange(count: Int)
}

class SplitViewGuestCountController: UITableViewController {
    
    let rowCount = 11
    var inputTextField: UITextField?
    var delegate: SplitGuestCountEventListener!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("guestCounterCell")
        
        let label = cell?.viewWithTag(Tag.SPLIT_GUEST_LABEL) as! UILabel
        
        let text = (indexPath.row == rowCount - 1) ? "Enter Party Size" : String(indexPath.row + 1) + " Guest"
        label.text = text
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == rowCount - 1{
            showDialogForPartySize()
        }else{
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            let label = cell?.viewWithTag(Tag.SPLIT_GUEST_LABEL) as! UILabel
            label.textColor = UIUtils.COLOR_GREEN
            cell?.accessoryType = .Checkmark
            cell?.tintColor = UIUtils.COLOR_GREEN
            delegate.onGuestCountChange(indexPath.row + 1)
        }
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        let label = cell?.viewWithTag(Tag.SPLIT_GUEST_LABEL) as! UILabel
        label.textColor = nil
        cell?.accessoryType = .None
    }
    
    func showDialogForPartySize(){
        let alert: UIAlertController = UIAlertController(title: "Please enter party size", message: "", preferredStyle: .Alert)
        let yesButton: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: {(action: UIAlertAction) -> Void in
            
            let enteredNumber = Int((self.inputTextField?.text)!)
            if(enteredNumber != nil){
                self.delegate.onGuestCountChange(enteredNumber!)
            }else{
                self.showErrorAlert()
            }
            
        })
        let noButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
            print("Cancel")
        })
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.keyboardType = UIKeyboardType.NumberPad
            self.inputTextField = textField
        })
        alert.addAction(yesButton)
        alert.addAction(noButton)
        self.presentViewController(alert, animated: true, completion: {() -> Void in
            dispatch_after(0, dispatch_get_main_queue(), {() -> Void in})
        })
    }
    
    func showErrorAlert(){
        let alert: UIAlertController = UIAlertController(title: "Please enter valid size", message: "", preferredStyle: .Alert)
        let okButton: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: {(action: UIAlertAction) -> Void in
            self.showDialogForPartySize()
        })
        alert.addAction(okButton)
        self.presentViewController(alert, animated: true, completion: {() -> Void in
            dispatch_after(0, dispatch_get_main_queue(), {() -> Void in})
        })
    }
    
    
}
