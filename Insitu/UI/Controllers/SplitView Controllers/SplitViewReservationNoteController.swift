//
//  SplitViewReservationNoteController.swift
//  Butter
//
//  Created by Harut on 8/21/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class SplitViewReservationNoteController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    
    @IBOutlet weak var newNoteTextField: UITextField!
    @IBOutlet weak var reservationNotesTable: UITableView!

    @IBAction func onAddNewNoteButtonClick(sender: UIButton) {
        if newNoteTextField.text != "" {
            notes.append(updatedString)
            print(notes[0])
        }
        newNoteTextField.text = ""
        reservationNotesTable.reloadData()
    }
    
    var notes = Array<String>()
    var updatedString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newNoteTextField.delegate = self
        reservationNotesTable.delegate = self
        reservationNotesTable.dataSource = self
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        var cell = tableView.dequeueReusableCellWithIdentifier("serversCellId")
        let cell = UITableViewCell()
        cell.textLabel?.text = notes[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler:{action, indexpath in
            self.notes.removeAtIndex(indexPath.row)
            self.reservationNotesTable.reloadData()
        })
        return [deleteRowAction]
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        updatedString = ((textField.text as NSString?)?.stringByReplacingCharactersInRange(range, withString: string))!
        return true
    }
}
