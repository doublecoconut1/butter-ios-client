//
//  SplitViewReservationTagController.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/19/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol TagChangeEventListener {
    func onAddTag(tag: Code, tagCategory: ITEM_CATEGORY)
    func onRemoveTag(tagName: String, tagCategory: ITEM_CATEGORY)
    func onTagControllerClose()
}

public enum ITEM_CATEGORY{
    case RESERVATION
    case GUEST
}

class SplitViewTagController: UIViewController, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var tagCategory: ITEM_CATEGORY!
    var sizingCell: TagCell?
    var selectedTags: Array<Code>!
    var allTags: Array<String>!
    var delegate: TagChangeEventListener!
    
    @IBAction func onCancelButtonClick(sender: UIBarButtonItem) {
        delegate.onTagControllerClose()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "TagCell", bundle: nil)
        self.sizingCell = (cellNib.instantiateWithOwner(nil, options: nil) as NSArray).firstObject as! TagCell?
        self.collectionView.registerNib(cellNib, forCellWithReuseIdentifier: "TagCell")
        self.collectionView.backgroundColor = UIColor.clearColor()
        self.collectionView.contentInset = UIEdgeInsetsMake(2, 2, 2, 2)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: false)
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! TagCell
        addOrRemoveFromReservationTags(cell)
        self.collectionView.reloadData()
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allTags.count
    }
    
    func addOrRemoveFromReservationTags(cell: TagCell){
        if(cell.tagName.textColor == UIColor.blackColor()){
            let code = Code()
            code.name = cell.tagName.text
            selectedTags.append(code)
            delegate.onAddTag(code, tagCategory: tagCategory)
        }else{
            selectedTags.removeAtIndex(findTag(cell.tagName.text!))
            delegate.onRemoveTag(cell.tagName.text!, tagCategory: tagCategory)
        }
    }
    
    func findTag(tagName: String) -> Int{
        for i in 0 ..< selectedTags.count{
            let currTag = selectedTags[i]
            if currTag.name == tagName {
                return i
            }
        }
        return -1
    }
    
    func configureCell(cell: TagCell, forIndexPath indexPath: NSIndexPath) {
        
        var selected = false

        for i in 0 ..< selectedTags.count {
            if(selectedTags[i].name == allTags[indexPath.row]){
                selected = true
                break
            }
        }

        cell.tagName.text = allTags[indexPath.row]
        cell.tagName.textColor = selected ? UIColor.whiteColor() : UIColor.blackColor()
        cell.backgroundColor = selected ? UIColor(red:0.37, green:0.683, blue:0.615, alpha:1) : UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        self.configureCell(self.sizingCell!, forIndexPath: indexPath)
        return self.sizingCell!.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("TagCell", forIndexPath: indexPath) as! TagCell
        cell.frame.size.height = 60
        self.configureCell(cell, forIndexPath: indexPath)
        return cell
    }
}
