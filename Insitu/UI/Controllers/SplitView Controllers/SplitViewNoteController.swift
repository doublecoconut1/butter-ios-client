//
//  SplitViewNoteController.swift
//  Butter
//
//  Created by Harut on 8/22/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol SplitViewNoteEventListener {
    func onNoteAdd(category: ITEM_CATEGORY, note: Note)
    func onNoteRemove(category: ITEM_CATEGORY, note: Note)
    func onTagControllerClose()
}

class SplitViewNoteController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    
    @IBOutlet weak var newNoteTextField: UITextField!
    @IBOutlet weak var reservationNotesTable: UITableView!
    

    var delegate: SplitViewNoteEventListener!
    var allNote = Array<Note>()
    var updatedString: String = ""
    var category: ITEM_CATEGORY!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newNoteTextField.delegate = self
        reservationNotesTable.delegate = self
        reservationNotesTable.dataSource = self
    }
    @IBAction func onCancelButtonClick(sender: UIBarButtonItem) {
         delegate.onTagControllerClose()
    }
    
    @IBAction func onAddNewNoteButtonClick(sender: UIButton) {
        if !newNoteTextField.text!.isEmpty {
            let note = Note()
            note.name = updatedString
            allNote.append(note)
            delegate.onNoteAdd(category, note: note)
        }
        newNoteTextField.text = ""
        reservationNotesTable.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allNote.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.textLabel?.text = allNote[indexPath.row].name
        return cell
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler:{action, indexpath in
            
            let note = self.allNote[indexPath.row]
            self.allNote.removeAtIndex(indexPath.row)
            self.delegate.onNoteRemove(self.category, note: note)
            self.reservationNotesTable.reloadData()
            
           
        })
        return [deleteRowAction]
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        updatedString = ((textField.text as NSString?)?.stringByReplacingCharactersInRange(range, withString: string))!
        return true
    }
}
