 //
//  SplitviewRootController.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/8/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit
 
protocol SplitRootEventListener{
    func onPersonOrderUpdate(order: Order)
    func getUpdate()
}
 
 public enum OrderCategory: String{
    case EXISTING_ORDER = "EXISTING_ORDER"
    case NEW_RESERVATION = "NEW_RESERVATION"
    case WALK_IN = "WALKIN"
    case WALK_IN_AFTER_RESERVATION = "WALK_IN_AFTER_RESERVATION"
    case WAITLIST = "WAITLIST"
    case EXISTING_WAITLIST = "EXISTING_WAITLIST"
}

class SplitViewRootController: UIViewController, SplitMenuEventListener, SplitCalendarEventListener, SplitGuestCountEventListener, SplitViewReservationTimeEventListener, SplitViewTableEventListener, TagChangeEventListener, SplitViewGuestEventListener, SplitViewNoteEventListener, SplitViewGuestSearchEventListener,
    SplitViewNewGuestEventListener{

    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loadingView: UIView!
    
    
    weak var currentViewController: UIViewController?
    var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    
    var delegate: SplitRootEventListener!
    var currentOrder : Order!
    var currentWaitOrder: WaitList!
    
    var newOrder: OrderWithMultipleTables!
    var currentDateData: CurrentDateDataResponse!
    var splitViewMenuController: SplitViewMenuController!
    var tables = Array<Tables>()
    var guestCount = 0
    var reservationNotes = [Note]()
    var reservationCodes = [Code]()
    var guestNotes = [Note]()
    var guestCodes = [Code]()
    var user: User!
    var orderDateTime: NSDate!
    var currTags: Array<String>!
    var currSelectedTags: Array<Code>!
    var currNotes: Array<Note>!
    var itemCategory: ITEM_CATEGORY!
    var searchViewExist = false
    var orderCategory = OrderCategory.EXISTING_ORDER
    var orderedTable : Tables!
    var orderHistory = OrderHistory()
    var past = [Order]()
    var odrerDateTime : String!
    var orderDatetimeCalculated: Int!
    var orderguestCount: Int!
    var durationTime: Int!
    var floorNumber: Int!
    var isFromWaitList = false
    var sortBy : SortType!
    var todayDate = TimeUtils.now()
    var timeRange = [NSDate]()
    var searchedName = ""
    var roomTableDic = Dictionary<Int, TableInfo>()
    var seatedTables = [Tables]()
    var seatedTableNames = [String]()
    
    var orderHistoryUpcomingCount: Int = 0
    var orderHistoryPastCount: Int = 0
    var availableTables: AvailableTables!
    
    override func loadView() {
        super.loadView()
        checkIfOrderIsFromWaitList()
        getAvailableReservationTime()
        setupOrderParams()
        getAvailableTables()
        getCurrentDateData()
        setupMenuViewContainer()
        setupFirstView()
    }
    
    override func viewDidLoad() {
        
    }
    
    func loadingVisible(visible: Bool){
        
        loadingView.hidden = !visible
        self.view.userInteractionEnabled = visible
        self.containerView.userInteractionEnabled = visible
        self.menuView.userInteractionEnabled = visible
    }
    
    func checkIfOrderIsFromWaitList(){
        isFromWaitList = currentOrder != nil && currentOrder.id == nil
    }
    
    func getAvailableTables() {
        ServerApi.getInstance.getAvailableTables(guestCount, date: orderDateTime) { tables in
            self.availableTables = tables
        }
    }
    
    func getAvailableReservationTime(){
        let date = UserDefaults.getInstance.getOpeningTime()
        timeRange = TimeUtils.getArrayFromTimeInterval(date.startTime, endTime: date.endTime)
    }
    
    func getCurrentDateData(){
        let date = TimeUtils.dateToString(orderDateTime, dateFormat: YYYYMMDD)
        ServerApi.getInstance.getCurrentDateInfo(["date": date, "service" : 1, "floor" : floorNumber, "sort" : sortBy.rawValue]){ currDateData in
            self.currentDateData = currDateData
            self.roomTableDic = RoomFactory.createTableObjects(self.currentDateData)
        }
    }
    
    func checkAddBtnAccessibility(){
        splitViewMenuController.addReservationButton.enabled = false
    }
    
    func onNewReservationPopupClosed() {
        delegate.getUpdate()
    }
    
    func setupOrderParams(){
        switch orderCategory {
        case OrderCategory.EXISTING_ORDER, OrderCategory.WALK_IN_AFTER_RESERVATION:
            setInfoFromCurrentOrder()
            break
        case OrderCategory.WAITLIST, OrderCategory.NEW_RESERVATION:
            createDefaultReservation()
        case OrderCategory.EXISTING_WAITLIST:
            setInfoFromCurrentWaitOrder()
        default:
            createWalkInReservation()
            break
        }
        getOrderHistory()
    }
    
    func getOrderHistory(){
        if user.id == nil {
            return
        }
        ServerApi.getInstance.getUserOrderHistory(user.id) { result in
            self.orderHistory = result
        }
    }
    
    func createWalkInReservation(){
        user = User()
        orderDateTime = TimeUtils.getNearestTimeFromInterval(timeRange)
        tables.append(orderedTable)
        guestCount = orderedTable.type.minSeats
    }
    
    func createDefaultReservation(){
        user = User()
        orderDateTime = TimeUtils.getNearestTimeFromInterval(timeRange)
        guestCount = 2
    }
    
    func setInfoFromCurrentWaitOrder(){
        user = currentWaitOrder.user
        guestCount = currentWaitOrder.guestCount
        reservationCodes = currentWaitOrder.codes
        guestCodes = currentWaitOrder.user.codes
        reservationNotes = currentWaitOrder.notes
        guestNotes = currentWaitOrder.user.notes
        orderDateTime = TimeUtils.stringToDate(currentWaitOrder.orderDateTime, dateFormat: YYYYMMDDHHMMSS)
    }
    
    func setInfoFromCurrentOrder(){
        orderedTable = currentOrder.orderedTable
        user = currentOrder.user
        tables.append(orderedTable)
        guestCount = currentOrder.guestCount
        reservationCodes = currentOrder.codes
        guestCodes = currentOrder.user.codes
        reservationNotes = currentOrder.notes
        guestNotes = currentOrder.user.notes
        orderDateTime = TimeUtils.stringToDate(currentOrder.orderDateTime, dateFormat: YYYYMMDDHHMMSS)
    }
    
    func setupFirstView(){
        let firstControllerName = String(SplitViewGuestProfileController)
        self.currentViewController = getViewControllerByName(firstControllerName)
        self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
        self.addChildViewController(self.currentViewController!)
        self.addSubview(self.currentViewController!.view, toView: self.containerView)
    }
    
    func getUpdatedOrderHistory() -> OrderHistory {
        return orderHistory
    }
    
    func getViewControllerByName(controllerName: String) -> UIViewController? {
        
        let nav = getNavControllerByName(controllerName)
        
        switch controllerName {
        case String(SplitViewGuestProfileController):
            let controller = nav.topViewController as! SplitViewGuestProfileController
            controller.guestTags = guestCodes
            controller.guestNotes = guestNotes
            controller.user = user
            controller.currentOrder = currentOrder
            controller.orderHistory = orderHistory
            controller.delegate = self
            return nav
        case String(SplitViewCalendarController):
            let controller = nav.topViewController as! SplitViewCalendarController
            controller.lastDate = TimeUtils.addMonthCountToCurrentDate(1)
            controller.delegate = self
            return nav
        case String(SplitViewReservationTimeController):
            let controller = nav.topViewController as! SplitViewReservationTimeController
            controller.orderDateTime = orderDateTime
            controller.orderCategory = orderCategory
            controller.timeRange = timeRange
            controller.delegate = self
            return nav
        case String(SplitViewGuestCountController):
            let controller = nav.topViewController as! SplitViewGuestCountController
            controller.delegate = self
            return nav
        case String(SplitViewTableController):
            let controller = nav.topViewController as! SplitViewTableController
            controller.tables = tables
            controller.availableTables = availableTables
            controller.currentDateData = currentDateData
            controller.guestCount = guestCount
            controller.pickMode = .WITH_GUEST_COUNT
            controller.delegate = self
            controller.orderedTable = orderedTable
            return nav
        case String(SplitViewTagController):
            let controller = nav.topViewController as! SplitViewTagController
            controller.allTags = currTags
            controller.tagCategory = itemCategory
            controller.selectedTags = currSelectedTags
            controller.delegate = self
            return nav
        case String(SplitViewNoteController):
            let controller = nav.topViewController as! SplitViewNoteController
            controller.allNote = currNotes
            controller.category = itemCategory
            controller.delegate = self
            return nav
        case String(SplitViewGuestSearchController):
            let controller = nav.topViewController as! SplitViewGuestSearchController
            controller.delegate = self
            return nav
        case String(SplitViewCreateNewGuestController):
            let controller = nav.topViewController as! SplitViewCreateNewGuestController
            controller.fullName = searchedName
            controller.delegate = self
            return nav
        default:
            return nil
        }
    }
    
    func onNoteAdd(category: ITEM_CATEGORY, note: Note){
        if category == ITEM_CATEGORY.RESERVATION {
            reservationNotes.append(note)
            updateMenuNotesAndTags()
        }else{
            guestNotes.append(note)
        }
    }
    
    func onGuestNameButtonClick()
    {
        guestCodes.removeAll()
        guestNotes.removeAll()
        reservationCodes.removeAll()
        reservationNotes.removeAll()
        
        toggleGuestInfoLabelAndSwitchController(user, viewControllerName: String(SplitViewGuestSearchController))
        checkAddOrderBtnAccessibility()
    }
    
    func onRemoveUserInfoBtnClick() {
        user = nil
        guestCodes.removeAll()
        guestNotes.removeAll()
        reservationCodes.removeAll()
        reservationNotes.removeAll()
        toggleGuestInfoLabelAndSwitchController(user, viewControllerName: nil)
        checkAddOrderBtnAccessibility()
    }
    
    func onUserCreated(user: User) {
        self.user = user
        self.guestCodes = []
        self.guestNotes = []
        self.orderHistory = OrderHistory()
        onUserChanged(user)
    }
    
    func onGuestTableCellClick(cellUser: User){
        user = cellUser
        guestCodes = user.codes
        guestNotes = user.notes
        getOrderHistory()
        onUserChanged(user)
        
    }
    
    func onUserChanged(user: User){
        toggleGuestInfoLabelAndSwitchController(user, viewControllerName: nil)
        checkAddOrderBtnAccessibility()
        UIUtils.dismissKeyboard(self.view)
    }
    
    func onCreateGuestCellClick(fullName: String) {
        searchedName = fullName
        switchDetailViewController(String(SplitViewCreateNewGuestController))
    }
    
    func toggleGuestInfoLabelAndSwitchController(user: User?, viewControllerName: String?){
        
        var name: String!
        var color: UIColor!
        
        if user == nil {
            name = "Create or Search for Guest"
            color = UIUtils.COLOR_SEARCH_BAR
            splitViewMenuController.removeUserInfoButton.hidden = true
            splitViewMenuController.guestNameButton.setTitle(name, forState: .Normal)
            splitViewMenuController.guestNameButton.setTitleColor(color, forState: .Normal)
            switchDetailViewController(String(SplitViewGuestSearchController))

        }else{
            if viewControllerName != nil {
                switchDetailViewController(String(SplitViewGuestSearchController))
            } else {
                name = getFullName(user!)
                color = UIUtils.COLOR_GREEN
                splitViewMenuController.removeUserInfoButton.hidden = false
                splitViewMenuController.guestNameButton.setTitle(name, forState: .Normal)
                splitViewMenuController.guestNameButton.setTitleColor(color, forState: .Normal)
                switchDetailViewController(String(SplitViewGuestProfileController))
            }
        }
    }
    
    func onNoteRemove(category: ITEM_CATEGORY, note: Note){
        if category == ITEM_CATEGORY.RESERVATION {
            reservationNotes.removeAtIndex(findNoteIndex(note, collection: reservationNotes))
            updateMenuNotesAndTags()
        }else{
            guestNotes.removeAtIndex(findNoteIndex(note, collection: guestNotes))
        }
    }
    
    func updateMenuNotesAndTags(){
        splitViewMenuController.reservationCodes = reservationCodes
        splitViewMenuController.reservationNotes = reservationNotes
        splitViewMenuController.updateReservationLabelsInfo()
    }
    
    func findNoteIndex(note: Note, collection: Array<Note>)-> Int{
        for i in 0 ..< collection.count{
            let currNote = collection[i]
            if (currNote.name == note.name) {
                return i
            }
        }
        return -1
    }
    
    func onTagControllerClose() {
        switchDetailViewController(String(SplitViewGuestProfileController))
    }
    
    func onAddGuestNoteButtonCLick(){
        setNoteViewItems(ITEM_CATEGORY.GUEST)
        switchDetailViewController(String(SplitViewNoteController))
    }
    
    func onAddGuestTagButtonClick() {
        setTagViewItems(ITEM_CATEGORY.GUEST)
        switchDetailViewController(String(SplitViewTagController))
    }
    
    func onAddTag(tag: Code, tagCategory: ITEM_CATEGORY) {
        
        if tagCategory == ITEM_CATEGORY.RESERVATION {
            reservationCodes.append(tag)
            updateMenuNotesAndTags()
            print(reservationCodes)
        }else{
            guestCodes.append(tag)
            print(guestCodes)
        }
    }
    
    func onRemoveTag(tagName: String, tagCategory: ITEM_CATEGORY) {
        
        if tagCategory == ITEM_CATEGORY.RESERVATION {
            reservationCodes.removeAtIndex(findTagIndexByName(tagName, collection: reservationCodes))
            updateMenuNotesAndTags()
            print(reservationCodes)
        }else{
            guestCodes.removeAtIndex(findTagIndexByName(tagName, collection: guestCodes))
            print(guestCodes)
        }
    }
    
    func findTagIndexByName(tagName: String, collection: Array<Code>) -> Int{
        for i in 0 ..< collection.count {
            let code = collection[i]
            if code.name == tagName {
                return i
            }
        }
        return -1
    }
    
    func onRoomTableDicCreated(roomTableDic: [Int : TableInfo]) {
        
    }
    
    func addTableToOrder(tableInfo: TableInfo) {
        splitViewMenuController.addTableName(tableInfo)
        tables.append(currentDateData.floor.tablesMap[String(tableInfo.id)]!)
        
        checkAddOrderBtnAccessibility()
    }
    
    func getHighlightedTables() -> [Tables] {
        return tables
    }
    
    func removeTableFromOrder(tableInfo: TableInfo) {
        splitViewMenuController.removeTableName(tableInfo)
        for i in 0 ..< tables.count {
            let table = tables[i]
            if table.id == tableInfo.id {
                tables.removeAtIndex(i)
                break
            }
        }
        checkAddOrderBtnAccessibility()
    }
    
    func setupOrderParams(order: Order) -> Order{
        order.user = user
        order.orderedTable = tables[0]
        order.guestCount = guestCount
        order.codes = reservationCodes
        order.notes = reservationNotes
        order.user.codes = guestCodes
        order.user.notes = guestNotes
        order.orderDateTime = TimeUtils.dateToString(orderDateTime, dateFormat: YYYYMMDDHHMMSS)
        
        if orderCategory == OrderCategory.WALK_IN_AFTER_RESERVATION {
            order.status = GuestStatus.SEATED
        }
        
        return order
    }
    
    func setupWaitListParams(waitingOrder: WaitList) -> WaitList{
        waitingOrder.user = user
        waitingOrder.guestCount = guestCount
        waitingOrder.codes = reservationCodes
        waitingOrder.notes = reservationNotes
        waitingOrder.user.codes = guestCodes
        waitingOrder.user.notes = guestNotes
        waitingOrder.orderDateTime = TimeUtils.dateToString(orderDateTime, dateFormat: YYYYMMDDHHMMSS)
        return waitingOrder
    }
    
    func createWaitList(){
        let order = currentWaitOrder == nil ? WaitList() : currentWaitOrder
        let waitingOrder = setupWaitListParams(order)
        let update = orderCategory == .EXISTING_WAITLIST
        
        ServerApi.getInstance.postWaitList(waitingOrder, update: update){ response in
            print(response)
            self.delegate.getUpdate()
        }
    }
    
    func createOrder(){
        if orderCategory == .EXISTING_ORDER || orderCategory == .WALK_IN_AFTER_RESERVATION{
            currentOrder = setupOrderParams(currentOrder)
            if !isFromWaitList {
                delegate.onPersonOrderUpdate(currentOrder)
            }
            if currentOrder.id == nil {
                ServerApi.getInstance.postOrder(currentOrder){response in
                    self.delegate.getUpdate()
                }
            }else{
                ServerApi.getInstance.forceUpdateOrder(currentOrder){response in
                    self.delegate.getUpdate()
                }
            }
        }else{
            let order = setupOrderParams(Order())
            if orderCategory == .NEW_RESERVATION {
                ServerApi.getInstance.postOrder(order){response in
                    self.delegate.getUpdate()
                }
            }else{
                ServerApi.getInstance.postWalkInOrder(order){response in
                    self.delegate.getUpdate()
                }
            }
        }
    }
    
    func onAddReservationButtonClick() {
        
        if StatusUtils.isWaitListCategory(orderCategory){
            createWaitList()
            self.dismissViewControllerAnimated(true, completion: nil)
        }else{
            if seatedTableExist(){
                Alerts.getInstance.showExistingSeatedTables(self, tableNames: seatedTableNames.joinWithSeparator(", ")){ action in
                    
                    if action == AlertActions.MAKE_ALL_PREVIOUS_ORDERS_DONE{
                        
                        ServerApi.getInstance.updateAllTableStatusesToDone(self.seatedTables){ response in
                            if self.tables.count == 1 {
                                self.createOrder()
                            }else{
                                self.createMultiOrder()
                            }
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                    }else{
                        print("cancelled")
                    }
                }
            }else{
                if tables.count == 1 {
                    createOrder()
                }else{
                    createMultiOrder()
                }
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    
    func seatedTableExist() -> Bool{
        
        var seatedTableExist = false
        seatedTableNames.removeAll()
        
        for table in tables{
            let tableInfo = roomTableDic[table.id]
            if tableInfo?.currentOrder != nil && (isFromWaitList || (isWalkInAfterReservation() && currentOrder != nil && tableInfo?.currentOrder.id != currentOrder.id))  {
                
                seatedTableNames.append(table.name)
                seatedTables.append(table)
                seatedTableExist = true
            }
        }
        return seatedTableExist
    }
    
    func createMultiOrder(){
        
        let orderType : OrderCategory? = (orderCategory == .WALK_IN || orderCategory == .WALK_IN_AFTER_RESERVATION || isFromWaitList) ? .WALK_IN : nil
        let orderId: String? = currentOrder != nil && currentOrder.id != nil ? String(currentOrder.id) : nil
        
        let multipleOrder = OrderWithMultipleTables()
        multipleOrder.user = user
        multipleOrder.tables = tables
        multipleOrder.guestCount = guestCount
        multipleOrder.codes = reservationCodes
        multipleOrder.notes = reservationNotes
        multipleOrder.user.codes = guestCodes
        multipleOrder.user.notes = guestNotes
        multipleOrder.orderDateTime = TimeUtils.dateToString(orderDateTime, dateFormat: YYYYMMDDHHMMSS)
        
        if isFromWaitList{
            multipleOrder.waitList = currentOrder.waitList
        }
        
        ServerApi.getInstance.postMultyOrder(orderId, order: multipleOrder, orderCategory: orderType){response in
            self.delegate.getUpdate()
        }
    }
    
    func onGuestCountChange(count: Int) {
        guestCount = count
        getAvailableTables()
        getCurrentDateData()
        splitViewMenuController.guestCountLabel.text = String(count)
        checkAddOrderBtnAccessibility()
    }
    
    func isErrorViewVisible() -> Bool{
        var guestChairCount = 0
        for table in tables {
            guestChairCount += table.type.maxSeats
        }
        if guestCount > guestChairCount {
            splitViewMenuController.errorViewHidden(false)
            return true
        }else{
            splitViewMenuController.errorViewHidden(true)
            return false
        }
    }
    
    func checkAddOrderBtnAccessibility(){
        
        let isWaitList = StatusUtils.isWaitListCategory(orderCategory)
        
        if !isWaitList {
            isErrorViewVisible()
        }
        
        if (!isWaitList && tables.count != 0 && user != nil) || ((isWaitList && user != nil)) {
            splitViewMenuController.changeAddButtonAccessibility(true)
        }else{
            splitViewMenuController.changeAddButtonAccessibility(false)
        }
    }
    
    func getNavControllerByName(controllerName: String) -> UINavigationController{
        return self.storyboard?.instantiateViewControllerWithIdentifier(controllerName) as! UINavigationController
    }
    
    func setupMenuViewContainer(){
        menuView.layer.borderWidth = 2
        menuView.layer.borderColor = UIUtils.COLOR_SPLITVIEW_TABLEVIEW_BORDER.CGColor
    }
    
    func switchDetailViewController(controllerName: String){
        let viewControllerName = controllerName
//        if viewControllerName == String(SplitViewGuestProfileController) {
//            viewControllerName = searchViewExist ? String(SplitViewGuestSearchController) : viewControllerName
//        }
        let newViewController = getViewControllerByName(viewControllerName)
        if newViewController != nil {
            newViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.cycleFromViewController(self.currentViewController!, toViewController: newViewController!)
            self.currentViewController = newViewController
        }
    }
    
    func cycleFromViewController(oldViewController: UIViewController, toViewController newViewController: UIViewController) {
        oldViewController.willMoveToParentViewController(nil)
        self.addChildViewController(newViewController)
        self.addSubview(newViewController.view, toView:self.containerView!)
        newViewController.view.layoutIfNeeded()
        
        oldViewController.view.removeFromSuperview()
        oldViewController.removeFromParentViewController()
        newViewController.didMoveToParentViewController(self)
    }
    
    func onReservationTimeChange(date: NSDate) {
        
        let timeIn24 = TimeUtils.dateToString(date, dateFormat: HHMM)
        let timeIn12 = TimeUtils.dateToString(date, dateFormat: HMM)
        let seperatedTime = timeIn24.componentsSeparatedByString(":")
        let hour = Int(seperatedTime[0])
        let minute = Int(seperatedTime[1])
        
        orderDateTime = TimeUtils.changeHourAndMinuteOfDay(orderDateTime, hour: hour!, minute: minute!)
        getAvailableTables()
        
        if splitViewMenuController.reservationTimeLabel != nil {
            splitViewMenuController.reservationTimeLabel.text = timeIn12
            splitViewMenuController.reservationTimeLabel.textColor = UIUtils.COLOR_GREEN
        }
    }
    
    func onCurrentDateChange(date: NSDate) {
        orderDateTime = TimeUtils.changeDayOfDate(orderDateTime, toDate: date)
        getAvailableTables()
        getCurrentDateData()
        splitViewMenuController.dateLabel.text = TimeUtils.dateToString(orderDateTime, dateFormat: MMMDD)
    }
    
    func onTableItemClicked(controllerName: String) {
        setTagViewItems(ITEM_CATEGORY.RESERVATION)
        setNoteViewItems(ITEM_CATEGORY.RESERVATION)
        
        if controllerName == String(SplitViewGuestProfileController) {
            toggleGuestInfoLabelAndSwitchController(user, viewControllerName: nil)
        } else if controllerName == String(SplitViewTableController) && StatusUtils.isWaitListCategory(orderCategory){
            print("You can't pick table because it's waitlist")
        }else{
            switchDetailViewController(controllerName)
        }
        UIUtils.dismissKeyboard(self.view)
    }
    
    func setNoteViewItems(category: ITEM_CATEGORY) {
        if category == ITEM_CATEGORY.RESERVATION {
            itemCategory = ITEM_CATEGORY.RESERVATION
            currNotes = reservationNotes
        }else{
            itemCategory = ITEM_CATEGORY.GUEST
            currNotes = guestNotes
        }
    }
    
    func setTagViewItems(category: ITEM_CATEGORY) {
        if category == ITEM_CATEGORY.RESERVATION {
            itemCategory = ITEM_CATEGORY.RESERVATION
            currSelectedTags = reservationCodes
            currTags = addTagToExistingTags(Constants.RESERVATION_TAGS, selectedTags: currSelectedTags)
            
        }else{
            itemCategory = ITEM_CATEGORY.GUEST
            currSelectedTags = guestCodes
            currTags = addTagToExistingTags(Constants.GUEST_TAGS, selectedTags: currSelectedTags)
        }
    }
    
    func addTagToExistingTags(availableTags: [String], selectedTags: [Code]) -> [String]{
        
        var allTags = availableTags
        
        for code in selectedTags {
            let index = allTags.indexOf(code.name)
            if index == nil {
                allTags.append(code.name)
            }
        }
        return allTags
    }
    
    func addSubview(subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
    }
    
    func insertTableInfo(){
        if orderCategory == OrderCategory.EXISTING_ORDER || orderCategory == OrderCategory.WALK_IN_AFTER_RESERVATION{
            let currentMonthDay = TimeUtils.getMonthAndDayFromDateInString(currentOrder.orderDateTime)
            let orderedDate = TimeUtils.stringToDate(currentOrder.orderDateTime, dateFormat: YYYYMMDDHHMMSS)
            
            splitViewMenuController.fullName = getFullName(currentOrder.user)
            splitViewMenuController.date  = TimeUtils.isSameDate(orderedDate, secondDate: TimeUtils.now()) ? "Today, " + currentMonthDay : currentMonthDay
            splitViewMenuController.reservationTime = TimeUtils.getHourAndMinutesFromDateInString(currentOrder.orderDateTime)
            
            splitViewMenuController.orderCategory = orderCategory
            splitViewMenuController.guestCount = String(currentOrder.guestCount)
            splitViewMenuController.tableNumber = currentOrder.orderedTable.name
            splitViewMenuController.reservationNotes = currentOrder.notes
            splitViewMenuController.reservationCodes = currentOrder.codes
            
        }else if orderCategory == OrderCategory.EXISTING_WAITLIST{
            
            let currentMonthDay = TimeUtils.getMonthAndDayFromDateInString(currentWaitOrder.orderDateTime)
            let orderedDate = TimeUtils.stringToDate(currentWaitOrder.orderDateTime, dateFormat: YYYYMMDDHHMMSS)
            
            splitViewMenuController.date  = TimeUtils.isSameDate(orderedDate, secondDate: TimeUtils.now()) ? "Today, " + currentMonthDay : currentMonthDay
            splitViewMenuController.reservationTime = TimeUtils.getHourAndMinutesFromDateInString(currentWaitOrder.orderDateTime)
            
            splitViewMenuController.orderCategory = orderCategory
            splitViewMenuController.tableNumber = ""
            splitViewMenuController.isAddButtonEnabled = true
            splitViewMenuController.guestCount = String(currentWaitOrder.guestCount)
            splitViewMenuController.fullName = getFullName(currentWaitOrder.user)
            splitViewMenuController.reservationNotes = currentWaitOrder.notes
            splitViewMenuController.reservationCodes = currentWaitOrder.codes
        }
        else{
            splitViewMenuController.reservationTime = TimeUtils.dateToString(orderDateTime, dateFormat: HMM)
            splitViewMenuController.orderCategory = orderCategory
            splitViewMenuController.tableNumber = (orderedTable != nil) ? orderedTable.name : ""
            splitViewMenuController.isAddButtonEnabled = orderCategory == OrderCategory.WALK_IN || orderCategory == OrderCategory.WAITLIST
            splitViewMenuController.date = TimeUtils.dateToString(todayDate, dateFormat: MMMDD)
            splitViewMenuController.guestCount = (orderedTable != nil) ? String(orderedTable.type.minSeats) : String(2)
            splitViewMenuController.fullName = "ANONYMOUS"
        }
        
    }
    
    func getFullName(user:User) -> String{
        return StringUtils.getFullNameFromUser(user)
    }
    
    func isWalkInAfterReservation() -> Bool{
        return orderCategory == .WALK_IN_AFTER_RESERVATION
    }
    
    func isExistingOrder() -> Bool{
        return orderCategory == .EXISTING_ORDER
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let nav = segue.destinationViewController as? UINavigationController{
            if let controller = nav.topViewController as? SplitViewMenuController{
                splitViewMenuController = controller
                splitViewMenuController.delegate = self
                insertTableInfo()
            }
        }
    }
    
}
