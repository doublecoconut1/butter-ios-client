     //
//  SplitViewTableController.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/13/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol SplitViewTableEventListener {
    func addTableToOrder(tableInfo: TableInfo)
    func removeTableFromOrder(tableInfo: TableInfo)
    func getHighlightedTables() -> [Tables]
    func onRoomTableDicCreated(roomTableDic: [Int: TableInfo])
}
     
public enum PickTableMode{
    case WITH_GUEST_COUNT
    case WITHOUT_GUEST_COUNT
}

class SplitViewTableController: UIViewController {
    
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var upcomingButton: UIButton!
    @IBOutlet weak var seatedTimeButton: UIButton!
    
    var delegate: SplitViewTableEventListener!
    var roomView: UIView!
    var currentDateData: CurrentDateDataResponse!
    var roomTableDic = Dictionary<Int,TableInfo>()
    var tables: Array<Tables>!
    var availableTables: AvailableTables!
    var guestCount = 0
    var orderedTable: Tables!
    var pickMode: PickTableMode!
    
    override func viewDidAppear(animated: Bool) {
        setTitle()
        setupRoom()
        highlightOrderedTable()
        
    }
    
    @IBAction func onUpcomingButtonClick(sender: UIButton) {
        RoomUtils.onUpcomingButtonClick(seatedTimeButton, upcomingBtn: upcomingButton, roomView: roomView, roomTableListDic: roomTableDic)

    }
    
    @IBAction func onSeatedTimeButtonClick(sender: UIButton) {
        RoomUtils.onSeatedTimeButtonClick(seatedTimeButton, upcomingBtn: upcomingButton, roomView: roomView, roomTableListDic: roomTableDic)
    }
    
    func setTitle(){
        self.title = "Choose Tables"
    }
    
    func highlightOrderedTable(){
        
        if orderedTable != nil {
            setTableInfoAvailable(orderedTable.id)
        }
        for table in tables{
            setTableInfoAvailable(table.id)
            selectTable(table.id)
        }
    }
    
    func setTableInfoAvailable(id: Int) -> TableInfo{
        let tableInfo = roomTableDic[id]
        tableInfo?.isAvailable = true
        return tableInfo!
    }
    
    func setupRoom(){
        
        rootView.backgroundColor = UIUtils.COLOR_ROOM
        
        roomView = RoomFactory.createRoom(rootView, floor: currentDateData.floor, isVisibleMaxMinSizes: true)
        let roomTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SplitViewTableController.onRoomTableTap(_:))) 
        roomTapGesture.numberOfTapsRequired = 1
        roomTapGesture.numberOfTouchesRequired = 1
        roomView.addGestureRecognizer(roomTapGesture)
        rootView.addSubview(roomView)
        
        roomTableDic = RoomFactory.createTableObjectsWithPosition(roomView, currentDateData: currentDateData, availableTables: availableTables)
        delegate.onRoomTableDicCreated(roomTableDic)
    }
    
    func onRoomTableTap(gesture: UITapGestureRecognizer){
        let position = gesture.locationInView(gesture.view)
        let pointInWindow: CGPoint = roomView.convertPoint(position, toView: nil)
        let pointInScreen: CGPoint = roomView.window!.convertPoint(pointInWindow, toWindow: nil)
        
        let tableInfo = RoomUtils.getTableFromPosition(pointInScreen, roomTableListDic: roomTableDic)
        
        if tableInfo != nil && tableInfo!.isAvailable {
            findTableInViewAndToggle(tableInfo!)
        }else{
            print("is not a table")
        }
    }
    
    func findTableInViewAndToggle(tableInfo: TableInfo){
        
        if pickMode == .WITH_GUEST_COUNT {
            pickTableViaGuestCount(tableInfo)
        }else{
            pickTableWithoutGuestCount(tableInfo)
        }
        
    }
    
    func pickTableWithoutGuestCount(tableInfo: TableInfo){
        if tableInfo.selected == false && RoomUtils.getSelectedTable(roomTableDic) == nil{
            addTable(tableInfo)
        }else if tableInfo.selected{
            removeTable(tableInfo)
        }
    }
    
    func setTableUnavailableIfSelectedTableExists(){
        
        let selectedTable = RoomUtils.getSelectedTable(roomTableDic)
        if selectedTable != nil{
            setTableUnavailable(selectedTable!.id)
            unselectTable(selectedTable!.id)
            roomTableDic = RoomUtils.unselectAllTables(roomTableDic)
        }
    }
    
    func setTableAvailable(id: Int){
        let tableView = roomView.viewWithTag(id)
        tableView!.backgroundColor = UIColor.whiteColor()
        tableView!.layer.borderColor = UIUtils.COLOR_TABLE_BORDER.CGColor
        roomTableDic[id]?.isAvailable = true
    }
    
    func setTableUnavailable(id: Int){
        let tableView = roomView.viewWithTag(id)
        tableView!.backgroundColor = UIUtils.COLOR_GRAY_DARK
        roomTableDic[id]?.isAvailable = false
    }
    
    func setTableAvailableAndSelected(table: Tables){
        setTableAvailable(table.id)
        selectTable(table.id)
    }
    
    func pickTableViaGuestCount(tableInfo: TableInfo){
        if tableInfo.selected == false{
            if guestsMoreThenSeats() {
                addTable(tableInfo)
            }else{
                Alerts.getInstance.guestsFitTables(self)
            }
        }else{
            removeTable(tableInfo)
        }
        
        tables = delegate.getHighlightedTables()
    }
    
    func addTable(tableInfo: TableInfo){
        selectTable(tableInfo.id)
        delegate.addTableToOrder(tableInfo)
    }
    
    func removeTable(tableInfo: TableInfo){
        unselectTable(tableInfo.id)
        delegate.removeTableFromOrder(tableInfo)
    }
    
    func selectTable(id: Int) {
        let tableView = roomView.viewWithTag(id)
        tableView!.layer.borderColor = UIColor.blackColor().CGColor
        roomTableDic[id]?.selected = true
    }
    
    func unselectTable(id: Int){
        let tableView = roomView.viewWithTag(id)
        tableView!.layer.borderColor = UIUtils.COLOR_TABLE_BORDER.CGColor
        roomTableDic[id]?.selected = false
    }
    
    func guestsMoreThenSeats() -> Bool{
        var maxSeatsCount = 0
        for table in tables {
            maxSeatsCount += table.type.maxSeats
        }
        if guestCount > maxSeatsCount {
            return true
        }
        return false
    }
}
