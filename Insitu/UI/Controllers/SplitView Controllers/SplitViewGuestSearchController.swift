//
//  SplitViewGuestSearchController.swift
//  Butter
//
//  Created by Harut on 8/23/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol SplitViewGuestSearchEventListener{
    func onGuestTableCellClick(user: User)
    func onCreateGuestCellClick(fullName: String)
}

class SplitViewGuestSearchController: UIViewController, UITableViewDataSource, UISearchResultsUpdating, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var data = [User]()
    var filteredData: [User]!
    var delegate: SplitViewGuestSearchEventListener!
    var staticRowsCount = 2
    var searchedName = ""
    
    var searchController: UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filteredData = data
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.definesPresentationContext = false
        searchController.hidesNavigationBarDuringPresentation = false
        
        searchController.searchBar.sizeToFit()
        self.edgesForExtendedLayout = .None
        searchController.edgesForExtendedLayout = .None
        
        tableView.tableHeaderView = searchController.searchBar
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func getGuestListFromServer(text: String){
        ServerApi.getInstance.getUsersWithName(text){ result in
            self.filteredData = result
            self.tableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
            delegate.onGuestTableCellClick(User())
            break
        case 1:
            delegate.onCreateGuestCellClick(searchedName)
        default:
            delegate.onGuestTableCellClick(filteredData[indexPath.row - staticRowsCount])
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = UITableViewCell()
            cell.textLabel?.text = "Temporary Guest"
            cell.textLabel?.textColor = UIUtils.COLOR_GREEN
            return cell
        case 1:
            let cell = UITableViewCell()
            cell.textLabel?.text = "Create New Guest"
            cell.textLabel?.textColor = UIUtils.COLOR_GREEN
            return cell
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier("gustSearchCell")! as! GuestSearchCell
            cell.phoneNumberLabel.text = filteredData[indexPath.row - staticRowsCount].phoneNumber
            cell.guestFullNameLabel?.text = getFullName(filteredData[indexPath.row - staticRowsCount])
            return cell
        }
    }
    
    func getFullName(user: User) -> String{
        
        return StringUtils.getFullNameFromUser(user)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count + staticRowsCount
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if var searchText = searchController.searchBar.text {
            searchedName = searchText
            if searchText.isEmpty {
                filteredData.removeAll()
                self.tableView.reloadData()
                return
            }
            getGuestListFromServer(searchText)
        }
    }
}