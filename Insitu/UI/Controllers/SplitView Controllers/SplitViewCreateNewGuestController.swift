//
//  SplitViewCreateNewGuestController.swift
//  Butter
//
//  Created by Harut on 9/26/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol SplitViewNewGuestEventListener {
    func onUserCreated(user: User)
}

class SplitViewCreateNewGuestController: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var secondNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var countryFlagButton: UIButton!
    
    var fullName: String!
    var delegate: SplitViewNewGuestEventListener!
    
    @IBAction func onCountryFlagButtonClick(sender: UIButton) {
        
    }
    
    @IBAction func onCancelButtonClick(sender: UIButton) {
    
    }
    
    @IBAction func onDoneButtonClick(sender: UIButton) {
        if userHasRequiredInfo(){
            createUser(){ newUser in
                self.delegate.onUserCreated(newUser)
            }
        }else{
            showAlertForMissingInfo()
        }
    }
    
    override func viewDidLoad() {
        setupInfoView()
    }
    
    func showAlertForMissingInfo(){
        let firstNameIsEmpty = StringUtils.isEmpty(firstNameTextField.text!)
        let lastNameIsEmpty = StringUtils.isEmpty(secondNameTextField.text!)
        
        var fields = ["First Name", "Last Name"]
        
        if firstNameIsEmpty || lastNameIsEmpty {
            if !firstNameIsEmpty {
                fields.removeAtIndex(fields.indexOf("First Name")!)
            }
            if !lastNameIsEmpty{
                fields.removeAtIndex(fields.indexOf("Last Name")!)
            }
            Alerts.getInstance.pleaseEnterRequiredFields(self, fields: fields)
        }
    }
    
    func createUser(callBack: (User) -> ()){
        let user = User()
        user.firstName = StringUtils.removeUnnecessaryCharsFromStartAndEnd(firstNameTextField.text!)
        user.lastName = StringUtils.removeUnnecessaryCharsFromStartAndEnd(secondNameTextField.text!)
        user.email = StringUtils.removeUnnecessaryCharsFromStartAndEnd(emailTextField.text!)
        user.phoneNumber = StringUtils.removeUnnecessaryCharsFromStartAndEnd(phoneNumberTextField.text!)
        ServerApi.getInstance.createUser(user){ newUser in
            callBack(newUser)
        }
    }
    
    func userHasRequiredInfo() -> Bool{
        let firstNameIsEmpty = StringUtils.isEmpty(firstNameTextField.text!)
        let lastNameIsEmpty = StringUtils.isEmpty(secondNameTextField.text!)
        
        if firstNameIsEmpty || lastNameIsEmpty {
            return false
        }
        return true
    }
    
    func setupInfoView(){
        setupFullName()
    }
    
    func setupFullName() {
        if !StringUtils.isEmpty(fullName){
            fullName = StringUtils.removeUnnecessaryCharsFromStartAndEnd(fullName)
            var separatedName = fullName.characters.split(" ").map(String.init)
            
            firstNameTextField.text = separatedName[0].uppercaseFirst
            if separatedName.count > 1{
                separatedName.removeFirst()
                secondNameTextField.text = (separatedName.map{($0)}.joinWithSeparator(" ")).uppercaseFirst
            }
            
        }
    }
}



