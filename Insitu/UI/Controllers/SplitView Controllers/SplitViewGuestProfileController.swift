//
//  SplitViewGuestProfileController.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/7/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol SplitViewGuestEventListener {
    func onAddGuestTagButtonClick()
    func onAddGuestNoteButtonCLick()
    func onGuestNameButtonClick()
    func getUpdatedOrderHistory() -> OrderHistory
}

class SplitViewGuestProfileController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var guestFullName: UIButton!
    
    @IBOutlet weak var guestPhoneNumber: UILabel!
    @IBOutlet weak var guestEmailAdress: UILabel!
    @IBOutlet weak var guestTagsLabel: UITextView!
    @IBOutlet weak var guestNotesLabel: UITextView!
    
    @IBOutlet weak var reservationNumber: UILabel!
    @IBOutlet weak var cancellationsNumber: UILabel!
    @IBOutlet weak var noShowsNumber: UILabel!
    @IBOutlet weak var walkInsNumber: UILabel!
    @IBOutlet weak var avgTurnTimeNumber: UILabel!
    
    @IBOutlet weak var noteTab: UIButton!
    @IBOutlet weak var clockTab: UIButton!
    
    @IBOutlet weak var historyPanelContainer: UIView!
    @IBOutlet weak var tagsPanelContainer: UIView!
    
    @IBOutlet weak var orderHistoryTableView: UITableView!
    
    var user: User!
    var currentOrder : Order!
    var orderHistory: OrderHistory!
    var guestNotes: Array<Note>!
    var guestTags: Array<Code>!
    var delegate: SplitViewGuestEventListener!
    var userStatistic: Statistic?
    
    @IBAction func onGuestFullNameClick(sender: UIButton) {
        delegate.onGuestNameButtonClick()
    }
    @IBAction func onGuesAddTagButtonClick(sender: UIButton) {
        delegate.onAddGuestTagButtonClick()
    }
    
    @IBAction func onGuestNoteAddButtonClick(sender: UIButton) {
        delegate.onAddGuestNoteButtonCLick()
    }

    @IBAction func onGuestProfileTabClick(sender: UIButton) {
        highlightOnlyCurrentTab(sender)
    }
    
    func switchGuestAndHistoryViews(isEnable: Bool) {
        tagsPanelContainer.hidden = isEnable
        historyPanelContainer.hidden = !isEnable
    }
    
    override func viewDidLoad() {
        updateUserStatistic()
        setupOrderData()
        historyPanelContainer.hidden = true
        reloadOrderHistoryTables()
        
    }
    
    override func viewDidDisappear(animated: Bool) {
        reloadOrderHistoryTables()
    }
    
    func reloadOrderHistoryTables() {
        self.orderHistoryTableView.reloadData()
    }
    
    func updateUserStatistic(){
        
        if self.user.id != nil{
            ServerApi.getInstance.getUserStatistic(self.user.id){ stats in
                self.userStatistic = stats
                self.setupUserStatistic()
            }
        }else{
            self.userStatistic = nil
            setupUserStatistic()
        }
    }
    
    func setupOrderData(){
        
        var fullName = ""
        var email = "Empty"
        var phoneNumber = "Empty"
        
        if user != nil{
            fullName = StringUtils.getFullNameFromUser(user)
            email = user.email.isEmpty ? email : user.email
            phoneNumber = (user.phoneNumber == nil || user.phoneNumber.isEmpty) ? phoneNumber : user.phoneNumber
            
        }
        
        guestTagsLabel.text = StringUtils.parseCodesToStringWithCommas(guestTags)
        guestNotesLabel.text = StringUtils.parseNotesToStringWithCommas(guestNotes)
        guestFullName.setTitle(fullName, forState: .Normal)
        guestEmailAdress.text = email
        guestPhoneNumber.text = phoneNumber
    }
    
    func setupUserStatistic(){
        var resNumb = 0
        var cancelNumb = 0
        var noShowsNumb = 0
        var walkInsNumb = 0
        var avgTurnTimeNumb = 0
        
        if userStatistic != nil {
            resNumb = userStatistic!.ordersTotal
            noShowsNumb = userStatistic!.noShowsTotal
            walkInsNumb = userStatistic!.walkinsTotal
            avgTurnTimeNumb = userStatistic!.averageSeatedTimeMinutes
            cancelNumb = userStatistic!.cancelledTotal
        }
        
        reservationNumber.text = String(resNumb)
        cancellationsNumber.text = String(cancelNumb)
        noShowsNumber.text = String(noShowsNumb)
        walkInsNumber.text = String(walkInsNumb)
        avgTurnTimeNumber.text = String(avgTurnTimeNumb)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderHistory.past.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let historyCell = tableView.dequeueReusableCellWithIdentifier("orderHistoryCellId") as! GuestHistoryViewCell
        
        let past = orderHistory.past
        
        historyCell.pastDateLabel.text! = TimeUtils.getYearMonthDayInString(String(past[indexPath.row].orderDateTime))
        historyCell.pastTimeLabel.text! = TimeUtils.getHourMinuteSecondInString(past[indexPath.row].holdOnDateTime)
        historyCell.pastGuestsCountLabel.text! = String(past[indexPath.row].guestCount)
        historyCell.pastTableLabel.text! = past[indexPath.row].orderedTable.name
        historyCell.pastDurationLabel.text! = String(past[indexPath.row].durationMinutes)
        
        return historyCell
    }
    
    func highlightOnlyCurrentTab(sender: UIButton) {
        noteTab.backgroundColor = UIUtils.COLOR_TAB_DEFAULT_BACKGROUND
        noteTab.tintColor = UIUtils.COLOR_TAB_DEFAULT_ICON
        clockTab.backgroundColor = UIUtils.COLOR_TAB_DEFAULT_BACKGROUND
        clockTab.tintColor = UIUtils.COLOR_TAB_DEFAULT_ICON
        
        sender.backgroundColor = UIUtils.COLOR_TAB_PRESSED_BACKGROUND
        
        let img: UIImage = sender.imageForState(UIControlState.Normal)!
        img.imageWithRenderingMode(.AlwaysTemplate)
        sender.tintColor = UIColor.whiteColor()
        
        if isHistoryTab(sender.tag){
            updateOrderHistory()
            reloadOrderHistoryTables()
            switchGuestAndHistoryViews(true)
        } else {
            switchGuestAndHistoryViews(false)
        }
    }
    
    func updateOrderHistory(){
        orderHistory = delegate.getUpdatedOrderHistory()
    }
    
    func isNoteTab(tag: Int) -> Bool{
        return tag == Tag.GUEST_PROFILE_NOTE_TAB
    }
    
    func isHistoryTab(tag: Int) -> Bool{
        return tag == Tag.GUEST_PROFILE_CLOCK_TAB
    }
}
