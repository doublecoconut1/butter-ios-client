//
//  ChangeTableViewController.swift
//  Butter
//
//  Created by Grigor Avagyan on 10/24/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol ChangeTableEventListener {
    func onCancelButtonClick()
    func onSaveButtonClick()
    func onRowClick(index: Int)
}

class SplitViewChangeTableViewController: UITableViewController {
    
    var orders = [Order]()
    var delegate: ChangeTableEventListener!
    
    override func viewDidLoad() {
        self.title = "Select row"
    }
    
    override func viewDidAppear(animated: Bool) {
        selectRow(0)
    }
    
    @IBAction func onSaveButtonClick(sender: UIBarButtonItem) {
        delegate.onSaveButtonClick()
    }
    
    @IBAction func onCancelButtonClick(sender: UIBarButtonItem) {
        delegate.onCancelButtonClick()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let currOrder = orders[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier(String(ChangeTableViewCell)) as! ChangeTableViewCell
        cell.fromTableLable.text = currOrder.orderedTable.name
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.contentView.backgroundColor = UIUtils.COLOR_GRAY_LIGHT
        delegate.onRowClick(indexPath.row)
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.contentView.backgroundColor = UIColor.whiteColor()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func setTableLabel(index: Int, text: String){
        let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as! ChangeTableViewCell
        cell.toTableLabel.text = text
    }
    
    func selectRow(index: Int){
        let firstRowPath = NSIndexPath(forRow: index, inSection: 0)
        self.tableView.selectRowAtIndexPath(firstRowPath, animated: false, scrollPosition: .None)
        self.tableView(self.tableView, didSelectRowAtIndexPath: firstRowPath)
    }
}
