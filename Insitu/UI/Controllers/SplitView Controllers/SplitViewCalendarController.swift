//
//  SplitViewCalendarController.swift
//  Butter
//
//  Created by Harut on 8/8/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit
import CVCalendar

protocol SplitCalendarEventListener {
    func onCurrentDateChange(date: NSDate)
}

class SplitViewCalendarController: UIViewController,CVCalendarViewAppearanceDelegate, CVCalendarMenuViewDelegate, CVCalendarViewDelegate{
    
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var calendarView: CVCalendarView!
    @IBOutlet weak var monthLabel: UILabel!
    
    var delegate: SplitCalendarEventListener!
    var shouldShowDaysOut = true
    var animationFinished = true
    var selectedDay:DayView!
    var lastDate: NSDate!
    var currentDate = TimeUtils.now()
    var todayDate = TimeUtils.now()
    
    override func viewDidLoad() {
        
        self.calendarView.delegate = self
        self.menuView.delegate = self
        disablePreviousDays()
        monthLabel.text = CVDate(date: currentDate).globalDescription
        
    }
    
    func presentationMode() -> CalendarMode{
        return .MonthView
    }
    func firstWeekday() -> Weekday{
        return .Sunday
    }
    
    func toggleMonthViewWithMonthOffset(offset: Int) {
        let calendar = NSCalendar.currentCalendar()
        let components = Manager.componentsForDate(todayDate) // from today
        
        components.month += offset
        
        let resultDate = calendar.dateFromComponents(components)!
        self.calendarView.toggleViewWithDate(resultDate)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        menuView.commitMenuViewUpdate()
        calendarView.commitCalendarViewUpdate()
    }
    
    func didSelectDayView(dayView: CVCalendarDayView, animationDidFinish: Bool) {
        disablePreviousDays()
        disableNextDays()
        selectedDay = dayView
        delegate.onCurrentDateChange(dayView.date.convertedDate()!)
    }
    
    func getDateDescription(date: CVDate) -> String{
        return TimeUtils.dateToString(date.convertedDate()!, dateFormat: MMMMYYYY)
    }
    
    func disablePreviousDays() {
        for weekV in calendarView.contentController.presentedMonthView.weekViews {
            for dayView in weekV.dayViews {
                if !TimeUtils.isSameDate(dayView.date.convertedDate()!, secondDate: todayDate) && dayView.date.convertedDate()! < todayDate{
                    dayView.userInteractionEnabled = false
                    dayView.dayLabel.textColor = calendarView.appearance.dayLabelWeekdayOutTextColor
                }
            }
        }
    }
    
    func disableNextDays() {
        for weekV in calendarView.contentController.presentedMonthView.weekViews {
            for dayView in weekV.dayViews {
                if !TimeUtils.isSameDate(dayView.date.convertedDate()!, secondDate: todayDate) && dayView.date.convertedDate()! > lastDate{
                    dayView.userInteractionEnabled = false
                    dayView.dayLabel.textColor = calendarView.appearance.dayLabelWeekdayOutTextColor
                }
            }
        }
    }
    
    func switchMonthPosition(date: NSDate){
        
        if TimeUtils.isFirstDateMonthBeforeSecondDateMonth(date, second: todayDate){
            calendarView.loadNextView()
        }else if TimeUtils.isFirstDateMonthBeforeSecondDateMonth(lastDate, second: date){
            calendarView.loadPreviousView()
        }
    }
    
    func presentedDateUpdated(date: CVDate) {
        switchMonthPosition(date.convertedDate()!)
        if monthLabel.text != getDateDescription(date) && self.animationFinished {
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .Center
            updatedMonthLabel.text = getDateDescription(date)
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = self.monthLabel.center
            
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransformMakeTranslation(0, offset)
            updatedMonthLabel.transform = CGAffineTransformMakeScale(1, 0.1)
            
            UIView.animateWithDuration(0.35, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                self.animationFinished = false
                self.monthLabel.transform = CGAffineTransformMakeTranslation(0, -offset)
                self.monthLabel.transform = CGAffineTransformMakeScale(1, 0.1)
                self.monthLabel.alpha = 0
                
                updatedMonthLabel.alpha = 1
                updatedMonthLabel.transform = CGAffineTransformIdentity
                
            }) { _ in
                
                self.animationFinished = true
                self.monthLabel.frame = updatedMonthLabel.frame
                self.monthLabel.text = updatedMonthLabel.text
                self.monthLabel.transform = CGAffineTransformIdentity
                self.monthLabel.alpha = 1
                updatedMonthLabel.removeFromSuperview()
            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
        }
    }
    
}