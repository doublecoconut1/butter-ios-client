//
//  ViewController.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/11/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class GuestsViewController: UIViewController {

    @IBOutlet var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSlidingMenu();
    }

    func setupSlidingMenu() {
        if self.revealViewController() != nil{
            menuButton.target = self.revealViewController();
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:));
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer());
        }
    }

}

