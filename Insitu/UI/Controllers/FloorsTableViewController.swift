//
//  RoomsTableViewController.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/13/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class FloorsTableViewController: UITableViewController {
    
    var delegate : SendDataDelegate!
    var floors: [FloorInfo]!
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return floors.count;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell();
        cell.textLabel?.text = floors[indexPath.row].name;
        return cell;
    }
    
    override func viewWillAppear(animated: Bool) {
        tableView.layoutIfNeeded();
        preferredContentSize = tableView.contentSize;
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        delegate.onComplete(String(FloorsTableViewController), object: floors[indexPath.row]);
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    

}
