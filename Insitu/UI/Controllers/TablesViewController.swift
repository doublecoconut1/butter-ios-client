//
//  TablesViewController.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/12/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class TablesViewController: UIViewController {

    @IBOutlet var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil{
            menuButton.target = self.revealViewController();
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:));
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer());
        }
    }

}
