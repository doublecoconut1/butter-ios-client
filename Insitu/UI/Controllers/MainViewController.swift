//
//  MainViewController.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/11/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit
import ObjectMapper

public enum AlertActions{
    case CANCEL
    case CHANGE_TABLE
    case CHANGE_TABLE_AND_SEAT
    case MARK_PREVIOUS_DONE_AND_SEAT
    case MAKE_ALL_PREVIOUS_ORDERS_DONE
    case SEAT
    case MULTY_BOOK
    case CHANGE_SINGLE_ORDER
    case CHANGE_ALL_ORDERS
    case CHANGE_ORDER_TABLES
    case SEAT_ON_AVAILABLE_TABLES
    case CONTINUE
}

public enum DragResult{
    case CANCEL
    case SHOW_NEW_RESERVATION
    case SEAT_GUEST
    case MARK_PREVIOUS_DONE_AND_SEAT
    case SEAT_GUEST_ANYWAY
}

public enum SortType: String{
    case TIME_AND_NAME = "orderDateTime,firstName,lastname"
    case TIME = "orderDateTime"
    case NAME = "firstName,lastname"
}

class MainViewController: UIViewController, UIGestureRecognizerDelegate, UIPopoverPresentationControllerDelegate,
    InfoTableEventListener, PersonInfoEventListener, SendDataDelegate, AddServerPopoverEventListener, SplitRootEventListener, SplitViewChangeTableEventListener{
    
    @IBOutlet weak var rootView: UIView!
    @IBOutlet var dateButton: UIButton!
    @IBOutlet weak var todayButton: UIButton!
    @IBOutlet var floorButton: UIButton!
    @IBOutlet var menuButton: UIBarButtonItem!
    @IBOutlet var bottomNavView: UIView!
    @IBOutlet var seatedTimeButton: UIButton!
    @IBOutlet var upcomingButton: UIButton!
    @IBOutlet weak var mainLeftPartView: UIView!
    @IBOutlet weak var newReservationButton: UIButton!
    @IBOutlet weak var capacityLabel: UILabel!
    @IBOutlet weak var coversLabel: UILabel!
    @IBOutlet weak var waitListButton: UIButton!
    
    var roomView: UIView = UIView()
    var floors: [FloorInfo]!
    var currentFloor: FloorInfo!
    
    var draggableView:DraggableView = DraggableView()
    var currentDateData : CurrentDateDataResponse!
    
    var currentDate = TimeUtils.now()
    var currentTableTabTag: Int = Tag.RESERVATION_TAB
    
    var upcomingOrders : [Order] = []
    var allOrders = [Order]()
    var allServers = [Server]()
    var waitList = [WaitList]()
    var sortedOrders = [Order]()
    var sortedServers = [Server]()
    var sortedUpcomingOrders = [Order]()
    var sortedWaitlist = [WaitList]()
    
    var roomTableListDic: Dictionary<Int,TableInfo> = Dictionary<Int,TableInfo>()
    
    var clickedCellIndex: NSIndexPath? = nil
    var defaultTableBorder: CGColor!
    var lastHighlightedTableTag: Int = 0
    var rootViewZPosition: CGFloat!
    var draggableOrder: Order!
    var sortBy = SortType.TIME_AND_NAME
    var searchText: [String]!
    var serverUpdateTimer: NSTimer!
    var availableTables: AvailableTables!
    
    var roomLongPressGesture: UILongPressGestureRecognizer!
    
    
    @IBOutlet weak var infoTableContainer: UIView!
    @IBOutlet weak var personInfoContainer: UIView!
    
    var personInfoController: PersonInfoViewController?
    var infoTableController: InfoTableViewController?
    var addServerPopoverController: AddServerPopoverController?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
        setupDateAndTodayButton()
        setupNewreservationButton()
        setupNavBar()
        setupMainLeftPartView()
        getCurrentDateInfo()
        //setupSlidingMenu()
    }
    @IBAction func onTodayButtonClick(sender: UIButton) {
        currentDate = TimeUtils.now()
        getServerData()
        dateButton.setTitle(TimeUtils.dateToString(currentDate, dateFormat: MMDDYYYY), forState: UIControlState.Normal)
    }
    
    @IBAction func onWaitListButtonClick(sender: UIButton) {
        showSplitViewModal(nil, orderedTable: nil, orderCategory: OrderCategory.WAITLIST)
    }
    
    @IBAction func onNewReservationButtonCLick(sender: UIButton) {
        showSplitViewModal(nil, orderedTable: nil, orderCategory: OrderCategory.NEW_RESERVATION)
    }
    
    func onPersonFullNameClick(order: Order) {
        showSplitViewModal(order, orderedTable: nil, orderCategory: OrderCategory.EXISTING_ORDER)
    }
    
    func getCurrentDateInfo(){
        ServerApi.getInstance.getFloorsInfo(){ result in
            let floorArr = [result[2]]
//            let floorArr = result
            self.floors = floorArr
            self.currentFloor = self.floors[0]
            self.updateNavbarFloorTitle()
            self.getServerData()
        }
    }
    
    func getServerData(){
        
        if self.roomLongPressGesture != nil {
            self.disableRoomLongPressGesture()
        }
        let date = TimeUtils.dateToString(self.currentDate, dateFormat: YYYYMMDD)
        ServerApi.getInstance.getCurrentDateInfo(["date": date, "service" : 1, "floor" : self.currentFloor.number!, "sort" : self.sortBy.rawValue]){ currDateData in
            self.currentDateData = currDateData
            self.getAvailableTables()
            self.setOpeningTime()
            self.initOrders()
            self.initServers()
            self.initWaitList()
            self.createRoom()
            self.reloadInfoTable()
            self.enableRoomLongPressGesture()
        }
    }
    
    func addServer(name: String, serverColor: String) {
        if name != "" && serverColor != "" {
            let addServer = Server()
            addServer.firstName = name
            addServer.color = serverColor
            addServer.coverCurrent = 0
            addServer.coverTotal = 0
            allServers.append(addServer)
            reloadInfoTable()
        }
    }
    
    func getAvailableTables() {
        ServerApi.getInstance.getAvailableTables(1, date: TimeUtils.now()) { tables in
            self.availableTables = tables
        }
    }
    
    func updateNavbarFloorTitle(){
        floorButton.setTitle(currentFloor.name, forState: .Normal)
    }
    
    func showChangeTableController(orders: [Order], status: GuestStatus){
        
        stopTimerUpdate()
        
        let controller = UIUtils.getControllerFromStoryboard(self, controllerName: String(SplitViewChangeTableRootController)) as! SplitViewChangeTableRootController
        
        controller.currentDateData = currentDateData
        controller.availableTables = availableTables
        controller.orders = orders
        controller.delegate = self
        controller.status = status
        controller.modalPresentationStyle = .OverCurrentContext
        
        self.presentViewController(controller, animated: true, completion: {() -> Void in
            self.infotableContainerToFront()
        })
        
    }
    
    func showSplitViewModal(order: AnyObject?, orderedTable: Tables?, orderCategory: OrderCategory){
        
        let controller = UIUtils.getControllerFromStoryboard(self, controllerName: String(SplitViewRootController)) as! SplitViewRootController
        
        if (order as? Order) != nil {
            controller.currentOrder = order as! Order
        }else if (order as? WaitList) != nil{
            controller.currentWaitOrder = order as! WaitList
        }
        
        controller.delegate = self
        controller.orderCategory = orderCategory
        controller.orderedTable = orderedTable
        controller.floorNumber = currentFloor.number
        controller.sortBy = sortBy
        controller.modalPresentationStyle = .OverCurrentContext
        stopTimerUpdate()
        
        self.presentViewController(controller, animated: true, completion: {() -> Void in
            self.infotableContainerToFront()
        })
    }
    
    func setupNewreservationButton(){

        newReservationButton.layer.cornerRadius = newReservationButton.bounds.width / 2
        newReservationButton.layer.zPosition = CGFloat.max
    }
    
    func getSortedData(sortType: SortType) {
        sortBy = sortType
        getServerData()
    }
    
    func filterByName(nameArr: [String]?) {
        
        searchText = nameArr
        sortedOrders = allOrders
        sortedServers = allServers
        sortedUpcomingOrders = upcomingOrders
        sortedWaitlist = waitList
        
        
        if nameArr != nil{
            sortedOrders = [Order]()
            sortedServers = [Server]()
            sortedWaitlist = [WaitList]()
            
            for order in allOrders{
                if order.fake != nil {
                    outer: for currOrder in order.childOrderList{
                        for name in nameArr! {
                            if StringUtils.objectNameContainsChar(currOrder.user, searchText: name){
                                sortedOrders.append(order)
                                break outer
                            }
                        }
                    }
                }else {
                    for name in nameArr! {
                        if StringUtils.objectNameContainsChar(order.user, searchText: name){
                            sortedOrders.append(order)
                            break
                        }
                    }
                }
            }
            
            for server in allServers{
                for name in nameArr! {
                    if StringUtils.objectNameContainsChar(server, searchText: name){
                        sortedServers.append(server)
                        break
                    }
                }
            }
            
            for waitOrder in waitList{
                for name in nameArr! {
                    if StringUtils.objectNameContainsChar(waitOrder.user, searchText: name){
                        sortedWaitlist.append(waitOrder)
                        break
                    }
                }
            }
            
            sortedUpcomingOrders = RoomUtils.filterUpcomingOrders(sortedOrders)
        }
    }
    
    func initServers(){
        self.allServers = self.currentDateData.servers
    }
    
    func initOrders(){
        self.allOrders = self.currentDateData.orders
    }
    
    func initWaitList() {
        self.waitList = self.currentDateData.waitlist
    }
    func updateCapacityCountAndServerAssignments(){
        
        var coversCount = 0
        
        for(_, value) in roomTableListDic{
            coversCount += value.table.type.maxSeats
            
            let tableView = findTableViewById(value.table.id)
            if value.assignment != nil {
                tableView.layer.borderColor = UIUtils.hexToUIColor(value.assignment.server.color).CGColor
            }else{
                tableView.layer.borderColor = UIUtils.COLOR_TABLE_BORDER.CGColor
            }
        }
        capacityLabel.text = String(coversCount)
    }
    
    func updateGuestCount(){
        let guestCount = RoomUtils.getSeatedOrdersGuestCount(0, orders: allOrders)
        coversLabel.text = String(guestCount)
    }
    
    func updateTableInfoFromAllOrders(){
        roomTableListDic = RoomUtils.updateTableOrders(allOrders, roomTableDic: roomTableListDic)
    }
    
    func setOpeningTime() {
        UserDefaults.getInstance.setOpeningTime(currentDateData.date)
    }
    
    func setupSlidingMenu(){
        if self.revealViewController() != nil{
            menuButton.target = self.revealViewController();
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:));
        }
    }
    
    func setupDateAndTodayButton(){
        waitListButton.backgroundColor = UIColor.clearColor()
        waitListButton.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        waitListButton.layer.cornerRadius = 5
        waitListButton.layer.borderWidth = 1
        waitListButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        todayButton.backgroundColor = UIColor.clearColor()
        todayButton.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        todayButton.layer.cornerRadius = 5
        todayButton.layer.borderWidth = 1
        todayButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        dateButton.backgroundColor = UIColor.clearColor()
        dateButton.contentEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        dateButton.layer.cornerRadius = 5
        dateButton.layer.borderWidth = 1
        dateButton.layer.borderColor = UIColor.whiteColor().CGColor
        dateButton.setTitle(TimeUtils.dateToString(currentDate, dateFormat: MMDDYYYY), forState: UIControlState.Normal);
    }
    
    
    func setupNavBar(){
        self.navigationItem.title = "BUTTER - Tables"
        let nav = self.navigationController?.navigationBar
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIUtils.COLOR_DEF_BLUE]
        
    }
    
    func setupMainLeftPartView(){
        mainLeftPartView.backgroundColor = UIUtils.COLOR_ROOM;
    }
    
    func initialize(){
        self.restartTimer()
        self.view.backgroundColor = UIUtils.COLOR_ROOM
    }
    
    func reloadInfoTable(){
        upcomingOrders = RoomUtils.filterUpcomingOrders(allOrders)
        
        self.updateTableInfoUpcomingOrders()
        self.updateCapacityCountAndServerAssignments()
        self.updateGuestCount()
        self.updateSeatedGuestsInRoom()
        self.showTableInfoViewsIfNecessary()
        self.filterByName(self.searchText)
        self.infoTableController!.reloadInfoTable()
    }
    
    func updateSeatedGuestsInRoom(){
        for(_, value) in roomTableListDic{
            if value.currentOrder != nil{
                
                let tableId = value.currentOrder.orderedTable.id
                let tableView = findTableViewById(tableId)
                
                if StatusUtils.isSeatedCategory(value.currentOrder.status){
                    removeChairFromTable(tableId)
                    addStatusPicToTable(tableView, status: value.currentOrder.status)
                }else{
                    removeChairFromTable(tableId)
                }
            }
        }
    }
    
    func showTableInfoViewsIfNecessary(){
        if isUpcomingBtnSelected() {
            RoomUtils.reloadUpcomingTablesTimeInfo(roomView, roomTableDic: roomTableListDic)
        }else if isSeatedBtnSelected(){
            reloadSeatedTimeInfo()
        }
    }
    
    func disableRoomLongPressGesture(){
        roomLongPressGesture.enabled = false
    }
    
    func enableRoomLongPressGesture(){
        roomLongPressGesture.enabled = true
    }
    
    func createRoom() {
        self.roomView = RoomFactory.createRoom(self.mainLeftPartView, floor: currentDateData.floor, isVisibleMaxMinSizes: false)
        
        roomLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(MainViewController.onRoomLongPress(_:)))
        roomLongPressGesture.minimumPressDuration = 0.12
        roomLongPressGesture.delaysTouchesBegan = true
        roomLongPressGesture.delegate = self
        
        let roomTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.onRoomTableTap(_:)))
        roomTapGesture.numberOfTapsRequired = 1
        roomTapGesture.numberOfTouchesRequired = 1
        
        self.roomView.addGestureRecognizer(roomLongPressGesture)
        self.roomView.addGestureRecognizer(roomTapGesture)
        
        self.mainLeftPartView.subviews.forEach({ $0.removeFromSuperview() })
        self.mainLeftPartView.addSubview(roomView)
        
        roomTableListDic = RoomFactory.createTableObjectsWithPosition(roomView, currentDateData: currentDateData, availableTables: nil)
    }
    
    @IBAction func onSeatedTimeButtonClick(sender: UIButton?) {
        RoomUtils.onSeatedTimeButtonClick(seatedTimeButton, upcomingBtn: upcomingButton, roomView: roomView, roomTableListDic: roomTableListDic)
    }
    
    @IBAction func onUpcomingButtonClick(sender: UIButton?) {
        RoomUtils.onUpcomingButtonClick(seatedTimeButton, upcomingBtn: upcomingButton, roomView: roomView, roomTableListDic: roomTableListDic)
    }
    
    func getUpdate() {
        getServerData()
        restartTimer()
    }
    
    func restartTimer(){
        stopTimerUpdate()
        startTimerUpdate()
    }
    
    func onPersonOrderUpdate(order: Order) {
        let orderDate = TimeUtils.stringToDate(order.orderDateTime, dateFormat: YYYYMMDDHHMMSS)
        if TimeUtils.isSameDate(orderDate, secondDate: currentDate) {
            allOrders[findOrderIndex(order)] = order
        }else{
            removeOrder(order)
        }
        reloadInfoTable()
    }
    
    func startTimerUpdate(){
        print("startTimerUpdate")
        let seconds: NSTimeInterval = 20
        serverUpdateTimer = nil
        serverUpdateTimer = NSTimer.scheduledTimerWithTimeInterval(seconds, target: self, selector: #selector(self.getServerData), userInfo: nil, repeats: true)
    }
    
    func reloadSeatedTimeInfo(){
        RoomUtils.reloadSeatedTimeInfo(roomView, roomTableDic: roomTableListDic)
    }
    
    func stopTimerUpdate(){
        if serverUpdateTimer != nil {
            print("update stoped")
            serverUpdateTimer.invalidate()
            serverUpdateTimer = nil
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "roomsPopOver" {
            if let controller = segue.destinationViewController as? FloorsTableViewController {
                controller.popoverPresentationController!.delegate = self
                controller.popoverPresentationController?.sourceRect = floorButton.frame
                controller.preferredContentSize = CGSize(width: 320, height: 186)
                controller.floors = floors
                controller.delegate = self;
            }
        }else if segue.identifier == "pickDate" {
            if let controller = segue.destinationViewController as? DateViewController {
                controller.popoverPresentationController!.delegate = self
                controller.preferredContentSize = CGSize(width: 320, height: 186)
                controller.delegate = self;
                controller.currentDate = currentDate
            }
        }else if segue.identifier == "personInfo" {
            if let controller = segue.destinationViewController as? PersonInfoViewController{
                personInfoController = controller
                personInfoController?.delegate = self
            }
        }else if segue.identifier == "tableInfo" {
            if let controller = segue.destinationViewController as? InfoTableViewController{
                infoTableController = controller
                infoTableController?.delegate = self
            }
        }
    }
    
    func onServerButtonClick(serverPlusbutton: UIButton) {
        let popoverContent = (self.storyboard?.instantiateViewControllerWithIdentifier(String(AddServerPopoverController)))! as! AddServerPopoverController
        popoverContent.delegate = self
        let nav = UINavigationController(rootViewController: popoverContent)
        nav.modalPresentationStyle = UIModalPresentationStyle.Popover
        let popover = nav.popoverPresentationController
        popover!.delegate = self
        popover!.sourceView = serverPlusbutton
        
        self.presentViewController(nav, animated: true, completion: nil)
    }
    
    func deleteSelectedServer(index: Int) {
        allServers.removeAtIndex(index)
        reloadInfoTable()
    }
    
    func onComplete(sender: String, object: AnyObject) {
        if sender == String(FloorsTableViewController) {
            currentFloor = object as! FloorInfo
            self.updateNavbarFloorTitle()
            getServerData()
            return
        }
        
        if sender == String(DateViewController) {
            let date: String = TimeUtils.dateToString(object as! NSDate, dateFormat: MMDDYYYY);
            dateButton.setTitle(date, forState: UIControlState.Normal);
            currentDate = object as! NSDate
            getServerData()
            return;
        }
    }
    
    func removeOrder(order: Order){
        allOrders.removeAtIndex(findOrderIndex(order))
        removeOrderFromRoom(order)
    }
    
    func findOrderIndex(order: Order) -> Int{
        for i in 0 ..< allOrders.count {
            let currentOrder = allOrders[i]
            if currentOrder.fake != nil{
                for childOrder in currentOrder.childOrderList{
                    if childOrder.id == order.id {
                        return i
                    }
                }
            }
            else if currentOrder.id == order.id {
                return i
            }
        }
        return -1
    }
    
    func onGuestStatusChanged(order: Order, status: GuestStatus, isMulti: Bool) {
        
        if isMulti{
            let tables = RoomUtils.getOrderedTablesByParentId(order.parentId, orders: allOrders)
            
            if StatusUtils.isSeatedCategory(status){
                
                let toSeatOrders = RoomUtils.getGroupedOrderByParentId(order.parentId, orders: allOrders)!.childOrderList
                let toDoneOrders = RoomUtils.getOccupiedTableOrdersByParentId(order.parentId, tables: tables, roomTableDic: roomTableListDic)
                
                if toDoneOrders.count != 0{
                    Alerts.getInstance.occupiedTablesAction(self, orders: toDoneOrders){ action in
                        switch action{
                        case .SEAT_ON_AVAILABLE_TABLES:
                            var orders = RoomUtils.removeOrdersFromAllOrders(toDoneOrders, allOrders: toSeatOrders)
                            orders = self.setOrdersStatus(orders, status: status)
                            self.onAllOrdersUpdated(orders)
                            break
                        case .CHANGE_ORDER_TABLES:
                            self.showChangeTableController(toSeatOrders, status: status)
                            break
                        case .MARK_PREVIOUS_DONE_AND_SEAT:
                            self.setOrdersStatus(toSeatOrders, status: status)
                            ServerApi.getInstance.updateAllTableStatusesToDone(RoomUtils.getTablesFromOrders(toDoneOrders)){ response in
                                  self.updateOrderStatusesByParentId(order.parentId, status: status)
                            }
                            break
                        default:
                            print("cancel")
                            break
                        }
                    }
                }else{
                    setOrdersStatus(toSeatOrders, status: status)
                    self.updateOrderStatusesByParentId(order.parentId, status: status)
                }
                return
                    
                
            }else if StatusUtils.isCancelledCategory(status){
                infotableContainerToFront()
            }
            self.updateOrderStatusesByParentId(order.parentId, status: status)
        }
        else if StatusUtils.isCancelledCategory(status) {
            order.status = status
            removeOrder(order)
            infotableContainerToFront()
            updateSingleOrderStatusAndReload(order)
            
        }else{
            
            let tableInfo = roomTableListDic[order.orderedTable.id]
            if tableInfo?.currentOrder != nil && tableInfo?.currentOrder.id != order.id && StatusUtils.isSeatedCategory(status){
                onDragFinished(order, tableInfo: tableInfo!){ order in
                    self.updateSingleOrderStatusAndReload(order!)
                }
            }else{
                order.status = status
                setOrdersStatus([order], status: status)
                self.updateSingleOrderStatusAndReload(order)
            }
        }
    }
    
    func onAllOrdersUpdated(orders: [Order]){
        var counter = 0
        
        for currentOrder in orders{
            ServerApi.getInstance.updateSingleOrderStatus(currentOrder){ result in
                counter += 1
                if counter == orders.count{
                    self.getServerData()
                }
            }
        }
    }
    
    func setOrdersStatus(orders: [Order], status: GuestStatus) -> [Order]{
        for currOrder in orders{
            currOrder.status = status
            allOrders[findOrderIndex(currOrder)].status = status
        }
        self.updateTableInfoFromAllOrders()
        self.reloadInfoTable()
        return orders
    }
    
    func updateOrderStatusesByParentId(parentId: Int, status: GuestStatus){
        ServerApi.getInstance.updateOrderStatusesByParentId(parentId, status: status){ callback in
            self.getServerData()
        }
    }
    
    func updateSingleOrderStatusAndReload(order: Order){
        self.updateTableInfoFromAllOrders()
        self.reloadInfoTable()
        ServerApi.getInstance.updateSingleOrderStatus(order){response in
            self.getServerData()
        }
    }
    
    func updateTableInfoFromAllOrdersAndReload(order: Order){
        self.updateTableInfoFromAllOrders()
        self.reloadInfoTable()
        self.sendUpdatedOrderAndGetUpdate(order)
    }
    
    func updateTableInfoUpcomingOrders(){
        
        for (_,value) in roomTableListDic {
            value.upcomingList = RoomUtils.getCurrentTableUpcomingOrders(value.id, upcomingOrders: upcomingOrders)
        }
        
    }
    
    func onRoomTableTap(gesture: UITapGestureRecognizer){
        let position = gesture.locationInView(self.view)
        let tableInfo = getTableFromPosition(position)
        if tableInfo != nil && tableInfo?.currentOrder != nil {
            updatePersonInfoViewAndShow((tableInfo?.currentOrder)!)
        }else if tableInfo != nil{
            showSplitViewModal(nil, orderedTable: tableInfo?.table, orderCategory: OrderCategory.WALK_IN)
        }
    }
    
    func getTableFromPosition(point: CGPoint) -> TableInfo? {
        return RoomUtils.getTableFromPosition(point, roomTableListDic: roomTableListDic)
    }
    
    func getCurrentTableItemsCount() -> Int {
        if currentDateData != nil {
            switch currentTableTabTag {
            case Tag.UPCOMING_TAB:
                return sortedUpcomingOrders.count
            case Tag.RESERVATION_TAB:
                return sortedOrders.count
            case Tag.SERVERS_TAB:
                return sortedServers.count
            case Tag.WAITLIST_TAB:
                return sortedWaitlist.count
            default:
                return 0;
            }
        }
        return 0;
    }
    
    func onTableItemClicked(parentIndex: Int, childIndex: Int?) {
        
        if !isServerTab(){
            if isUpcomingTab() || isReservationTab(){
                var order = isUpcomingTab() ? sortedUpcomingOrders[parentIndex] : sortedOrders[parentIndex]
                if childIndex != nil {
                    order = order.childOrderList[childIndex!]
                }
                updatePersonInfoViewAndShow(order)
            }else{
                showSplitViewModal(sortedWaitlist[parentIndex], orderedTable: nil, orderCategory: OrderCategory.EXISTING_WAITLIST)
            }
        }
    }
    
    func updatePersonInfoViewAndShow(order: Order){
        stopTimerUpdate()
        personInfoController?.order = order
        personInfoController?.setupInfo()
    }
    
    func onTableTabTagChanged(tag: Int) {
        currentTableTabTag = tag
    }
    
    func getCurrentTableCellByIndex(index: Int) -> AnyObject {
        
        if isServerTab() {
            return sortedServers[index]
        }else if isUpcomingTab(){
            return sortedUpcomingOrders[index]
        }else if isReservationTab(){
            return sortedOrders[index]
        }else{
            return sortedWaitlist[index]
        }
    }
    
    func onRoomLongPress(gest: UILongPressGestureRecognizer) {
        let point: CGPoint = gest.locationInView(self.view);
        print(point)
        dragPopup(gest, point: point)
    }
    
    func infotableContainerToFront(){
        personInfoContainer.slideInToBorder()
        personInfoContainer.subviews[0].frame.origin.x = personInfoContainer.frame.width
        personInfoContainer.hidden = true
        
        self.view.bringSubviewToFront(newReservationButton)
    }
    
    func personInfoContainerToFront(){
        personInfoContainer.subviews[0].frame.origin.x = 0
        personInfoContainer.hidden = false
        personInfoContainer.slideInFromBorder()
        
        self.view.bringSubviewToFront(personInfoContainer)
        self.view.bringSubviewToFront(newReservationButton)
    }
    
    func onPersonViewSetupFinished(){
        personInfoContainerToFront()
    }
    
    func onPersonViewClosing(){
        infotableContainerToFront()
        restartTimer()
    }
    
    func removeCurrentTableSeatedTimeView(tableView: UIView){
        tableView.superview?.viewWithTag(Tag.SEATED_TABLE_TIME_VIEW)?.removeFromSuperview()
    }
    
    func dragPopup(gest: UILongPressGestureRecognizer,point: CGPoint){
        
        UIUtils.transformViewToPosition(draggableView ,point: point)
        let gestureView = gest.view
        let isTableView = gestureView is UITableView
        var indexPath: NSIndexPath!
        
        if (gest.state == UIGestureRecognizerState.Began){
            
            stopTimerUpdate()
            rootViewToFront()
            
            if isTableView {
                let tableView = gestureView as! UITableView
                tableView.alwaysBounceVertical = false
                let p = gest.locationInView(tableView)
                indexPath = tableView.indexPathForRowAtPoint(p)
            
                if let index = indexPath {
                    
                    let section = (tableView != infoTableController!.contentTableView)
                        ? infoTableController?.selectedIndex : -1
                    
                    clickedCellIndex = NSIndexPath(forRow: index.row, inSection: section!)
                    createDraggableView(point, tableInfo: nil)
                }
            }else{
                let tableInfo = getTableFromPosition(point);
                if(tableInfo != nil){
                    if tableInfo?.currentOrder != nil {
                        let tableView = findTableViewById((tableInfo?.id)!)
                        createDraggableView(point, tableInfo: tableInfo)
                        removeChairFromTable((tableInfo?.id)!)
                        draggableOrder = tableInfo?.currentOrder
                        removeCurrentTableSeatedTimeView(tableView)
                    }else{
                        print("not a table")
                    }
                }
            }
            return
        }
        if (gest.state == UIGestureRecognizerState.Changed) {
            let tableInfo = getTableFromPosition(point)
            if tableInfo != nil && rootView.viewWithTag(Tag.DRAGGABLE_VIEW_TAG) != nil{
                disableTableBorder()
                lastHighlightedTableTag = (tableInfo?.id)!
                highlightTableBorder()
                
            }else{
                disableTableBorder()
                lastHighlightedTableTag = 0
            }
            return
        }
        
        if(gest.state == UIGestureRecognizerState.Ended){
            
            rootViewToDefaultPosition()
            disableTableBorder()
            removeDraggableView()
            
            if lastHighlightedTableTag != 0 {
                let tableInfo : TableInfo = roomTableListDic[lastHighlightedTableTag]!
                
                if clickedCellIndex != nil {
                    if isServerTab(){
                        updateServerAssignmentOnServer(tableInfo)
                    }else if isWaitListTab(){
                        onDragFinished(getClickedCellOrder() as! WaitList, tableInfo: tableInfo){ order in
                            self.reloadInfoTable()
                            self.updateTableInfoFromAllOrdersAndReload(order!)
                        }
                        
                    }else{
                        onDragFinished(getClickedCellOrder() as! Order, tableInfo: tableInfo){ order in
                            self.reloadInfoTable()
                            self.updateTableInfoFromAllOrdersAndReload(order!)
                        }
                    }
                }else{
                    onDragFinished(draggableOrder, tableInfo: tableInfo){ order in
                        self.reloadInfoTable()
                        self.updateTableInfoFromAllOrdersAndReload(order!)
                    }
                }
                showTableInfoViewsIfNecessary()
                
            }else if lastHighlightedTableTag == 0 && draggableOrder != nil{
                returnDraggableOrderToTable()
            }
            lastHighlightedTableTag = 0
            restartTimer()
        }
        
        if isTableView {
            (gestureView as! UITableView).alwaysBounceVertical = true
        }
    }
    
    func returnDraggableOrderToTable(){
        if draggableOrder != nil {
            let tableView = findTableViewById(draggableOrder.orderedTable.id)
            addStatusPicToTable(tableView, status: draggableOrder.status)
            showTableInfoViewsIfNecessary()
            draggableOrder = nil
        }
    }
    
    func removeOrderFromRoom(order: Order){
        for (_, value) in roomTableListDic {
            if value.currentOrder != nil && value.currentOrder.id == order.id {
                value.currentOrder = nil
                removeChairFromTable(value.table.id)
                draggableOrder = nil
                break
            }
        }

    }
    
    func updateServerAssignmentOnServer(tableInfo: TableInfo) {
        
        var serverExistOnTable = false
        let tableView = findTableViewById(tableInfo.id)
        
        if tableInfo.assignment != nil{
            serverExistOnTable = true
        }
        
        let server = getClickedCellOrder() as! Server
        
        UIUtils.setColorToBorder(tableView, color: UIUtils.hexToUIColor(server.color))
        
        if serverExistOnTable {
            tableInfo.assignment.server = server
            ServerApi.getInstance.updateServerOnTable(tableInfo){ result in
                self.getServerData()
            }
        }else{
            let currDate = TimeUtils.dateToString(currentDate, dateFormat: YYYYMMDD)
            
            ServerApi.getInstance.addServerToTable(tableInfo, server: server, currDate: currDate){result in
                self.getServerData()
            }
        }
    }
    
    func copyFromWaitToOrder(order: Order, waitOrder: WaitList) -> Order{
        order.user = waitOrder.user
        order.codes = waitOrder.codes
        order.notes = waitOrder.notes
        order.guestCount = waitOrder.guestCount
        order.orderDateTime = waitOrder.orderDateTime
        order.status = GuestStatus.SEATED
        order.waitList = waitOrder
        return order
    }
    
    func seatGuest(tableInfo: TableInfo, order: Order) -> Order {
        
        removeOrderFromRoom(order)
        if !StatusUtils.isSeatedCategory(order.status) {
            order.status = GuestStatus.SEATED
        }
        order.orderedTable = tableInfo.table
        tableInfo.currentOrder = order
        return order
    }
    

    
    func seatWaitingGuest(tableInfo: TableInfo, waitOrder: WaitList) -> Order {
        let order = copyFromWaitToOrder(Order(), waitOrder: waitOrder)
        order.orderedTable = tableInfo.table
        
        tableInfo.currentOrder = order
        return order
    }
    
    func markDoneAndSeatOrder(tableInfo: TableInfo, order: Order) -> Order{
        let currTableOrder = tableInfo.currentOrder
        currTableOrder.status = GuestStatus.DONE
        
        self.removeOrderFromRoom(order)
        
        order.orderedTable = tableInfo.table
        if !StatusUtils.isSeatedCategory(order.status) {
            order.status = GuestStatus.SEATED
        }
        
        self.sendUpdatedOrder(currTableOrder)
        
        tableInfo.currentOrder = order
        return order
    }
    
    func markDoneAndSeatWaitOrder(tableInfo: TableInfo, waitOrder: WaitList) -> Order{
        let currTableOrder = tableInfo.currentOrder
        currTableOrder.status = GuestStatus.DONE
        
        self.sendUpdatedOrder(currTableOrder)
        
        let order = copyFromWaitToOrder(Order(), waitOrder: waitOrder)
        order.orderedTable = tableInfo.table
        
        tableInfo.currentOrder = order
        return order
    }
    
    func seatGuestOnNotEnoughtSpaceTable(tableInfo: TableInfo, order: Order) -> Order{
        self.removeOrderFromRoom(order)
        
        order.orderedTable = tableInfo.table
        if !StatusUtils.isSeatedCategory(order.status) {
            order.status = GuestStatus.SEATED
        }
        
        tableInfo.currentOrder = order
        return order
    }
    
    func onDragFinished(dragedObject: AnyObject, tableInfo: TableInfo, callBack: (Order?) -> Void){
        
        let tableOrder = tableInfo.currentOrder
        let isOrder = dragedObject is Order
        let order = dragedObject as? Order
        let waitlist = dragedObject as? WaitList
        
        let guestCount = isOrder ? order!.guestCount : waitlist!.guestCount
        
        if isOrder && order!.fake != nil{
            let tableIsFromOrder = RoomUtils.isTableInOrderList(tableInfo.table, orders: order!.childOrderList)
            
            if tableIsFromOrder {
                onGuestStatusChanged(order!, status: .SEATED, isMulti: true)
            }else{
                let tableName = tableInfo.table.name
                Alerts.getInstance.tableIsNotFromGroupedOrder(self, tableName: tableName){ action in
                    if action == .CHANGE_ORDER_TABLES{
                        self.showChangeTableController(order!.childOrderList, status: .SEATED)
                    }
                }
            }
            return
        }
        
        if isOrder && tableOrder != nil{
            if order!.id == tableOrder.id {
                return self.returnDraggableOrderToTable()
            }
        }
        
        if guestCount > tableInfo.table.type.maxSeats {
            
            Alerts.getInstance.tableIsTooSmallForGuestCount(self){ action in
                
                if action == AlertActions.MARK_PREVIOUS_DONE_AND_SEAT{
                    if tableOrder != nil {
                        if isOrder{
                            callBack(self.markDoneAndSeatOrder(tableInfo, order: order!))
                        }else{
                            callBack(self.markDoneAndSeatWaitOrder(tableInfo, waitOrder: waitlist!))
                        }
                    } else {
                        if isOrder{
                            callBack(self.seatGuestOnNotEnoughtSpaceTable(tableInfo, order: order!))
                        }else{
                            callBack(self.seatWaitingGuest(tableInfo, waitOrder: waitlist!))
                        }
                    }
                }else if action == AlertActions.MULTY_BOOK {
                    
                    self.returnDraggableOrderToTable()
                    
                    if !isOrder{
                        let newOrder = self.copyFromWaitToOrder(Order(), waitOrder: waitlist!)
                        newOrder.orderedTable = tableInfo.table
                        self.showSplitViewModal(newOrder, orderedTable: nil, orderCategory: OrderCategory.WALK_IN_AFTER_RESERVATION)
                    }else{
                        let newOrder = order!
                        self.showSplitViewModal(newOrder, orderedTable: nil, orderCategory: OrderCategory.WALK_IN_AFTER_RESERVATION)
                    }
                }else{
                    self.returnDraggableOrderToTable()
                }
            }
        }
        else if tableOrder == nil {
            if isOrder{
                callBack(seatGuest(tableInfo, order: order!))
            }else{
                callBack(seatWaitingGuest(tableInfo, waitOrder: waitlist!))
            }
        }
        else if tableOrder != nil && (!isOrder || tableOrder.id != order!.id){
            
            Alerts.getInstance.showAlertToSetPreviousDoneAndSeatGuest(self){ action in
                
                if action == AlertActions.MARK_PREVIOUS_DONE_AND_SEAT{
                    if isOrder{
                        callBack(self.markDoneAndSeatOrder(tableInfo, order: order!))
                    }else{
                        callBack(self.markDoneAndSeatWaitOrder(tableInfo, waitOrder: waitlist!))
                    }
                }else{
                    print("canceled")
                }
                self.returnDraggableOrderToTable()
            }
        }else{
            print("something new in here")
        }
    }
    
    func changeTableAndSendOrderToServer(order: Order, tableInfo: TableInfo){
        removeOrderFromRoom(order)
        
        order.orderedTable = tableInfo.table
        self.reloadInfoTable()
        self.sendUpdatedOrderAndGetUpdate(order)
    }
    
    func getClickedCellOrder() -> AnyObject?{
        
        var order: Order!
        let row = clickedCellIndex!.row
        let section = clickedCellIndex!.section
        
        if self.isReservationTab(){
            order = (section != -1) ? sortedOrders[section].childOrderList[row] : sortedOrders[row]
            return order
        }else if self.isUpcomingTab(){
            order = (section != -1) ? sortedUpcomingOrders[section].childOrderList[row] : sortedUpcomingOrders[row]
            return order
        }else if self.isWaitListTab(){
            return sortedWaitlist[(clickedCellIndex?.row)!]
        }else{
            return sortedServers[(clickedCellIndex?.row)!]
        }
    }
    
    func sendUpdatedOrder(order: Order) {
        ServerApi.getInstance.forceUpdateOrder(order, callBack: nil)
    }
    
    func sendUpdatedOrderAndGetUpdate(order: Order){
        if order.id == nil {
            ServerApi.getInstance.postOrder(order){response in
                self.getServerData()
            }
        }else{
            ServerApi.getInstance.forceUpdateOrder(order){response in
                self.getServerData()
            }
        }
    }
    
    func removeChairFromTable(tableId: Int){
        let chairView = findTableViewById(tableId).superview!.viewWithTag(Tag.CHAIR_PIC_TAG)
        if  chairView != nil{
            chairView?.removeFromSuperview()
        }
    }
    
    func createDraggableView(point: CGPoint, tableInfo: TableInfo?){
        
        var color: UIColor = UIUtils.COLOR_CHAIR
        let name: String!
        
        if tableInfo == nil {
            if isUpcomingTab() || isReservationTab(){
                
                let order: Order = getClickedCellOrder() as! Order
                var user: User!
                
                if order.fake != nil {
                    user = RoomUtils.getFirstUpcomingOrder(order.childOrderList).user
                }else{
                    user = order.user
                }
                
                name = StringUtils.getFullNameFromUser(user)
                
            }else if isServerTab(){
                let server: Server = getClickedCellOrder() as! Server
                name = server.firstName
                color = UIColor().HexToColor(server.color)
            }else{
                let waitOrder: WaitList = getClickedCellOrder() as! WaitList
                name = StringUtils.getFullNameFromUser(waitOrder.user)
            }
        }else{
            name = tableInfo?.currentOrder.user.firstName
            clickedCellIndex = nil
        }
        
        
        draggableView.frame = CGRectMake(point.x - 120, point.y - 40, 120, 40)
        draggableView.layer.borderColor = UIColor.blackColor().CGColor
        draggableView.name = name
        draggableView.icon.backgroundColor = color
        draggableView.iconColor = UIColor.whiteColor()
        draggableView.layer.borderWidth = 2
        draggableView.tag = Tag.DRAGGABLE_VIEW_TAG
        rootView.addSubview(draggableView)
    }
    
    func rootViewToFront() {
        rootViewZPosition = rootView.layer.zPosition
        rootView.layer.zPosition = 1;
    }
    
    func rootViewToDefaultPosition() {
        rootView.layer.zPosition = rootViewZPosition
    }
    
    func findTableViewById(tag: Int) -> UIView{
        return mainLeftPartView.viewWithTag(tag)!
    }
    
    func highlightTableBorder(){
        let view = findTableViewById(lastHighlightedTableTag);
        defaultTableBorder = view.layer.borderColor
        view.layer.borderColor = UIUtils.COLOR_DEF_BLUE.CGColor
    }
    
    func disableTableBorder() {
        findTableViewById(lastHighlightedTableTag).layer.borderColor = defaultTableBorder
    }
    
    func removeDraggableView(){
        draggableView.removeFromSuperview()
    }
    
    func addStatusPicToTable(view: UIView, status: GuestStatus){
        if (view.superview!.viewWithTag(Tag.CHAIR_PIC_TAG) == nil) {
            let chairView = RoomFactory.createChairImageView(view.superview!, imageName: StatusUtils.getStatusImage(status))
            let timeView = view.superview?.viewWithTag(Tag.UPCOMING_TABLE_CONTAINER)
            view.superview?.insertSubview(chairView, belowSubview: timeView!)
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    func isServerTab() -> Bool {
        return currentTableTabTag == Tag.SERVERS_TAB
    }
    
    func isReservationTab() -> Bool {
        return currentTableTabTag == Tag.RESERVATION_TAB
    }
    
    func isUpcomingTab() -> Bool {
        return currentTableTabTag == Tag.UPCOMING_TAB
    }
    
    func isWaitListTab() -> Bool{
        return currentTableTabTag == Tag.WAITLIST_TAB
    }
    
    func isUpcomingBtnSelected() -> Bool{
        return upcomingButton.selected
    }
    
    func isSeatedBtnSelected() -> Bool{
        return seatedTimeButton.selected
    }
}
