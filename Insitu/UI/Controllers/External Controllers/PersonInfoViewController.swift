//
//  PersonInfoViewController.swift
//  Butter
//
//  Created by Grigor Avagyan on 7/31/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol PersonInfoEventListener{
    func onPersonViewClosing()
    func onPersonViewSetupFinished()
    func onGuestStatusChanged(order: Order, status: GuestStatus, isMulti: Bool)
    func onPersonFullNameClick(order: Order)
}

class PersonInfoViewController: UIViewController, GuestStatusEventListener {

    @IBOutlet weak var personFirstName: UILabel!
    
    @IBOutlet weak var personFullName: UIButton!
    @IBOutlet weak var personGuestCount: UILabel!
    @IBOutlet weak var personTableNumber: UILabel!
    
    @IBOutlet weak var guestNote: UILabel!
    @IBOutlet weak var guestTag: UILabel!
    @IBOutlet weak var reservationNote: UILabel!
    @IBOutlet weak var personReservationTime: UILabel!
    @IBOutlet weak var reservationTag: UILabel!
    @IBOutlet weak var personPhoneNumber: UILabel!
    
    
    @IBOutlet weak var personReservationStateButton: UIButton!
    @IBOutlet weak var personSeatButton: UIButton!
    
    @IBOutlet weak var personCancelReservationButton: UIButton!
    @IBOutlet weak var personCancelReservationXButton: UIButton!
    
    @IBOutlet weak var personinfoCloseButton: UIButton!

    @IBOutlet weak var personInfoSeparator1: UIView!
    @IBOutlet weak var personInfoSeparator2: UIView!
    @IBOutlet weak var personInfoSeparator3: UIView!
    
    @IBOutlet weak var personInfoParentView: UIView!
    
    var order: Order!
    var delegate: PersonInfoEventListener!
    
    @IBAction func closePersonInfo(sender: UIButton) {
//        self.view.slideInToBorder()
        delegate.onPersonViewClosing()
    }
    @IBAction func onSeatNowClick(sender: UIButton) {
        if personSeatButton.titleLabel?.text == "Done" {
            changeStatusButtonTextAndSendOrder(GuestStatus.DONE)
        } else {
            changeStatusButtonTextAndSendOrder(GuestStatus.SEATED)
        }
    }
    
    @IBAction func onFullNameClick(sender: UIButton) {
        openNewReservationView()
    }
    
    @IBAction func onCancelReservationButtonClick(sender: UIButton) {
        
        Alerts.getInstance.cancelReservation(self){ isCanceled in
            if isCanceled{
                self.changeStatusButtonTextAndSendOrder(GuestStatus.CANCELLED)
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewComponents()
        setupInfo()
    }
    
    override func viewDidAppear(animated: Bool) {
        self.view.frame.origin.x = self.view.superview!.frame.width
        return super.viewDidAppear(animated)
    }
    
    func openNewReservationView(){
        delegate.onPersonFullNameClick(order)
    }
    
    func setupViewComponents() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PersonInfoViewController.onParentViewClick(_:)))
        
        personInfoParentView.addGestureRecognizer(tapGesture)
        
        personReservationStateButton.layer.borderWidth = 2
        personReservationStateButton.layer.borderColor = UIUtils.COLOR_BUTTON_BORDER_BG.CGColor
        personReservationStateButton.setTitleColor(UIUtils.COLOR_GRAY, forState: .Highlighted)
        
        personSeatButton.layer.borderWidth = 2
        personSeatButton.layer.borderColor = UIUtils.COLOR_BUTTON_BORDER_BG.CGColor
        personSeatButton.setTitleColor(UIUtils.COLOR_GRAY, forState: .Highlighted)
        
        personCancelReservationButton.layer.borderWidth = 2
        personCancelReservationButton.layer.borderColor = UIUtils.COLOR_BUTTON_BORDER_BG.CGColor
        personCancelReservationButton.setTitleColor(UIUtils.COLOR_GRAY, forState: .Highlighted)
        
    }
    
    func onParentViewClick(gesture: UITapGestureRecognizer){
        openNewReservationView()
    }
    
    func setupInfo(){
        
        if order == nil {
            return
        }
        let guestCount = "0"
        let orderTableName = ""
        let empty = "Empty"
        let time = ""
        let phoneNumber = "Not Registered"

        let fullName = StringUtils.getFullNameFromUser(order.user)
        
        personFirstName.text = fullName
        personFullName.setTitle(fullName, forState:  UIControlState.Normal)
        personGuestCount.text = (order.guestCount != nil) ? String(order.guestCount) : guestCount
        personTableNumber.text = (order.orderedTable.name != nil) ? String(order.orderedTable.name) : orderTableName
        guestNote.text = (order.user.notes.count != 0) ? StringUtils.parseNotesToStringWithCommas(order.user.notes) : empty
        reservationNote.text = (order.notes.count != 0) ? StringUtils.parseNotesToStringWithCommas(order.notes) : empty
        guestTag.text = (order.user.codes.count != 0) ? StringUtils.parseCodesToStringWithCommas(order.user.codes) : empty
        reservationTag.text = (order.codes.count != 0) ? StringUtils.parseCodesToStringWithCommas(order.codes) : empty
        personReservationTime.text = (order.orderDateTime != nil) ? String(TimeUtils.getHourAndMinutesFromDateInString(order.orderDateTime)) : time
        personPhoneNumber.text = (order.user.phoneNumber != nil) ? String(order.user.phoneNumber) : phoneNumber
        
        setButtonText(order.status)
        
        delegate?.onPersonViewSetupFinished()
    }
    
    func onStatusChanged(status: GuestStatus) {
        if status == .CANCELLED || status == .NO_SHOW {
            Alerts.getInstance.cancelReservation(self){ isCanceled in
                if isCanceled{
                    self.changeStatusButtonTextAndSendOrder(status)
                }
            }
        } else {
            changeStatusButtonTextAndSendOrder(status)
        }
    }
    
    func changeStatusButtonTextAndSendOrder(status: GuestStatus){
        
        if order.parentId != nil {
            Alerts.getInstance.changeStatusForAllOrders(self, orders: nil){ response in
                switch response{
                case .CHANGE_SINGLE_ORDER:
                    self.setButtonText(status)
                    self.delegate.onGuestStatusChanged(self.order, status: status, isMulti: false)
                    break
                case .CHANGE_ALL_ORDERS:
                    self.setButtonText(status)
                    self.delegate.onGuestStatusChanged(self.order, status: status, isMulti: true)
                    break
                default:
                    break
                }
            }
        }else{
            self.setButtonText(status)
            self.delegate.onGuestStatusChanged(self.order, status: status, isMulti: false)
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print(segue.identifier)
        if segue.identifier == "statusPopup" {
            if let nav = segue.destinationViewController as? UINavigationController{
                if let controller = nav.topViewController as? GuestStatusTableViewController{
                    controller.delegatge = self
                    controller.status = order.status
                }
            }
        }
    }
    
    func changeSeatNowToDone(isDone: Bool) {
        
        if isDone {
            personSeatButton.setTitle("Done", forState: .Normal)
        } else {
            personSeatButton.setTitle("Seat Now", forState: .Normal)
        }
    }

    func setButtonText(status: GuestStatus) {
        personReservationStateButton.setTitle(String(status), forState: UIControlState.Normal)
        if StatusUtils.isSeatedCategory(status){
            changeSeatNowToDone(true)
        } else {
            changeSeatNowToDone(false)
        }
    }
    
}

extension UIView {
    func slideInFromBorder(duration: NSTimeInterval = 0.5, completionDelegate: AnyObject? = nil) {
        let slideInFromBorderTransition = CATransition()
        
        if let delegate: AnyObject = completionDelegate {
            slideInFromBorderTransition.delegate = delegate
        }
        
        slideInFromBorderTransition.type = kCATransitionMoveIn
        slideInFromBorderTransition.subtype = kCATransitionFromRight
        slideInFromBorderTransition.duration = duration
        slideInFromBorderTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromBorderTransition.fillMode = kCAFillModeRemoved
        
        self.layer.addAnimation(slideInFromBorderTransition, forKey: "slideInFromLeftTransition")
    }
    
    func slideInToBorder(duration: NSTimeInterval = 0.5, completionDelegate: AnyObject? = nil) {
        let slideInToBorderTransition = CATransition()
        
        if let delegate: AnyObject = completionDelegate {
            slideInToBorderTransition.delegate = delegate
        }
        
        slideInToBorderTransition.type = kCATransitionReveal
        slideInToBorderTransition.subtype = kCATransitionFromLeft
        slideInToBorderTransition.duration = duration
        slideInToBorderTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInToBorderTransition.fillMode = kCAFillModeRemoved
        
        self.layer.addAnimation(slideInToBorderTransition, forKey: "slideInFromLeftTransition")
    }
}










