//
//  Butter
//
//  Created by Grigor Avagyan on 7/31/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol InfoTableEventListener{
    func onTableTabTagChanged(tag: Int)
    //func pulseTables(value: Bool)
    func onTableItemClicked(parentIndex: Int, childIndex: Int?)
    func getCurrentTableCellByIndex(index: Int) -> AnyObject
    func getCurrentTableItemsCount() -> Int
    func dragPopup(gest: UILongPressGestureRecognizer, point: CGPoint)
    func deleteSelectedServer(index:Int)
    func onServerButtonClick(button: UIButton)
    func getSortedData(sortType: SortType)
    func filterByName(name: [String]?)
    func onGuestStatusChanged(order: Order, status: GuestStatus, isMulti: Bool)
}

class InfoTableViewController:  UIViewController, UITableViewDataSource, UITableViewDelegate,
                                UIGestureRecognizerDelegate, UIPopoverPresentationControllerDelegate,
                                UIActionSheetDelegate, UISearchBarDelegate, OnTableItemPressEventListener,
                                GuestStatusEventListener{
    
    var delegate: InfoTableEventListener!
    
    @IBOutlet var upcomingTabButton: UIButton!
    @IBOutlet var reservationTabButton: UIButton!
    @IBOutlet var serversTabButton: UIButton!
    @IBOutlet weak var waitListTabButton: UIButton!
    
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var searchTab: UISearchBar!
    @IBOutlet var contentTableView: UITableView!
    
    @IBOutlet weak var reservationTitleView: UIView!
    @IBOutlet weak var serverTitleView: UIView!
    @IBOutlet weak var waitListTitleView: UIView!
    
    @IBOutlet weak var filterButton: UIButton!
    
    var cellLongGest: UILongPressGestureRecognizer!
    let serverPlusButton = UIButton(type: .Custom)
    var selectedIndex = -1
    var parentIndex = -1
    
    
    override func viewDidLoad() {
        setupInfoTable()
        setupSearchView()
        reloadInfoTable()
    }
    
    @IBAction func onTabButtonClick(sender: UIButton) {
        
        unselectAllTabs()
        hideAllTitles()
        
        sender.backgroundColor = UIUtils.COLOR_GRAY
        
        if sender == serversTabButton {
            serverTitleView.hidden = false
        } else if sender == waitListTabButton{
            waitListTitleView.hidden = false
        }else{
            reservationTitleView.hidden = false
        }
        
        unselectGroupedRow()
        delegate!.onTableTabTagChanged(sender.tag)
        
        reloadInfoTable()
    }
    
    @IBAction func onFilterButtonClick(sender: UIButton) {
        Alerts.getInstance.filterBy(self, sourceView: filterButton){ sortType in
            self.delegate.getSortedData(sortType)
        }
    }
    
    func unselectGroupedRow(){
        selectedIndex = -1
    }
    
    func unselectAllTabs(){
        upcomingTabButton.backgroundColor = UIColor.whiteColor()
        reservationTabButton.backgroundColor = UIColor.whiteColor()
        serversTabButton.backgroundColor = UIColor.whiteColor()
        waitListTabButton.backgroundColor = UIColor.whiteColor()
    }
    
    func hideAllTitles() {
        serverTitleView.hidden = true
        waitListTitleView.hidden = true
        reservationTitleView.hidden = true
    }
    
    func setupSearchView(){
        searchTab.layer.borderColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1).CGColor
        searchTab.layer.borderWidth = 1
        searchTab.delegate = self
    }
    
    func onServerPlusButtonClick() {
        delegate.onServerButtonClick(serverPlusButton)
    }
    
    func changSearchTabSizeAddPlusButton(makeSmall: Bool){
        
        if makeSmall {
            changeSearchTabWidthToFull(false)
            searchContainer.addSubview(serverPlusButton)
        } else {
            changeSearchTabWidthToFull(true)
            serverPlusButton.removeFromSuperview()
        }
    }
    
    func onStatusChanged(status: GuestStatus) {
        let order = delegate.getCurrentTableCellByIndex(parentIndex) as! Order
        delegate.onGuestStatusChanged(order, status: status, isMulti: true)
    }
    
    func showStatusPopup(sender: UIButton, parentIndex: Int) {
        self.parentIndex = parentIndex
        
        let controller = RoomUtils.getStatusPopup(sender)
        controller.delegatge = self
        self.presentViewController(controller, animated: true, completion: nil)
    }

    func onTableItemClicked(parentIndex: Int, childIndex: Int?) {
        delegate.onTableItemClicked(parentIndex, childIndex: childIndex)
    }
    
    func changeSearchTabWidthToFull(fullSize: Bool){
        var newFrame = searchTab.frame
        
        if fullSize {
            newFrame.size.width = searchContainer.frame.width
        }else{
            newFrame.size.width = searchContainer.frame.width - 44
        }
        newFrame.size.height = searchContainer.frame.height
        
        searchTab.frame = newFrame
    }
    
    func reloadInfoTable(){
        self.contentTableView.reloadData()
    }
    
    func setupInfoTable() {
        cellLongGest = UILongPressGestureRecognizer(target: self, action: #selector(InfoTableViewController.onTableViewCellLongPress(_:)))
        cellLongGest.minimumPressDuration = 0.15
        cellLongGest.delaysTouchesBegan = true
        cellLongGest.delegate = self
        self.contentTableView.addGestureRecognizer(cellLongGest)
        self.contentTableView.delegate = self
        self.contentTableView.dataSource = self
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let object = delegate?.getCurrentTableCellByIndex(indexPath.row)
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        if object is Order{
            let order = object as! Order
            if order.fake != nil {
                handleGroupedSelection(indexPath)
                return
            }
        }
        
        delegate?.onTableItemClicked(indexPath.row, childIndex: nil)
    }
    
    func handleGroupedSelection(indexPath: NSIndexPath){
        let cell = contentTableView.cellForRowAtIndexPath(indexPath) as! GroupedOrderViewCell
        
        let isSelected = cell.selected
        if isSelected && selectedIndex != indexPath.row {
            selectedIndex = indexPath.row
        }else{
            unselectGroupedRow()
        }
        updateTableRowHeight()
    }
    
    func updateTableRowHeight(){
        contentTableView.beginUpdates()
        contentTableView.endUpdates()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cellheight: CGFloat = 35
        
        if indexPath.row == selectedIndex {
            let order = delegate.getCurrentTableCellByIndex(indexPath.row) as! Order
            if order.fake != nil {
                let childCount = order.childOrderList.count
                return (CGFloat(childCount + 1) * cellheight)
            }
        }
        
        return cellheight
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        let object = delegate?.getCurrentTableCellByIndex(indexPath.row)
        if (object as? Server) != nil{
            if cell!.selected {
                //tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
               // delegate.pulseTables(false)
            }else{
               // delegate.pulseTables(true)
            }
        }
    }
    
    func onTableViewSectionRowLongPress(gest: UILongPressGestureRecognizer, parentIndex: Int) {
        selectedIndex = parentIndex
        getPointAndDragPopup(gest)
    }

    func onTableViewCellLongPress(gest: UILongPressGestureRecognizer) {
        getPointAndDragPopup(gest)
    }
    
    func getPointAndDragPopup(gest: UILongPressGestureRecognizer){
        let point: CGPoint = gest.locationInView(self.parentViewController?.view)
        delegate.dragPopup(gest, point: point)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (delegate?.getCurrentTableItemsCount())!
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let object = delegate!.getCurrentTableCellByIndex(indexPath.row)
        
        switch object {
        case is Server:
            let server: Server = object as! Server
            return RoomUtils.setupServerCell(tableView, server: server)
        case is Order:
            let order: Order = object as! Order
            if order.fake != nil{
                let cell = RoomUtils.setupGroupedCell(tableView, index: indexPath.row, order: order)
                cell.delegate = self
                return cell
            }
            return RoomUtils.setupReservationCell(tableView, order: order)
        case is WaitList:
            let waitList: WaitList = object as! WaitList
            return RoomUtils.setupWaitlistCell(tableView, waitList: waitList)
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.whiteColor()
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            let separatedFullNameArr = searchText.characters.split(" ").map(String.init)
            if separatedFullNameArr.count == 0{
                delegate.filterByName([searchText])
            }else{
                let firstTwoWords = Array(separatedFullNameArr.prefix(2))
                delegate.filterByName(firstTwoWords)
            }
        }else{
            delegate.filterByName(nil)
        }
        reloadInfoTable()
    }
}




