//
//  Alerts.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/22/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

public class Alerts{
    
    class var getInstance: Alerts {
        struct Static {
            static let instance = Alerts()
        }
        return Static.instance
    }
    
    func filterBy(viewController: UIViewController, sourceView: UIView, callBack:(SortType) -> ()){
        let alertController = UIAlertController(title: "Choose your action.", message: "Sort by", preferredStyle: .ActionSheet)
        let actionDate = UIAlertAction(title: "Time", style: .Default){
            UIAlertAction in
            callBack(SortType.TIME)
        }
        let actionName = UIAlertAction(title: "Name", style: .Default){
            UIAlertAction in
            callBack(SortType.NAME)
        }
        
        alertController.addAction(actionDate)
        alertController.addAction(actionName)
        
        alertController.popoverPresentationController?.sourceView = sourceView
        alertController.popoverPresentationController?.sourceRect = sourceView.bounds
        
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func guestsFitTables(viewController: UIViewController) {
        let alertController = UIAlertController(title: "Info!", message: "Guests already fit tables.", preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "OK", style: .Default){
            UIAlertAction in
        }
        
        alertController.addAction(actionOk)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func cancelReservation(viewController: UIViewController, callBack: (Bool)-> ()) {
        let alertController = UIAlertController(title: "Choose your action", message: "Are you sure you want to cancel?", preferredStyle: .Alert)
        let actionYes = UIAlertAction(title: "YES",style: .Default){
            UIAlertAction in
            callBack(true)
        }
        let actionCancel = UIAlertAction(title: "NO", style: .Cancel){
            UIAlertAction in
            callBack(false)
        }
        
        alertController.addAction(actionYes)
        alertController.addAction(actionCancel)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showAlertToSeat(viewController: UIViewController, callBack: (AlertActions)-> ()) {
        let alertController = UIAlertController(title: "Choose your action", message: "Please select your action.", preferredStyle: .Alert)
        let actionChangeAndSeat = UIAlertAction(title: "SEAT",style: .Default){
            UIAlertAction in
            callBack(AlertActions.SEAT)
        }
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        alertController.addAction(actionChangeAndSeat)
        alertController.addAction(actionCancel)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func pleaseEnterRequiredFields(viewController: UIViewController, fields: [String]){
        
        let message = fields.joinWithSeparator(", ") + " required"
        let alertController = UIAlertController(title: "Info!", message: message, preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "OK", style: .Default){
            UIAlertAction in
        }
        
        alertController.addAction(actionOk)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func tableIsTooSmallForGuestCount(viewController: UIViewController, callBack: (AlertActions)->()){
        let alertController = UIAlertController(title: "No enought space for guests.", message: "Please select your action.", preferredStyle: .Alert)
        let actionCancel = UIAlertAction(title: "Cancel", style: .Default){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        let actionBookAnyway = UIAlertAction(title: "Book Anyway", style: .Default){
            UIAlertAction in
            callBack(AlertActions.MARK_PREVIOUS_DONE_AND_SEAT)
        }

        let actionMultyBook = UIAlertAction(title: "Multy-Book", style: .Default){
            UIAlertAction in
            callBack(AlertActions.MULTY_BOOK)
        }

        
        alertController.addAction(actionCancel)
        alertController.addAction(actionBookAnyway)
        alertController.addAction(actionMultyBook)
        
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func someOfTablesAreNotChanged(viewController: UIViewController, orders: [Order], callBack: (AlertActions) -> ()){
        
        let tableList = orders.map{$0.orderedTable.name}.joinWithSeparator(", ")
        let message = tableList + " tables are not changed"
        
        let alertController = UIAlertController(title: "Choose your action", message: message, preferredStyle: .Alert)
        
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        let actionContinue = UIAlertAction(title: "CONTINUE",style: .Default){
            UIAlertAction in
            callBack(AlertActions.CONTINUE)
        }
        
        alertController.addAction(actionCancel)
        alertController.addAction(actionContinue)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func occupiedTablesAction(viewController: UIViewController, orders: [Order], callBack: (AlertActions) -> ()){
        
        let tableList = orders.map{$0.orderedTable.name}.joinWithSeparator(", ")
        let isSingleOrder = orders.count == 1

        let title = isSingleOrder ? "Table Occupied!" : "Tables Occupied!"
        let message = isSingleOrder ? "Table " + tableList + " is occipied." : "Tables " + tableList + " are occipied."
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        let action1 = UIAlertAction(title: "ONLY SEAT AVAILABLE TABLES",style: .Default){
            UIAlertAction in
            callBack(AlertActions.SEAT_ON_AVAILABLE_TABLES)
        }
        
        let action2 = UIAlertAction(title: "MOVE TO NEW TABLES", style: .Default){
            UIAlertAction in
            callBack(AlertActions.CHANGE_ORDER_TABLES)
        }
        
        let action3 = UIAlertAction(title: "MARK PREVIOUS DONE", style: .Default){
            UIAlertAction in
            callBack(AlertActions.MARK_PREVIOUS_DONE_AND_SEAT)
        }
        
        
        alertController.addAction(actionCancel)
        
        if !isSingleOrder {
            alertController.addAction(action1)
        }
        
        
        alertController.addAction(action2)
        alertController.addAction(action3)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func changeStatusForAllOrders(viewController: UIViewController, orders: [Order]?, callBack: (AlertActions) -> ()){
        
        let title = "Deal with Multi-Book!"
        let message = "This reservation is booked on multiple tables. Do you want to update the status for:"
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        let actionChangeSingleOrder = UIAlertAction(title: "THIS TABLE ONLY",style: .Default){
            UIAlertAction in
            callBack(AlertActions.CHANGE_SINGLE_ORDER)
        }
        
        let actionChangeAllOrders = UIAlertAction(title:  "ALL TABLES", style: .Default){
            UIAlertAction in
            callBack(AlertActions.CHANGE_ALL_ORDERS)
        }
        
        
        alertController.addAction(actionCancel)
        alertController.addAction(actionChangeSingleOrder)
        alertController.addAction(actionChangeAllOrders)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func tableIsNotFromGroupedOrder(viewController: UIViewController, tableName: String, callBack: (AlertActions) -> ()){
        
        let message = "Sorry, this party is not booked at Table " + tableName
        let title = "Deal with Multi-Book!"
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let actionChangeAndSeat = UIAlertAction(title: "CHANGE TABLES",style: .Default){
            UIAlertAction in
            callBack(AlertActions.CHANGE_ORDER_TABLES)
        }
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL) }
        
        alertController.addAction(actionChangeAndSeat)
        alertController.addAction(actionCancel)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showExistingSeatedTables(viewController: UIViewController, tableNames: String, callBack: (AlertActions) -> ()){
        
        let message = "There are guests at table " + tableNames
        
        let alertController = UIAlertController(title: "Choose your action", message: message, preferredStyle: .Alert)
        let actionChangeAndSeat = UIAlertAction(title: "MARK DONE",style: .Default){
            UIAlertAction in
            callBack(AlertActions.MAKE_ALL_PREVIOUS_ORDERS_DONE)
        }
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL) }
        
        alertController.addAction(actionChangeAndSeat)
        alertController.addAction(actionCancel)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showAlertThatTablesAreTheSame(viewController: UIViewController, callBack: (AlertActions)->()){
        let alertController = UIAlertController(title: "Congratulations", message: "You are already there", preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "OK", style: .Default){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        alertController.addAction(actionOk)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showAlertToSetPreviousDoneAndSeatGuest(viewController: UIViewController, callBack: (AlertActions)-> ()) {
        let alertController = UIAlertController(title: "Choose your action", message: "Table is occupied at this moment.", preferredStyle: .Alert)
        let actionChangeAndSeat = UIAlertAction(title: "MARK PREVIOUS DONE AND SEAT",style: .Default){
            UIAlertAction in
            callBack(AlertActions.MARK_PREVIOUS_DONE_AND_SEAT)
        }
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        alertController.addAction(actionChangeAndSeat)
        alertController.addAction(actionCancel)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showAlertToChangeTable(viewController: UIViewController, callBack: (AlertActions)-> ()) {
        let alertController = UIAlertController(title: "Choose your action", message: "Please select your action.", preferredStyle: .Alert)
        let actionChangeAndSeat = UIAlertAction(title: "CHANGE TABLE",style: .Default){
            UIAlertAction in
            callBack(AlertActions.CHANGE_TABLE)
        }
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        alertController.addAction(actionChangeAndSeat)
        alertController.addAction(actionCancel)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showAlertToChangeTableAndSeatGuest(viewController: UIViewController, callBack: (AlertActions)-> ()) {
        let alertController = UIAlertController(title: "Choose your action", message: "Please select your action.", preferredStyle: .Alert)
        let actionChangeAndSeat = UIAlertAction(title: "CHANGE TABLE AND SEAT",style: .Default){
            UIAlertAction in
            callBack(AlertActions.CHANGE_TABLE_AND_SEAT)
        }
        let actionChange = UIAlertAction(title: "CHANGE TABLE",style: .Default){
            UIAlertAction in
            callBack(AlertActions.CHANGE_TABLE)
        }
        let actionCancel = UIAlertAction(title: "CANCEL", style: .Cancel){
            UIAlertAction in
            callBack(AlertActions.CANCEL)
        }
        
        alertController.addAction(actionChangeAndSeat)
        alertController.addAction(actionChange)
        alertController.addAction(actionCancel)
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
}