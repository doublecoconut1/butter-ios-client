//
//  DottedView.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/1/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class DottedView: UIView {
    
    let shapelayer = CAShapeLayer()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init() {
        super.init(frame: CGRectZero)
        setup()
    }
    
    func setup() {
        
        let size: CGFloat = self.bounds.height
        
        
        let path = UIBezierPath()
        path.moveToPoint(CGPoint(x: 0, y: 0))
        path.addLineToPoint(CGPoint(x: self.bounds.width, y: 0))
        
        shapelayer.strokeColor = UIUtils.COLOR_BUTTON_BORDER_BG.CGColor
        shapelayer.lineWidth = size
        shapelayer.lineJoin = kCALineJoinMiter
        shapelayer.lineDashPattern = [size, size]
        shapelayer.path = path.CGPath
        layer.addSublayer(shapelayer)
    }
}
