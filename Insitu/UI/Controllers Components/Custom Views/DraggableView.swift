//
//
//  Created by Grigor Avagyan on 7/25/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class DraggableView: UIView{

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    var name: String? {
        get {
            return nameLabel?.text
        }
        set {
            nameLabel.text = newValue
        }
    }
    
    var iconColor: UIColor? {
        get {
            return icon.tintColor
        }
        set {
            icon.image = (icon.image!.imageWithRenderingMode(.AlwaysTemplate))
            icon.tintColor = newValue
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    func initSubviews() {
        // standard initialization logic
        let nib = UINib(nibName: "DraggableView", bundle: nil)
        nib.instantiateWithOwner(self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }

}
