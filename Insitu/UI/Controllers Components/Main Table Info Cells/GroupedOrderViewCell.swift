//
//  GroupedOrderViewCell.swift
//  Butter
//
//  Created by Grigor Avagyan on 10/26/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol OnTableItemPressEventListener {
    func onTableItemClicked(parentIndex: Int, childIndex: Int?)
    func onTableViewSectionRowLongPress(gest: UILongPressGestureRecognizer, parentIndex: Int)
    func showStatusPopup(sender: UIButton, parentIndex: Int)
}

class GroupedOrderViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var guestCountLabel: UILabel!
    @IBOutlet weak var groupedTableView: UITableView!
    @IBOutlet weak var tableNumberLabel: UILabel!
    @IBOutlet weak var statusIcon: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var gropedCellContentView: UIView!
    @IBOutlet weak var vipImageView: UIImageView!
    @IBOutlet weak var groupedIcon: UIImageView!
    
    var orders: [Order]!
    var parentCellIndex: Int!
    var delegate: OnTableItemPressEventListener!
    var cellLongPress: UILongPressGestureRecognizer!
    var cellIsOpened = false
    
    @IBAction func onStatusButtonClick(sender: UIButton) {
        delegate.showStatusPopup(sender, parentIndex: parentCellIndex)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupTable()
        rotateIcon()
    }
    
    override func prepareForReuse() {
        groupedTableView.reloadData()
    }
    
    func rotateIcon(){
        
        let degree = (self.frame.height > 35) ? 90 : 0
        groupedIcon.rotateByDegree(degree, animDuration: 0.20)
    }
    
    func setupTable(){
        
        groupedTableView.delegate = self
        groupedTableView.dataSource = self
        groupedTableView.scrollEnabled = false
        
        cellLongPress = UILongPressGestureRecognizer(target: self, action: #selector(self.onTableViewCellLongPress(_:)))
        cellLongPress.minimumPressDuration = 0.15
        cellLongPress.delaysTouchesBegan = true
        cellLongPress.delegate = self
        
        groupedTableView.addGestureRecognizer(cellLongPress)
    }
    
    func onTableViewCellLongPress(gest: UILongPressGestureRecognizer){
        delegate.onTableViewSectionRowLongPress(gest, parentIndex: parentCellIndex)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        delegate.onTableItemClicked(parentCellIndex, childIndex: indexPath.row)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return RoomUtils.setupReservationCell(tableView, order: orders[indexPath.row])
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
}
