//
//  ServersViewCell.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/23/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

class ServersViewCell: UITableViewCell {
    
    @IBOutlet weak var serverIcon: UIImageView!
    @IBOutlet weak var serverName: UILabel!
    @IBOutlet weak var coversCurr: UILabel!
    @IBOutlet weak var coversTotal: UILabel!
    
}