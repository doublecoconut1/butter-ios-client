//
//  ChangeTableViewCell.swift
//  Butter
//
//  Created by Grigor Avagyan on 10/24/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class ChangeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var fromTableLable: UILabel!
    @IBOutlet weak var toTableLabel: UILabel!
    @IBOutlet weak var toTableView: UIView!
}
