//
//  Insitu
//
//  Created by Grigor Avagyan on 7/17/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class ReservationViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var guestCountLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var tabelNumberLabel: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var VIPImageView: UIImageView!
    
}
