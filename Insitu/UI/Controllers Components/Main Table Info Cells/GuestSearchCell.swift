//
//  GuestSearchCell.swift
//  Butter
//
//  Created by Harut on 8/24/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class GuestSearchCell: UITableViewCell {

    @IBOutlet weak var guestFullNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
}
