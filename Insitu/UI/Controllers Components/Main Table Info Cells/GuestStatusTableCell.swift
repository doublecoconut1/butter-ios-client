//
//  GuestStatusTableCell.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/3/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

class GuestStatusTableCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var icon: UIImageView!
}
