//
//  GuestStatusTableViewController.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/2/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import UIKit

protocol GuestStatusEventListener {
    func onStatusChanged(status: GuestStatus)
}

class GuestStatusTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    internal var status: GuestStatus!
    internal var delegatge: GuestStatusEventListener!
    internal var lastCheckedCell: UITableViewCell?
    internal var headers = ["Special", "Expected", "In-House", "Seated"]
    
    @IBOutlet weak var statusTableView: UITableView!
    
    @IBAction func onCancelButtonClick(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(false, completion: nil)
    }
    
    override func viewDidLoad() {
        self.title = "Update Status"
        
        statusTableView.dataSource = self
        statusTableView.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        let indexPath = StatusUtils.getIndexPath(status)
        print(String(indexPath?.section) + " / " + String(indexPath?.row))
        if indexPath != nil {
            self.statusTableView.scrollToRowAtIndexPath(indexPath!, atScrollPosition: UITableViewScrollPosition.Middle, animated: false)
        }
        
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headers[section]
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return StatusUtils.allSection.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StatusUtils.allSection[section].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let currStatus = StatusUtils.allSection[indexPath.section][indexPath.row]
        let name = String(currStatus)
        
        let cell = tableView.dequeueReusableCellWithIdentifier("guestStatusCell") as! GuestStatusTableCell
        cell.name.text = name
        cell.icon.image = UIImage(named: StatusUtils.getStatusImage(currStatus))
        cell.accessoryType = .None
        
        if name == String(status){
            cell.tintColor = UIUtils.COLOR_GREEN
            cell.accessoryType = .Checkmark
            lastCheckedCell = cell
        }

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var label: UILabel!
        let cell = tableView.cellForRowAtIndexPath(indexPath)!
        
        cell.tintColor = UIUtils.COLOR_GREEN
        
        if (lastCheckedCell != nil){
            lastCheckedCell?.accessoryType = .None
        }
        
        tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .Checkmark
        
        label = cell.viewWithTag(Tag.GUEST_STATUS_TABLE_CELL_LABEL) as! UILabel
        status = GuestStatus(rawValue: label.text!)!
        dismissViewControllerAnimated(false, completion: nil)
        delegatge.onStatusChanged(status)
    }
}
