//
//  RoomTable.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/25/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

public class TableInfo{
    
    public static let X_MIN = "xMin"
    public static let Y_MIN = "yMin"
    public static let X_MAX = "xMax"
    public static let Y_MAX = "yMax"
    
    var id : Int!
    var table: Tables!
    var position : Dictionary<String, CGFloat>!
    var assignment: Assignments!
    var upcomingList: [Order]!
    var currentOrder : Order!
    var isAvailable = true
    var selected = false
}