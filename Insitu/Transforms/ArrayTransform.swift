//
//  ArrayTransform.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/21/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class ArrayTransform<T:BaseData where T:Mappable> : TransformType {
    typealias Object = Array<T>
    typealias JSON = String
    
    func transformFromJSON(value: AnyObject?) -> Array<T>? {
        var arr = Array<T>()
        if value != nil {
            let parsedArr = Mapper<T>().mapArray(value as! String)
            if parsedArr != nil {
                return parsedArr
            }else if (value as? String != nil && !((value as? String)?.isEmpty)!){
                if Code() is T {
                    let code = Code()
                    code.name = value as! String
                    arr.append(code as! T)
                }else{
                    let code = Note()
                    code.name = value as! String
                    arr.append(code as! T)
                }
            }
        }
        return arr
        
    }
    
    func transformToJSON(value: Object?) -> JSON? {
        if value != nil {
            return value?.description
        }
        return Array<AnyObject>().description
    }
    
}