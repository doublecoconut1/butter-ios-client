//
//  BaseData.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/18/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

public class BaseData : Mappable{
    
    init(){
    }
    
    required public convenience init?(_ map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
    }
}