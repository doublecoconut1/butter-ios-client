//
//  SendDataProtocol.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/14/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

public protocol SendDataDelegate: class {
    func onComplete(sender: String, object: AnyObject)
}