
import Foundation
import ObjectMapper

class CurrentDateDataResponse : BaseData, CustomStringConvertible{
    
    var floor : Floor!
    var date : Date!
    var orders : [Order]!
    var orderMap: AnyObject!
    var servers : [Server]!
    var serverAssignments: [Assignments]!
    var waitlist: [WaitList]!
    
    override func mapping(map:Map){
        floor <- map["floor"]
        date <- map["date"]
        orders <- map["orders"]
        orderMap <- map["orderMap"]
        servers <- map["servers"]
        serverAssignments <- map["serverAssignments"]
        waitlist <- map["waitList"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}