//
//  Floors.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/23/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class FloorInfo: BaseData{

    var height : Float!
    var id : Int!
    var name : String!
    var number : Int!
    var width : Float!
    
    override func mapping(map: Map) {
        height <- map["height"]
        id <- map["id"]
        name <- map["name"]
        number <- map["number"]
        width <- map["width"]
    }
    
}