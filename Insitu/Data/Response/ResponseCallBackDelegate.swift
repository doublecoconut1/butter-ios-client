//
//  ResponseCallBck.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/18/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation

protocol ResponseCallBackDelegate {
    associatedtype T
    func MapFromSource(_: T) -> T
}