
import Foundation
import ObjectMapper

public class Type : BaseData{
	public var id : Int!
	public var type : String!
	public var maxSeats : Int!
	public var minSeats : Int!
	public var expectedTimeMinutes : Int!

    public override func mapping(map:Map){
        id <- map["id"]
        type <- map["type"]
        maxSeats <- map["maxSeats"]
        minSeats <- map["minSeats"]
        expectedTimeMinutes <- map["expectedTimeMinutes"]
    }
}