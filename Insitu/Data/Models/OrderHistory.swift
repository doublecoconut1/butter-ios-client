//
//  OrderHistory.swift
//  Butter
//
//  Created by Harut on 8/31/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class OrderHistory: BaseData {
    
    var upcoming = [Order]()
    var past = [Order]()

    
    override func mapping(map:Map){
        upcoming <- map["upcoming"]
        past <- map["past"]
        
    }

}