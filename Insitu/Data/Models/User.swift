
import Foundation
import ObjectMapper

class User : BaseData, CustomStringConvertible{
	var id : Int!
	var email = ""
    var phoneType: String!
    var phoneNumber: String!
	var firstName = ""
	var lastName = ""
	var fullName : String!
	var subscriber : String!
	var codes = [Code]()
	var notes = [Note]()
    
    override func mapping(map:Map){
        id <- map["id"]
        email <- map["email"]
        phoneType <- map["phoneType"]
        phoneNumber <- map["phoneNumber"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        fullName <- map["fullName"]
        subscriber <- map["subscriber"]
        codes <- (map["codes"], ArrayTransform<Code>())
        notes <- (map["notes"], ArrayTransform<Note>())
    }
    
    var description: String{
        return self.toJSONString()!
    }
}