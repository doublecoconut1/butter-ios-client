//
//  WaitList.swift
//  Butter
//
//  Created by Grigor Avagyan on 9/23/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class WaitList : BaseData, CustomStringConvertible{
    
    var id: Int!
    var orderDateTime: String!
    var guestCount: Int!
    var user: User!
    var orderedTable = Tables()
    var codes: [Code]!
    var notes: [Note]!
    var dataType: String!
    
    override func mapping(map: Map) {
        id <- map["id"]
        orderDateTime <- map["orderDateTime"]
        guestCount <- map["guestCount"]
        user <- map["user"]
        orderedTable <- map["orderedTable"]
        codes <- (map["codes"], ArrayTransform<Code>())
        notes <- (map["notes"], ArrayTransform<Note>())
        dataType <- map["dataType"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}