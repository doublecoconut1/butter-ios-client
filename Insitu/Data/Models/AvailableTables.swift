//
//  AvailableTables.swift
//  Butter
//
//  Created by Harut on 9/6/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class AvailableTables: BaseData, CustomStringConvertible {
    
    var available: [Tables]!
    var walkIn: [Tables]!
    var seated: [Tables]!
    
    override func mapping(map: Map) {
        available <- map["available"]
        walkIn <- map["walkIn"]
        seated <- map["seated"]
    }
    
    var description: String {
        return self.toJSONString()!
    }
    
}