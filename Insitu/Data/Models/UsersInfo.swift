//
//  UsersInfo.swift
//  Butter
//
//  Created by Grigor Avagyan on 9/26/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class UsersInfo : BaseData, CustomStringConvertible{
    
    var count: Int!
    var page: Int!
    var pageCount: Int!
    var resultCount: Int!
    var result: [User]!
    
    override func mapping(map: Map) {
        count <- map["count"]
        page <- map["page"]
        pageCount <- map["pageCount"]
        resultCount <- map["resultCount"]
        result <- map["result"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}