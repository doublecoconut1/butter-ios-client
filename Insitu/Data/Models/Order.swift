
import Foundation
import ObjectMapper

public enum GuestStatus : String{
    //Special
    case CANCELLED = "CANCELLED"
    case NO_SHOW = "NO_SHOW"
    //Expected
    case NOT_CONFIRMED = "NOT_CONFIRMED"
    case CONFIRMED = "CONFIRMED"
    case LEFT_MESSAGE = "LEFT_MESSAGE"
    case UNKNOWN = "UNKNOWN"
    case WRONG_NUMBER = "WRONG_NUMBER"
    case LATE = "LATE"
    //In-House
    case PARTIALLY_ARRIVED = "PARTIALLY_ARRIVED"
    case ALL_ARRIVED = "ALL_ARRIVED"
    case SEND_TEXT = "SEND_TEXT"
    //Seated
    case PARTIALLY_SEATED = "PARTIALLY_SEATED"
    case SEATED = "SEATED"
    case ENTREE = "ENTREE"
    case CLEARED = "CLEARED"
    case DESERT = "DESERT"
    case VALET = "VALET"
    case CHECK_DROPPED = "CHECK_DROPPED"
    case PAID = "PAID"
    case BUS_TABLE = "BUS_TABLE"
    case DONE = "DONE"
    //
    case UPCOMING = "UPCOMING"
    case HOLD_ON = "HOLD_ON"
    case WAITING_TO_CONFIRMATION = "WAITING_TO_CONFIRMATION"
}

class Order : BaseData, CustomStringConvertible{
    
	var id : Int!
	var orderDateTime : String!
    var dateTimeCalculated : Int!
	var holdOnDateTime : String!
    var seatedDateTime: String!
	var guestCount : Int!
	var status : GuestStatus!
	var durationMinutes : Int!
    var user : User!
	var orderedTable : Tables!
	var codes = [Code]()
	var notes = [Note]()
	var notificationEmail : String!
	var notificationSMS : String!
	var notificationsSMSOnDay : String!
    var multi : Bool!
    var selected : Bool!
    var dataType: String!
    var statusChangedDate: String!
    var parentId: Int!
    var waitList: WaitList!
    var fake: Bool!
    var childOrderList: [Order]!
    
    override init() {
        super.init()
    }

    override func mapping(map:Map){
        
        id                      <- map["id"]
        orderDateTime           <- map["orderDateTime"]
        seatedDateTime          <- map["seatedDateTime"]
        dateTimeCalculated      <- map["dateTimeCalculator"]
        holdOnDateTime          <- map["holdOnDateTime"]
        guestCount              <- map["guestCount"]
        status                  <- map["status"]
        durationMinutes         <- map["durationMinutes"]
        user                    <- map["user"]
        orderedTable            <- map["orderedTable"]
        codes                   <- (map["codes"], ArrayTransform<Code>())
        notes                   <- (map["notes"], ArrayTransform<Note>())
        notificationEmail       <- map["notificationEmail"]
        notificationSMS         <- map["notificationSMS"]
        notificationsSMSOnDay   <- map["notificationsSMSOnDay"]
        multi                   <- map["multi"]
        selected                <- map["selected"]
        dataType                <- map["dataType"]
        statusChangedDate       <- map["statusChangedDate"]
        parentId                <- map["parentId"]
        waitList                <- map["waitList"]
        fake                    <- map["fake"]
        childOrderList          <- map["childOrderList"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}




