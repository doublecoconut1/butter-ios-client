//
//  Table.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/20/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class Table : BaseData{
    
    var id : Int!
    var name : String!
    var number : Int!
    
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        number <- map["number"]
    }

}