
import Foundation
import ObjectMapper

public class Floor : BaseData{
    
	public var id : Int!
	public var name : String!
    public var number : Int!
    public var width : Int!
	public var height : Int!
	public var config : Config!
	public var tables : Array<Tables>!
    public var tablesMap : [String : Tables] = [:]

    
    override public func mapping(map: Map){
        id <- map["id"]
        name <- map["name"]
        number <- map["number"]
        width <- map["width"]
        height <- map["height"]
        config <- map["config"]
        tables <- map["tables"]
        tablesMap <- map["tablesMap"]
    }

}