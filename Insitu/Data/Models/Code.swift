//
//  Code.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/21/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class Code : BaseData, CustomStringConvertible{
    
    var name: String!
    
    override init() {
        super.init()
    }
    
    override func mapping(map: Map) {
        name <- map["name"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}