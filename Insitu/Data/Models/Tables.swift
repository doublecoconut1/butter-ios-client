
import Foundation
import ObjectMapper

public class Tables : BaseData{
	public var id : Int!
	public var name : String!
	public var number : Int!
	public var type : Type!
	public var config : Config!
	public var index : Int!
    
    public override func mapping(map:Map){
        id <- map["id"]
        name <- map["name"]
        number <- map["number"]
        type <- map["type"]
        config <- map["config"]
        index <- map["index"]
    }

}