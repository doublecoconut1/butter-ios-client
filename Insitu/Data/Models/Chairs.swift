
import Foundation
import ObjectMapper

public class Chairs : BaseData{
    
	public var shape : String!
	public var width : String!
	public var top : String!
	public var left : String!
	public var widthToHeightRatio : CGFloat!

    
    public override func mapping(map: Map) {
        super.mapping(map)
        
        shape <- map["shape"]
        width <- map["width"]
        top   <- map["top"]
        left  <- map["left"]
        widthToHeightRatio  <- map["widthToHeightRatio"]
    }
}