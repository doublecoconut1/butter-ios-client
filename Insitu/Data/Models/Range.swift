//
//  Range.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/26/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class Range: BaseData, CustomStringConvertible{
    
    var available : Bool!
    var dateTime : String!
    var value : String!
    
    override func mapping(map: Map) {
        available <- map["available"]
        dateTime <- map["dateTime"]
        value <- map["value"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}