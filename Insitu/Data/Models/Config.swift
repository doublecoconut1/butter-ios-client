
import Foundation
import ObjectMapper

public enum ShapeType: String{
    case RECTANGLE = "rectangle"
    case ROUND = "round"
    case SQUARE = "square"
}

public class Config : BaseData{
	public var shape : ShapeType!
	public var widthToHeightRatio : CGFloat!
	public var width : String!
	public var chairs : Array<Chairs>!
	public var rotation : String!
	public var top : String!
	public var left : String!
	public var visibleOnFloor : String!
    
    override public func mapping(map: Map) {
        super.mapping(map)
        
        shape <- map["shape"]
        widthToHeightRatio  <- map["widthToHeightRatio"]
        width <- map["width"]
        chairs <- map["chairs"]
        rotation <- map["rotation"]
        top   <- map["top"]
        left  <- map["left"]
        visibleOnFloor  <- map["visibleOnFloor"]
    }

}