//
//  ReservationMonthRange.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/26/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class ReservationMonthRange: BaseData, CustomStringConvertible{
    
    var available : Bool!
    var date : String!
    var ranges : [Range]!
    
    override func mapping(map: Map) {
        available <- map["available"]
        date <- map["date"]
        ranges <- map["ranges"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}