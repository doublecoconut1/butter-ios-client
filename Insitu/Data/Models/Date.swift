
import Foundation
import ObjectMapper

public class Date : BaseData{
	public var startTime : String!
	public var endTime : String!
    
    public override func mapping(map: Map){
        startTime <- map["startTime"]
        endTime <- map["endTime"]
    }

}