//
//  Assignments.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/20/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

public class Assignments : BaseData{
    
    var date : String!
    var id : Int!
    var server : Server!
    var table : Table!
    
    override public func mapping(map: Map) {
        date <- map["date"]
        id <- map["id"]
        server <- map["server"]
        table <- map["table"]
        
    }
    
}