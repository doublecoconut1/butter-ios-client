//
//  Statistic.swift
//  Butter
//
//  Created by Grigor Avagyan on 10/31/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class Statistic : BaseData, CustomStringConvertible {
        
    var averageSeatedTimeMinutes : Int!
    var cancelledTotal : Int!
    var doneTotal : Int!
    var noShowsTotal : Int!
    var ordersTotal : Int!
    var walkinsTotal : Int!
    
    override func mapping(map: Map) {
        averageSeatedTimeMinutes    <- map["averageSeatedTimeMinutes"]
        cancelledTotal              <- map["cancelledTotal"]
        doneTotal                   <- map["doneTotal"]
        noShowsTotal                <- map["noShowsTotal"]
        ordersTotal                 <- map["ordersTotal"]
        walkinsTotal                <- map["walkinsTotal"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}