/* 
Copyright (c) 2016 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class TablesMap {
	public var 64 : 64?
	public var 65 : 65?
	public var 66 : 66?
	public var 67 : 67?
	public var 24 : 24?
	public var 25 : 25?
	public var 26 : 26?
	public var 27 : 27?
	public var 28 : 28?
	public var 29 : 29?
	public var 30 : 30?
	public var 31 : 31?
	public var 32 : 32?
	public var 33 : 33?
	public var 34 : 34?
	public var 35 : 35?
	public var 36 : 36?
	public var 37 : 37?
	public var 38 : 38?
	public var 39 : 39?
	public var 40 : 40?
	public var 41 : 41?
	public var 42 : 42?
	public var 43 : 43?
	public var 44 : 44?
	public var 45 : 45?
	public var 46 : 46?
	public var 47 : 47?
	public var 48 : 48?
	public var 49 : 49?
	public var 50 : 50?
	public var 51 : 51?
	public var 52 : 52?
	public var 53 : 53?
	public var 54 : 54?
	public var 55 : 55?
	public var 56 : 56?
	public var 57 : 57?
	public var 58 : 58?
	public var 59 : 59?
	public var 60 : 60?
	public var 61 : 61?
	public var 62 : 62?
	public var 63 : 63?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let tablesMap_list = TablesMap.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of TablesMap Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [TablesMap]
    {
        var models:[TablesMap] = []
        for item in array
        {
            models.append(TablesMap(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let tablesMap = TablesMap(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: TablesMap Instance.
*/
	required public init?(dictionary: NSDictionary) {

		if (dictionary["64"] != nil) { 64 = 64(dictionary: dictionary["64"] as! NSDictionary) }
		if (dictionary["65"] != nil) { 65 = 65(dictionary: dictionary["65"] as! NSDictionary) }
		if (dictionary["66"] != nil) { 66 = 66(dictionary: dictionary["66"] as! NSDictionary) }
		if (dictionary["67"] != nil) { 67 = 67(dictionary: dictionary["67"] as! NSDictionary) }
		if (dictionary["24"] != nil) { 24 = 24(dictionary: dictionary["24"] as! NSDictionary) }
		if (dictionary["25"] != nil) { 25 = 25(dictionary: dictionary["25"] as! NSDictionary) }
		if (dictionary["26"] != nil) { 26 = 26(dictionary: dictionary["26"] as! NSDictionary) }
		if (dictionary["27"] != nil) { 27 = 27(dictionary: dictionary["27"] as! NSDictionary) }
		if (dictionary["28"] != nil) { 28 = 28(dictionary: dictionary["28"] as! NSDictionary) }
		if (dictionary["29"] != nil) { 29 = 29(dictionary: dictionary["29"] as! NSDictionary) }
		if (dictionary["30"] != nil) { 30 = 30(dictionary: dictionary["30"] as! NSDictionary) }
		if (dictionary["31"] != nil) { 31 = 31(dictionary: dictionary["31"] as! NSDictionary) }
		if (dictionary["32"] != nil) { 32 = 32(dictionary: dictionary["32"] as! NSDictionary) }
		if (dictionary["33"] != nil) { 33 = 33(dictionary: dictionary["33"] as! NSDictionary) }
		if (dictionary["34"] != nil) { 34 = 34(dictionary: dictionary["34"] as! NSDictionary) }
		if (dictionary["35"] != nil) { 35 = 35(dictionary: dictionary["35"] as! NSDictionary) }
		if (dictionary["36"] != nil) { 36 = 36(dictionary: dictionary["36"] as! NSDictionary) }
		if (dictionary["37"] != nil) { 37 = 37(dictionary: dictionary["37"] as! NSDictionary) }
		if (dictionary["38"] != nil) { 38 = 38(dictionary: dictionary["38"] as! NSDictionary) }
		if (dictionary["39"] != nil) { 39 = 39(dictionary: dictionary["39"] as! NSDictionary) }
		if (dictionary["40"] != nil) { 40 = 40(dictionary: dictionary["40"] as! NSDictionary) }
		if (dictionary["41"] != nil) { 41 = 41(dictionary: dictionary["41"] as! NSDictionary) }
		if (dictionary["42"] != nil) { 42 = 42(dictionary: dictionary["42"] as! NSDictionary) }
		if (dictionary["43"] != nil) { 43 = 43(dictionary: dictionary["43"] as! NSDictionary) }
		if (dictionary["44"] != nil) { 44 = 44(dictionary: dictionary["44"] as! NSDictionary) }
		if (dictionary["45"] != nil) { 45 = 45(dictionary: dictionary["45"] as! NSDictionary) }
		if (dictionary["46"] != nil) { 46 = 46(dictionary: dictionary["46"] as! NSDictionary) }
		if (dictionary["47"] != nil) { 47 = 47(dictionary: dictionary["47"] as! NSDictionary) }
		if (dictionary["48"] != nil) { 48 = 48(dictionary: dictionary["48"] as! NSDictionary) }
		if (dictionary["49"] != nil) { 49 = 49(dictionary: dictionary["49"] as! NSDictionary) }
		if (dictionary["50"] != nil) { 50 = 50(dictionary: dictionary["50"] as! NSDictionary) }
		if (dictionary["51"] != nil) { 51 = 51(dictionary: dictionary["51"] as! NSDictionary) }
		if (dictionary["52"] != nil) { 52 = 52(dictionary: dictionary["52"] as! NSDictionary) }
		if (dictionary["53"] != nil) { 53 = 53(dictionary: dictionary["53"] as! NSDictionary) }
		if (dictionary["54"] != nil) { 54 = 54(dictionary: dictionary["54"] as! NSDictionary) }
		if (dictionary["55"] != nil) { 55 = 55(dictionary: dictionary["55"] as! NSDictionary) }
		if (dictionary["56"] != nil) { 56 = 56(dictionary: dictionary["56"] as! NSDictionary) }
		if (dictionary["57"] != nil) { 57 = 57(dictionary: dictionary["57"] as! NSDictionary) }
		if (dictionary["58"] != nil) { 58 = 58(dictionary: dictionary["58"] as! NSDictionary) }
		if (dictionary["59"] != nil) { 59 = 59(dictionary: dictionary["59"] as! NSDictionary) }
		if (dictionary["60"] != nil) { 60 = 60(dictionary: dictionary["60"] as! NSDictionary) }
		if (dictionary["61"] != nil) { 61 = 61(dictionary: dictionary["61"] as! NSDictionary) }
		if (dictionary["62"] != nil) { 62 = 62(dictionary: dictionary["62"] as! NSDictionary) }
		if (dictionary["63"] != nil) { 63 = 63(dictionary: dictionary["63"] as! NSDictionary) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.64?.dictionaryRepresentation(), forKey: "64")
		dictionary.setValue(self.65?.dictionaryRepresentation(), forKey: "65")
		dictionary.setValue(self.66?.dictionaryRepresentation(), forKey: "66")
		dictionary.setValue(self.67?.dictionaryRepresentation(), forKey: "67")
		dictionary.setValue(self.24?.dictionaryRepresentation(), forKey: "24")
		dictionary.setValue(self.25?.dictionaryRepresentation(), forKey: "25")
		dictionary.setValue(self.26?.dictionaryRepresentation(), forKey: "26")
		dictionary.setValue(self.27?.dictionaryRepresentation(), forKey: "27")
		dictionary.setValue(self.28?.dictionaryRepresentation(), forKey: "28")
		dictionary.setValue(self.29?.dictionaryRepresentation(), forKey: "29")
		dictionary.setValue(self.30?.dictionaryRepresentation(), forKey: "30")
		dictionary.setValue(self.31?.dictionaryRepresentation(), forKey: "31")
		dictionary.setValue(self.32?.dictionaryRepresentation(), forKey: "32")
		dictionary.setValue(self.33?.dictionaryRepresentation(), forKey: "33")
		dictionary.setValue(self.34?.dictionaryRepresentation(), forKey: "34")
		dictionary.setValue(self.35?.dictionaryRepresentation(), forKey: "35")
		dictionary.setValue(self.36?.dictionaryRepresentation(), forKey: "36")
		dictionary.setValue(self.37?.dictionaryRepresentation(), forKey: "37")
		dictionary.setValue(self.38?.dictionaryRepresentation(), forKey: "38")
		dictionary.setValue(self.39?.dictionaryRepresentation(), forKey: "39")
		dictionary.setValue(self.40?.dictionaryRepresentation(), forKey: "40")
		dictionary.setValue(self.41?.dictionaryRepresentation(), forKey: "41")
		dictionary.setValue(self.42?.dictionaryRepresentation(), forKey: "42")
		dictionary.setValue(self.43?.dictionaryRepresentation(), forKey: "43")
		dictionary.setValue(self.44?.dictionaryRepresentation(), forKey: "44")
		dictionary.setValue(self.45?.dictionaryRepresentation(), forKey: "45")
		dictionary.setValue(self.46?.dictionaryRepresentation(), forKey: "46")
		dictionary.setValue(self.47?.dictionaryRepresentation(), forKey: "47")
		dictionary.setValue(self.48?.dictionaryRepresentation(), forKey: "48")
		dictionary.setValue(self.49?.dictionaryRepresentation(), forKey: "49")
		dictionary.setValue(self.50?.dictionaryRepresentation(), forKey: "50")
		dictionary.setValue(self.51?.dictionaryRepresentation(), forKey: "51")
		dictionary.setValue(self.52?.dictionaryRepresentation(), forKey: "52")
		dictionary.setValue(self.53?.dictionaryRepresentation(), forKey: "53")
		dictionary.setValue(self.54?.dictionaryRepresentation(), forKey: "54")
		dictionary.setValue(self.55?.dictionaryRepresentation(), forKey: "55")
		dictionary.setValue(self.56?.dictionaryRepresentation(), forKey: "56")
		dictionary.setValue(self.57?.dictionaryRepresentation(), forKey: "57")
		dictionary.setValue(self.58?.dictionaryRepresentation(), forKey: "58")
		dictionary.setValue(self.59?.dictionaryRepresentation(), forKey: "59")
		dictionary.setValue(self.60?.dictionaryRepresentation(), forKey: "60")
		dictionary.setValue(self.61?.dictionaryRepresentation(), forKey: "61")
		dictionary.setValue(self.62?.dictionaryRepresentation(), forKey: "62")
		dictionary.setValue(self.63?.dictionaryRepresentation(), forKey: "63")

		return dictionary
	}

}