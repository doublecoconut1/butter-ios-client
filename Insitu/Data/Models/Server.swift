
import Foundation
import ObjectMapper

public class Server : BaseData{
    
	public var id : Int!
	public var firstName : String!
    public var lastName : String!
	public var color : String!
	public var coverTotal : Int!
	public var coverCurrent : Int!
	public var dataType : String!
    
    override init() {
        super.init()
    }
    
    override public func mapping(map:Map){
        id <- map["id"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        color <- map["color"]
        coverTotal <- map["coverTotal"]
        coverCurrent <- map["coverCurrent"]
        dataType <- map["dataType"]
    }

}