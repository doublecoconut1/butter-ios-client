//
//  File.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/26/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class OrderWithMultipleTables: BaseData, CustomStringConvertible{
    
    var codes : [Code]!
    var notes : [Note]!
    var guestCount : Int!
    var orderDateTime : String!
    var tables : [Tables]!
    var user : User!
    var waitList: WaitList!
    
    override func mapping(map: Map) {
        codes <- (map["codes"], ArrayTransform<Code>())
        notes <- (map["notes"], ArrayTransform<Note>())
        guestCount <- map["guestCount"]
        orderDateTime <- map["orderDateTime"]
        tables <- map["tables"]
        user <- map["user"]
        waitList <- map["waitList"]
    }
    
    var description: String{
        return self.toJSONString()!
    }
}