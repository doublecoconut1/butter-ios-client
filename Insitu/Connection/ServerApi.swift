//
//  ServerApi.swift
//  Insitu
//
//  Created by Grigor Avagyan on 7/18/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

public class ServerApi{

    static var BASE_URL: String = "https://insitu-api.doublecoconut.com/api/admin"
//    static var BASE_URL: String = "http://192.168.0.102:8011/api/admin"
    
    static var DELETE_FROM_WAITLIST = BASE_URL + "/waitlist/%@"
    
    static var PUT_MULTI_ORDERS = BASE_URL + "/orders/multi/%@"
    static var PUT_SERVER_ASSIGNMENT = BASE_URL + "/assignments/%@"
    static var PUT_WAITLIST = BASE_URL + "/waitlist/%@"
    static var PUT_TABLES_UPDATE = BASE_URL + "/orders/status/tablesUpdate"
    static var PUT_ORDER_UPDATE = BASE_URL + "/orders/%@?force=true"
    static var PUT_ORDERS_STATUS_UPDATE_BY_PARENT_ID = BASE_URL + "/orders/status/multiUpdate/%@"
    static var PUT_SINGLE_ORDER_STATUS_UPDATE = BASE_URL + "/orders/status/%@"

    
    static var GET_CURR_DATE_INFO = BASE_URL + "/views/table"
    static var GET_CURR_DATE_INFO_V2 = BASE_URL + "/views/table/v2"
    static var GET_GUEST_LIST = BASE_URL + "/guests"
    static var GET_USER_LIST = BASE_URL + "/guests/pageable/v4"
    static var GET_ORDER_HISTORY = BASE_URL + "/guests/%@/order_history"
    static var GET_FLOORS_INFO = BASE_URL + "/floors"
    static var GET_AVAILABLE_TABLES = BASE_URL + "/orders/search?guestCount=%@&date=%@&time=%@"
    static var GET_RESERVATION_TIME_BY_MONTH = BASE_URL + "/orders/search/byMonth?"
    static var GET_USER_STATISTIC = BASE_URL + "/guests/%@/order_stats"

    static var POST_MULTI_ORDERS = BASE_URL + "/orders/multi"
    static var POST_NEW_ORDER_FROM_SEARCH = BASE_URL + "/orders/createBySearch"
    static var POST_NEW_ORDER = BASE_URL + "/orders"
    static var POST_WALKIN_ORDER = BASE_URL + "/orders"
    static var POST_NEW_USER = BASE_URL + "/guests"
    static var POST_SERVER_ASSIGNMENT = BASE_URL + "/assignments"
    static var POST_WAITLIST = BASE_URL + "/waitlist"
    
    public var onComplete: ((result: String)->())?
    
    
    class var getInstance: ServerApi {
        struct Static {
            static let instance = ServerApi()
        }
        return Static.instance
    }
    
    func postWaitList(waitOrder: WaitList, update: Bool, callBack: ((AnyObject) -> Void)?){
        
        var method : Alamofire.Method = .PUT
        
        if !update {
            method = .POST
            waitOrder.orderedTable.id = 188
        }
        
        let param = Mapper().toJSON(waitOrder)
        let url = method == .POST ? ServerApi.POST_WAITLIST : String(format: ServerApi.PUT_WAITLIST, String(waitOrder.id))
        
        Alamofire.request(method, url, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in postWaitList")
            }
        }
        
        
    }
    
    func getUserStatistic(id: Int, callBack: ((Statistic?) -> Void)?) {
        
        let url = String(format: ServerApi.GET_USER_STATISTIC, String(id))
        
        Alamofire.request(.GET, url, parameters: nil, encoding: .JSON).responseObject{(response: Response<Statistic, NSError>) in
            if(response.result.isSuccess){
                callBack!(response.result.value!)
            }else{
                callBack!(nil)
                print("error in updateSingleOrderStatus")
            }
        }
    }
    
    func updateSingleOrderStatus(order: Order, callBack: ((AnyObject) -> Void)?){
        
        let url = String(format: ServerApi.PUT_SINGLE_ORDER_STATUS_UPDATE, String(order.id))
        var params = Mapper().toJSON(order)
        params["dateTime"] = TimeUtils.dateToString(TimeUtils.now(), dateFormat: YYYYMMDDHHMMSS)
        
        Alamofire.request(.PUT, url, parameters: params, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in updateSingleOrderStatus")
            }
        }
    }
    
    func updateOrderStatusesByParentId(parentId: Int, status: GuestStatus, callBack: ((AnyObject) -> Void)?){
        
        
        let url = String(format: ServerApi.PUT_ORDERS_STATUS_UPDATE_BY_PARENT_ID, String(parentId))
        var params = [String: AnyObject]()
        
        params["status"] = status.rawValue
        params["dateTime"] = TimeUtils.dateToString(TimeUtils.now(), dateFormat: YYYYMMDDHHMMSS)
        
        Alamofire.request(.PUT, url, parameters: params, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in updateOrderStatusesByParentId")
            }
        }
    }
    
    func createUser(user: User, callBack: (User) -> ()){
        let param = Mapper().toJSON(user)
        Alamofire.request(.POST, ServerApi.POST_NEW_USER, parameters: param, encoding: .JSON).responseObject{(response: Response<User, NSError>) in
            if(response.result.isSuccess){
                callBack(response.result.value!)
            }else{
                print("error in postq createUser")
            }
        }
    }
    
    func deleteOrderFromWaitlist(waitOrder: WaitList, callBack: ((AnyObject) -> Void)?){
        
        let param = Mapper().toJSON(waitOrder)
        let url = String(format: ServerApi.DELETE_FROM_WAITLIST, String(waitOrder.id))
        
        Alamofire.request(.DELETE, url, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in deleteOrderFromWaitlist")
            }
        }
    }
    
    func postMultyOrder(orderId: String?, order: OrderWithMultipleTables, orderCategory: OrderCategory?, callBack: ((AnyObject) -> Void)?){
        
        let url = orderId != nil ? String(format: ServerApi.PUT_MULTI_ORDERS, orderId!) : ServerApi.POST_MULTI_ORDERS
        var param = Mapper().toJSON(order)
        
        if orderCategory != nil {
            param["type"] = OrderCategory.WALK_IN.rawValue
            param["status"] = GuestStatus.SEATED.rawValue
            param["dateTime"] = TimeUtils.dateToString(TimeUtils.now(), dateFormat: YYYYMMDDHHMMSS)
        }
        
        let method: Alamofire.Method = orderId != nil ? .PUT : .POST
        
        Alamofire.request(method, url, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil {
                    callBack!(response.result.value!)
                }
            }else{
                print("error in postMultyOrder")
            }
        }
    }
    
    func postOrder(order: Order, callBack: ((AnyObject) -> Void)?){
        
        var param = Mapper().toJSON(order)
        param["dateTime"] = TimeUtils.dateToString(TimeUtils.now(), dateFormat: YYYYMMDDHHMMSS)
        
        Alamofire.request(.POST, ServerApi.POST_NEW_ORDER, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in postCreatedOrderBySearch")
            }
        }
        
    }
    
    func getAvailableTables(guestCount: Int, date: NSDate, callBack: AvailableTables -> ()){
        let stringDate = TimeUtils.dateToString(date, dateFormat: YYYYMMDD)
        let time = TimeUtils.dateToString(date, dateFormat: HHMM)
        let url = String(format: ServerApi.GET_AVAILABLE_TABLES, "\(guestCount)", stringDate, time)
        Alamofire.request(.GET, url, parameters: nil).responseObject{(response: Response<AvailableTables, NSError>) in
            if(response.result.isSuccess){
                callBack(response.result.value!)
            }else{
                print("error in getAvailableTables")
            }
        }
    }
    
    func getUserOrderHistory(userId: Int, callBack: OrderHistory -> ()) {
        let url = String(format: ServerApi.GET_ORDER_HISTORY, "\(userId)")
        Alamofire.request(.GET, url, parameters: nil).responseObject{(response: Response<OrderHistory, NSError>) in
            if(response.result.isSuccess){
                callBack(response.result.value!)
            }else{
                print("error in getOrderHistory")
            }
        }
    }
    
    func getFloorsInfo(callBack: (data: [FloorInfo]) -> ()){
        Alamofire.request(.GET, ServerApi.GET_FLOORS_INFO, parameters: nil).responseArray{ (response: Response<[FloorInfo], NSError>) in
            if(response.result.isSuccess){
                callBack(data: response.result.value!)
            }else{
                print("error in getFloorsInfo")
            }
        }
    }
    
    func searchReservationTimesByMonth(guestCount: Int, callBack: (data: [ReservationMonthRange]) -> ()){
        let params = ["guestCount": guestCount]
        Alamofire.request(.GET, ServerApi.GET_RESERVATION_TIME_BY_MONTH, parameters: params).responseArray{ (response: Response<[ReservationMonthRange], NSError>) in
            if(response.result.isSuccess){
                callBack(data: response.result.value!)
            }else{
                print("error in searchReservationTimesByMonth")
            }
        }
    }
    
    func getCurrentDateInfo(params: Dictionary<String, AnyObject>, callBack: (data: CurrentDateDataResponse) -> ()){
        
        Alamofire.request(.GET, ServerApi.GET_CURR_DATE_INFO_V2, parameters: params).responseObject{(response: Response<CurrentDateDataResponse, NSError>) in
            if(response.result.isSuccess){
                callBack(data: response.result.value!)
            }else{
                print("error in getCurrentDateInfo")
            }
        }
    }
    
    func getUsersWithName(text: String, callBack: (data: [User]) -> ()){
        
        let params = ["query": text]
            
        Alamofire.request(.GET, ServerApi.GET_USER_LIST, parameters: params).responseObject{ (response: Response<UsersInfo, NSError>) in
            if(response.result.isSuccess){
                callBack(data: response.result.value!.result)
            }else{
                print("error in getCurrentDateInfo")
            }
        }
    }
    
    func getGuestList(text: String, callBack: (data: [User]) -> ()){
        let dateString = TimeUtils.dateToString(TimeUtils.now(), dateFormat: YYYYMMDD)
        let params = ["date": dateString, "firstName" : text, "lastName": text, "email" : text]
        
        Alamofire.request(.GET, ServerApi.GET_GUEST_LIST, parameters: params).responseArray{ (response: Response<[User], NSError>) in
            if(response.result.isSuccess){
                callBack(data: response.result.value!)
            }else{
                print("error in getCurrentDateInfo")
            }
        }
    }
    
    func updateAllTableStatusesToDone(tables: [Tables], callBack: ((AnyObject) -> Void)?){
        
        var params = [String: AnyObject]()
        params["tables"] = tables.toJSON()
        params["status"] = GuestStatus.DONE.rawValue
        params["dateTime"] = TimeUtils.dateToString(TimeUtils.now(), dateFormat: YYYYMMDDHHMMSS)
        
        Alamofire.request(.PUT, ServerApi.PUT_TABLES_UPDATE, parameters: params, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in updateAllTableStatuses")
            }
        }

    }
    
    func updateServerOnTable(tableInfo: TableInfo, callBack: ((AnyObject) -> Void)?){
        
        let url = String(format: ServerApi.PUT_SERVER_ASSIGNMENT, String(tableInfo.assignment.id))
        
        var serviceParam = [String: AnyObject]()
        serviceParam["id"] = 1
        serviceParam["name"] = "Lunch"
        
        var param = Mapper().toJSON(tableInfo.assignment)
        param["service"] = serviceParam
        
        
        Alamofire.request(.PUT, url, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in updateServerOnTable")
            }
        }
    }
    
    func addServerToTable(tableInfo: TableInfo, server: Server, currDate: String, callBack: ((AnyObject) -> Void)?){
        
        var serviceParam = [String: AnyObject]()
        serviceParam["id"] = 1
        serviceParam["name"] = "Lunch"
        
        var param = [String:AnyObject]()
        param["table"] = Mapper().toJSON(tableInfo.table)
        param["service"] = serviceParam
        param["server"] = Mapper().toJSON(server)
        param["date"] = currDate
        
        Alamofire.request(.POST, ServerApi.POST_SERVER_ASSIGNMENT, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in addServerToTable")
            }
        }
    }
    
    func postCreatedOrderBySearch(order: Order, callBack: ((AnyObject) -> Void)?){
        let param = Mapper().toJSON(order)
        Alamofire.request(.POST, ServerApi.POST_NEW_ORDER_FROM_SEARCH, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in postCreatedOrderBySearch")
            }
        }
    }
    
    func postWalkInOrder(order: Order, callBack: ((AnyObject) -> Void)?){
        
        var param = Mapper().toJSON(order)
        param["type"] = OrderCategory.WALK_IN.rawValue
        param["status"] = GuestStatus.SEATED.rawValue
        param["dateTime"] = TimeUtils.dateToString(TimeUtils.now(), dateFormat: YYYYMMDDHHMMSS)
        
        Alamofire.request(.POST, ServerApi.POST_WALKIN_ORDER, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in postWalkInOrder")
            }
        }
    }
    
    func forceUpdateOrder(order: Order, callBack: ((AnyObject) -> Void)?){
        
        let url = String(format: ServerApi.PUT_ORDER_UPDATE, "\(order.id)")
        
        var param = Mapper().toJSON(order)
        param["dateTime"] = TimeUtils.dateToString(TimeUtils.now(), dateFormat: YYYYMMDDHHMMSS)
        
        
        Alamofire.request(.PUT, url, parameters: param, encoding: .JSON).responseString{(response: Response<String, NSError>) in
            if(response.result.isSuccess){
                if callBack != nil{
                    callBack!(response.result.value!)
                }
            }else{
                print("error in forceUpdateOrder")
            }
        }
    }
}