//
//  UserDefaults.swift
//  Butter
//
//  Created by Grigor Avagyan on 8/22/16.
//  Copyright © 2016 Grigor Avagyan. All rights reserved.
//

import Foundation
import ObjectMapper

class UserDefaults{
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    class var getInstance: UserDefaults {
        struct Static {
            static let instance = UserDefaults()
        }
        return Static.instance
    }
    
    func setOpeningTime(date: Date){
        userDefaults.setValue(date.toJSONString(), forKey: "openingTime")
        userDefaults.synchronize()
    }
    
    func getOpeningTime() -> Date{
        let date = userDefaults.valueForKey("openingTime") as! String
        return Mapper<Date>().map(date)!
    }
}